<?php
    require_once "repositorio.php";
?>
<!DOCTYPE HTML>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8" />
        <title>Copa do Mundo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            table, td, th {
                border: 1px solid black;
                text-align: center;
                padding: 4px;
            }
            table {
                border-collapse: collapse;
                width: 200px;
            }
            input[type=text] {
                clear: both;
            }
        </style>
    </head>
    <body>

        <form method="POST" action="efetuar-cadastro.php">
            <input type="text" name="nome"  required placeholder="Nome"/><br />
            <input type="number" name="qtdeCampeao" required placeholder="Qtde Campeão"/><br />

            <button>Cadastrar</button>
        </form>

        <br /><br />

        <table>
            <tr>
                <th>Código</th>
                <th>Seleção</th>
                <th>Qtde Campeão</th>
                <th>Opções</th>
            </tr>
            <?php
                $selecoes = listarSelecoes();
                foreach($selecoes as $selecao) {
                    echo "<tr><td>{$selecao->getCodigo()}</td>";
                    echo "<td>{$selecao->getNome()}</td>";
                    echo "<td>{$selecao->getQtdeCampeao()}</td>";
                    ?>
                        <td>
                            <form method="POST" action="efetuar-exclusao.php">
                                <input name="codigo" type="hidden" value="<?= $selecao->getCodigo() ?>" />
                                <button>Excluir</button>
                            </form>
                        </td>
                    <?php
                    echo "</tr>";
                }
            ?>
        </table>

    </body>
</html>
