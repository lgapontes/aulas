mysql -u root -p
create database copa_database CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
create user 'copa_user'@'localhost' identified by '123eja';
grant all on copa_database.* to 'copa_user'@'localhost' identified by '123eja';

create table selecoes (
    codigo bigint auto_increment,
    nome varchar(100) not null unique,
    qtdeCampeao int default 0,
    primary key (codigo)
);

insert into selecoes (nome,qtdeCampeao) values
    ('Brasil',5),
    ('Alemanha',4);
