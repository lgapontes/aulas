<?php

class Selecao {
    private $codigo;
    private $nome;
    private $qtdeCampeao;

    public function __construct($codigo,$nome,$qtdeCampeao) {
        $this->codigo = $codigo;
        $this->nome = $nome;
        $this->qtdeCampeao = $qtdeCampeao;
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function getNome() {
        return $this->nome;
    }
    public function getQtdeCampeao() {
        return $this->qtdeCampeao;
    }
}
