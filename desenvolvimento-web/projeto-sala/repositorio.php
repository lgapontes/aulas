<?php

require_once "modelo.php";

error_reporting(E_ERROR | E_PARSE);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

function conectar() {
    $conexao = new mysqli("127.0.0.1","copa_user","123eja","copa_database");
    mysqli_set_charset($conexao, "utf8");
    return $conexao;
}

function listarSelecoes() {
    try {
        $selecoes = array();

        $conexao = conectar();
        $query = $conexao->prepare("select codigo, nome, qtdeCampeao from selecoes");
        $query->execute();
        $resultado = $query->get_result();

        while ($linha = $resultado->fetch_assoc()) {
            $selecao = new Selecao(
                $linha["codigo"],
                $linha["nome"],
                $linha["qtdeCampeao"]
            );
            array_push($selecoes,$selecao);
        }

        return $selecoes;

    } catch (Exception $e) {
        var_dump($e);
        die();
    }
}

function excluirSelecao($codigo) {
    try {

        $conexao = conectar();
        $query = $conexao->prepare("delete from selecoes where codigo = ?");
        $query->bind_param("i",$codigo);
        $query->execute();

    } catch (Exception $e) {
        var_dump($e);
        die();
    }
}

function incluirSelecao($selecao) {
    try {
        $conexao = conectar();
        $query = $conexao->prepare("insert into selecoes (nome,qtdeCampeao) values (?,?)");
        $query->bind_param("si",$selecao->getNome(),$selecao->getQtdeCampeao());
        $query->execute();

    } catch (Exception $e) {
        var_dump($e);
        die();
    }
}
