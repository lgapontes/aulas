<?php

    if (
        !isset($_POST["endereco"]) ||
        !isset($_POST["cidade"]) ||
        !isset($_POST["cep"]) ||
        strlen($_POST["endereco"]) == 0 ||
        strlen($_POST["cidade"]) == 0 ||
        strlen($_POST["cep"]) == 0
    ) {
        echo "Todos os campos são obrigatórios!";
        die();
    }

    $endereco = $_POST["endereco"];
    $cidade = $_POST["cidade"];
    $cep = $_POST["cep"];

    $query = $endereco . ", " . $cidade . ", cep " . $cep;

    header("Location: https://maps.google.com?q=" . urlencode($query));
    die();
