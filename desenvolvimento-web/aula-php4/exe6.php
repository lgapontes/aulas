<?php
    header('Access-Control-Allow-Origin: *');

    class Dados {
        public $mensagem;
        public function __construct($mensagem) {
            $this->mensagem = $mensagem;
        }
    }

    $dados = new Dados("API sem erros de acentuação!");
    
    header('Content-Type: application/json');
    echo json_encode($dados, JSON_UNESCAPED_UNICODE);
    die();
