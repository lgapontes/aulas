<?php
    header('Access-Control-Allow-Origin: *');

    class Cliente {
        public $nome;
        public $idade;
        public $aposentado;
        public function __construct($nome,$idade,$aposentado) {
            $this->nome = $nome;
            $this->idade = $idade;
            $this->aposentado = $aposentado;
        }
    }
    $cliente = new Cliente("João",40,false);

    header('Content-Type: application/json');
    echo json_encode($cliente, JSON_UNESCAPED_UNICODE);
    die();