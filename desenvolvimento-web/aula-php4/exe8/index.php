<?php

    require_once "repositorio.php";

    header('Access-Control-Allow-Origin: *');

    $perfis = listarPerfis();

    header('Content-Type: application/json');
    echo json_encode($perfis, JSON_UNESCAPED_UNICODE);
    die();
