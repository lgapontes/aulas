<?php

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

function exibirErro($erro) {
    echo "<pre>";
    var_dump($erro);
    echo "</pre>";
    die();
}

function conectar() {
    try {
        $conexao = new mysqli('127.0.0.1','roupas_user','123eja','roupas_database');
        return $conexao;
    } catch(Exception $erro) {
        exibirErro($erro);
    }
}

function listarPerfis() {
    try {

        $perfis = array();
        $conexao = conectar();
        $query = $conexao->prepare('select codigo, descricao from perfis');
        $query->execute();
        $resultado = $query->get_result();
        if ($resultado->num_rows > 0) {
            while($perfil = $resultado->fetch_assoc()) {
                array_push($perfis,$perfil);
            }
        }
        return $perfis;

    } catch(Exception $erro) {
        exibirErro($erro);
    }
}
