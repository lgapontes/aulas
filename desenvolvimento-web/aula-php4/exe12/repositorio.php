<?php

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

function exibirErro($erro) {
    echo "<pre>";
    var_dump($erro);
    echo "</pre>";
    die();
}

function conectar() {
    try {
        $conexao = new mysqli('127.0.0.1','roupas_user','123eja','roupas_database');
        return $conexao;
    } catch(Exception $erro) {
        exibirErro($erro);
    }
}

function inserirPerfil($descricao) {
    try {

        $conexao = conectar();
        $query = $conexao->prepare('insert into perfis (descricao) values (?)');
        $query->bind_param('s',$descricao);
        $query->execute();
        $codigo = $conexao->insert_id;
        $query->close();
        $conexao->close();
        return obterPerfil($codigo);

    } catch(Exception $erro) {
        throw new Exception("Erro ao inserir perfil: " . $erro->getMessage());
    }
}

function listarPerfis() {
    try {

        $perfis = array();
        $conexao = conectar();
        $query = $conexao->prepare('select codigo, descricao from perfis');
        $query->execute();
        $resultado = $query->get_result();
        if ($resultado->num_rows > 0) {
            while($perfil = $resultado->fetch_assoc()) {
                array_push($perfis,$perfil);
            }
        }
        return $perfis;

    } catch(Exception $erro) {
        exibirErro($erro);
    }
}

function obterPerfil($codigo) {
    try {

        $conexao = conectar();
        $query = $conexao->prepare('select codigo, descricao from perfis where codigo = ?');
        $query->bind_param('i',$codigo);
        $query->execute();
        $resultado = $query->get_result();
        if ($resultado->num_rows == 1) {
            return $resultado->fetch_assoc();
        } else {
            return null;
        }

    } catch(Exception $erro) {
        exibirErro($erro);
    }
}
