<?php

    require_once "repositorio.php";

    header('Access-Control-Allow-Origin: *');

    $json = array();

    if (isset($_GET['codigo'])) {
        $codigo = intval($_GET['codigo']);
        $perfil = obterPerfil($codigo);

        if ($perfil == null) {
            $json["status"] = 404;
            $json["mensagem"] = "Perfil não encontrado!";
        } else {
            $json["status"] = 200;
            $json["mensagem"] = "Perfil retornado com sucesso!";
            $json["dados"] = $perfil;
        }

    } else {
        $json["status"] = 200;
        $json["mensagem"] = "Perfil retornado com sucesso!";
        $json["dados"] = listarPerfis();
    }

    header('Content-Type: application/json');
    echo json_encode($json, JSON_UNESCAPED_UNICODE);
    die();
