<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/favicon.png">
        <style>
            h1 {
                text-align: center;
            }

            div {
                width: 100%;
                max-width: 500px;
                margin: 0 auto;
                padding: 10px;
                font-size: 1.2em;
                font-family: monospace;
                background-color: #dddddd;
            }

            input {
                display: block;
                font-size: 1.2em;
                margin: 10px auto;
                width: 100px;
            }

            button {
                display: block;
                font-size: 1.2em;
                margin: 10px auto;
            }
        </style>
    </head>
    <body>

        <h1>Consumindo dados dos perfis (opção 3)</h1>
        <input type="number" value="1" placeholder="Código do perfil" />
        <button>Consultar Perfil</button>
        <div>Resultado de consulta da API...</div>

        <script src="js/jquery-3.3.1.min.js"></script>
        <script>

            $('button').on('click',function(evento){
                evento.preventDefault();
                $('div').text('Realizando acesso...');

                var url = 'http://127.0.0.1:8080/exe10/index-opcao3.php?codigo=' + $('input').val();

                $.get(url,function(json){
                    $('div').text(JSON.stringify(json));
                }).fail(function(json){
                    var retorno = json.responseText;
                    $('div').text(retorno);
                });

            });

        </script>
    </body>
</html>
