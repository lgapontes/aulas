<?php
    require_once "repositorio.php";
    header('Access-Control-Allow-Origin: *');

    function status400($msg) {
        header("HTTP/1.1 400");
        echo '{"mensagem": "' . $msg . '"}';
        die();
    }
    function status404($msg) {
        header("HTTP/1.1 404");
        echo '{"mensagem": "' . $msg . '"}';
        die();
    }
    function status405() {
        header("HTTP/1.1 405");
        echo '{"mensagem": "Método não permitido!"}';
        die();
    }
    function status200($json) {
        header('Content-Type: application/json');
        echo json_encode($json, JSON_UNESCAPED_UNICODE);
        die();
    }
    function status201($json) {
        header('Content-Type: application/json');
        header("HTTP/1.1 201");
        echo json_encode($json, JSON_UNESCAPED_UNICODE);
        die();
    }

    if ($_SERVER["REQUEST_METHOD"] === "POST") {

        $body = file_get_contents("php://input");
        $dados = json_decode($body,true);

        if ( array_key_exists("descricao",$dados) ) {

            try {

                $perfil = inserirPerfil($dados["descricao"]);
                status201($perfil);

            } catch(Exception $e) {
                status400($e->getMessage());
            }

        } else {
            status400("Perfil não enviado corretamente!");
        }

    } else if ($_SERVER["REQUEST_METHOD"] === "PUT") {

        $body = file_get_contents("php://input");
        $dados = json_decode($body,true);

        if ( array_key_exists("codigo",$dados) && array_key_exists("descricao",$dados) ) {

            try {
                $codigo = $dados["codigo"];
                $descricao = $dados["descricao"];

                $perfil = atualizarPerfil($codigo,$descricao);
                status200($perfil);

            } catch(Exception $e) {
                status400($e->getMessage());
            }

        } else {
            status400("Perfil não enviado corretamente!");
        }

    } else if ($_SERVER["REQUEST_METHOD"] === "DELETE") {

        $body = file_get_contents("php://input");
        $dados = json_decode($body,true);

        if ( array_key_exists("codigo",$dados) ) {

            try {
                $codigo = $dados["codigo"];

                $resultado = apagarPerfil($codigo);

                if ($resultado) {
                    $retorno = array(
                        "mensagem" => "Perfil de código " . $codigo . " excluído com sucesso!"
                    );
                    status200($retorno);
                } else {
                    status404("Perfil de código " . $codigo . " não encontrado!");
                }

            } catch(Exception $e) {
                status400($e->getMessage());
            }

        } else {
            status400("Perfil não enviado corretamente!");
        }

    } else {
        status405();
    }
