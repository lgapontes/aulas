<?php
    header('Access-Control-Allow-Origin: *');

    $cliente = array(
        "nome" => "João",
        "idade" => 40,
        "aposentado" => false
    );

    header('Content-Type: application/json');
    echo json_encode($cliente, JSON_UNESCAPED_UNICODE);
    die();
