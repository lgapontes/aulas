<?php

    require_once "repositorio.php";

    header('Access-Control-Allow-Origin: *');

    if (isset($_GET['codigo'])) {
        $codigo = intval($_GET['codigo']);
        $json = obterPerfil($codigo);
    } else {
        $json = listarPerfis();    
    }

    header('Content-Type: application/json');
    echo json_encode($json, JSON_UNESCAPED_UNICODE);
    die();
