<?php
    header('Access-Control-Allow-Origin: *');

    class Dados {
        public $mensagem;
        public function __construct($mensagem) {
            $this->mensagem = $mensagem;
        }
    }
    $dados = new Dados("Minha primeira API!");

    header('Content-Type: application/json');
    echo json_encode($dados);
    die();
