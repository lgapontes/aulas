<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/favicon.png">
        <style>
            h1 {
                text-align: center;
            }

            div {
                width: 100%;
                max-width: 800px;
                margin: 0 auto;
                padding: 10px;
                font-size: 1.2em;
                font-family: monospace;
            }

            div.sucesso {
                background-color: #9fc4ac;
            }
            div.falha {
                background-color: #f4bdd2;
            }

            button {
                display: block;
                font-size: 1.2em;
                margin: 10px auto;
            }
        </style>
    </head>
    <body>

        <h1>Exemplo de acesso a API com e sem CORS</h1>
        <button id="botao-sem-cors">Consultar API sem CORS</button>
        <div id="div-sem-cors" class="falha">Resultado de consulta da API sem CORS...</div>
        <br />
        <button id="botao-com-cors">Consultar API com CORS</button>
        <div id="div-com-cors" class="sucesso">Resultado de consulta da API com CORS...</div>

        <script src="js/jquery-3.3.1.min.js"></script>
        <script>

            function consultarAPI(url,callback) {
                $.get(url,function(json){
                    callback(json);
                }).fail(function(json){
                    callback('Ocorreu um erro ao consultar a API!');
                });
            }

            $('#botao-sem-cors').on('click',function(evento){
                evento.preventDefault();
                $('#div-sem-cors').text('Realizando acesso...');
                consultarAPI('http://lgapontes.com/aulas/cors/sem-cors/index.php',function(json){
                    $('#div-sem-cors').text(json);
                });
            });
            $('#botao-com-cors').on('click',function(evento){
                evento.preventDefault();
                $('#div-com-cors').text('Realizando acesso...');
                consultarAPI('http://lgapontes.com/aulas/cors/com-cors/index.php',function(json){
                    $('#div-com-cors').text(json.mensagem);
                });
            });
        </script>
    </body>
</html>
