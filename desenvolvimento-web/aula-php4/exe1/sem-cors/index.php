<?php

    class Exemplo implements JsonSerializable {
        private $mensagem;
        public function __construct($mensagem) {
            $this->mensagem = $mensagem;
        }
        public function jsonSerialize() {
            $vars = get_object_vars($this);
            return $vars;
        }
    }

    $exemplo = new Exemplo('JSON retornado com sucesso!');

    header('Content-Type: application/json');
    echo json_encode($exemplo, JSON_UNESCAPED_UNICODE);
    die();
