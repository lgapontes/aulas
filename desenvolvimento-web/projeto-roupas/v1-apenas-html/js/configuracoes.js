
var formulario = $('form.editar-produto');
var botaoSecundario = $('#secundario');

$('main div ul li > span.editar').each(function(index){
    var botaoEditar = $(this);
    var linha = botaoEditar.parent();

    botaoEditar.click(function(){
        exibirEditarProduto(linha);
    });

    /* Código apenas enquanto não tem PHP */
    linha.find('input[name=codigo]').val(index+1);
    linha.find('input[name=estoque]').val(1);
});

$('a.novo-produto').click(function(){
    var produto = {
        codigo: "",
        descricao: "",
        preco: "",
        estoque: ""
    };
    exibirProduto(produto);

    botaoSecundario.text('Cancelar').off().on('click',function(){
        formulario.hide();
    });
    formulario.show();
    rolarParaBaixo();
});

$('form').submit(function(event){
    event.preventDefault();

    var produto = {
        codigo: $('#codigo').val(),
        descricao: $('#descricao').val(),
        preco: $('#preco').val(),
        estoque: $('#estoque').val()
    };

    if (produto.codigo.length == 0) {
        novaLinha(produto);
    } else {
        atualizarLinha(produto);
    }
    formulario.hide();
});

function exibirProduto(produto) {
    $('#codigo').val(produto.codigo);
    $('#descricao').val(produto.descricao);
    $('#preco').val(produto.preco);
    $('#estoque').val(produto.estoque);
}

function novaLinha(produto) {
    var novoCodigo = $('main div ul li').length + 1;

    var linha = $('<li>');
    var inputCodigo = $('<input>').attr('type','hidden').attr('name','codigo').val(novoCodigo);
    var inputEstoque = $('<input>').attr('type','hidden').attr('name','estoque').val(produto.estoque);
    var spanDescricao = $('<span>').addClass('descricao').text(produto.descricao);
    var spanPreco = $('<span>').addClass('preco').text(produto.preco);
    var botaoEditar = $('<span>').addClass('editar');

    linha.append(inputCodigo);
    linha.append(inputEstoque);
    linha.append(spanDescricao);
    linha.append(spanPreco);
    linha.append(botaoEditar);
    linha.hide();

    $('main div ul').prepend(linha);
    linha.fadeIn(1000);

    botaoEditar.click(function(){
        exibirEditarProduto(linha);
    });
}

function atualizarLinha(produto) {
    var linha = $('main div ul li input[name=codigo][value=' + produto.codigo + ']').parent();
    linha.find('span.descricao').text(produto.descricao);
    linha.find('span.preco').text(produto.preco);
    linha.find('input[name=estoque]').val(produto.estoque);
}

function exibirEditarProduto(linha) {
    var produto = {
        codigo: linha.find('input[name=codigo]').val(),
        estoque: linha.find('input[name=estoque]').val(),
        descricao: linha.find('span.descricao').text(),
        preco: linha.find('span.preco').text()
    };
    exibirProduto(produto);

    botaoSecundario.text('Excluir').off().on('click',function(){
        formulario.hide();
        excluirProduto(linha);
    });

    formulario.show();
    rolarParaBaixo();
}

function excluirProduto(linha) {
    $.when(linha.fadeOut(1000)).done(function(){
        linha.remove();
    });
}

function rolarParaBaixo() {
    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
}
