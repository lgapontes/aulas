$('ul li > span.excluir').each(function(){
    var botaoExcluir = $(this);
    var linha = botaoExcluir.parent();

    botaoExcluir.on('click',function(){
        $.when(linha.fadeOut(1000)).done(function(){
            linha.remove();
            calcularTotal();
        });
    });
});

function calcularTotal() {
    var total = 0.0;
    $('ul li > span.preco').each(function(){
        total += parseFloat($(this).text());
    });

    var footerTotal = $('main div.borda > footer');

    if (total == 0) {
        footerTotal.text('R$ 0,00').hide();
        var spanVazio = $('<span>').addClass('vazio').text('Nenhum produto encontrado!');
        $('main div.borda').append(spanVazio);
        $('a.concluir-compra').hide();
    } else {
        $('main div.borda > footer').text('R$ ' + total.toFixed(2));
    }
}

$(function(){
    calcularTotal();
});
