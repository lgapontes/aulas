
function inicializarTooltip() {
    $('span.tooltip').tooltipster({
        timer: 3000
    });
}

$('form').submit(function(event){
    event.preventDefault();

    /* Simulação consulta a API */
    var spanTooltip = $('span.tooltip');
    spanTooltip
        .removeClass('info')
        .addClass('loading')
        .tooltipster('content', 'Verificando e-mail...');
    setTimeout(function(){
        spanTooltip
            .removeClass('loading')
            .addClass('warning')
            .tooltipster('content', 'Este e-mail já existe!')
            .tooltipster('show');
    },2000);
});

$(function(){
    inicializarTooltip();
});
