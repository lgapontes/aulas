function carregarImagensRoupas() {
    $('img.roupa').each(function(){
        var imagemRoupa = $(this);
        var imagemLoading = imagemRoupa.parent().find('img.loading');

        imagemRoupa.ready(function(){
            imagemLoading.addClass('loading-esconder');
            imagemRoupa.addClass('roupa-mostrar');
        });
    });
}

$(function(){
    carregarImagensRoupas();
});
