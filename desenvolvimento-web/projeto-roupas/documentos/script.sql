create table perfis(
	codigo bigint auto_increment,
	descricao varchar(100) unique not null,
	primary key (codigo)
);

create table usuarios (
	codigo bigint auto_increment,
	email varchar(100) unique not null,
	nome varchar(100) not null,
	senha varchar(64) not null,
	codigoPerfil bigint not null,
	primary key (codigo),
	foreign key (codigoPerfil) references perfis(codigo)
);

create table compras(
	codigo bigint auto_increment,
	codigoUsuario bigint not null,
	dataCompra timestamp default current_timestamp,
	primary key (codigo),
	foreign key (codigoUsuario) references usuarios(codigo)
);

create table imagens (
	codigo bigint auto_increment,
	nomeArquivo varchar(100) not null,
	primary key (codigo)
);

create table produtos (
	codigo bigint auto_increment,
	descricao varchar(25) not null,
	preco decimal(6,2) not null,
	estoque int not null,
	codigoImagem bigint not null,
	primary key (codigo),
	foreign key (codigoImagem) references imagens(codigo)
);

create table comprasProdutos (
	codigoCompra bigint not null,
	codigoProduto bigint not null,
	primary key (codigoCompra,codigoProduto),
	foreign key (codigoCompra) references compras(codigo),
	foreign key (codigoProduto) references produtos(codigo)
);

insert into perfis (descricao) values ('Cliente'), ('Vendedor');
