<?php
    require_once __DIR__."/lib/cabecalho.php";
?>

        <main>
            <?php 
                require_once __DIR__."/lib/mensagens.php";
            ?>

            <form method="POST" action="controller/efetuar-login.php">
                <h1>Login</h1>
                <div>
                    <label for="email">E-mail</label>
                    <input id="email" name="email" type="email" required>

                    <label for="senha">Senha</label>
                    <input id="senha" name="senha" type="password" required>

                    <button type="submit">Login</button>
                </div>
            </form>
        </main>

        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/tooltipster.bundle.min.js"></script>        
    </body>
</html>