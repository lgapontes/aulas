<?php

require_once __DIR__."/../lib/banco.php";
require_once __DIR__."/../model/perfil.php";

function listarPerfis() {
    $conexao = Conexao::getInstance();
    $perfis = array();
    $resultado = $conexao->query('select * from perfis');

    if ($resultado->num_rows > 0) {
        while ($linha = $resultado->fetch_assoc()) {
            $perfil = new Perfil($linha['codigo'],$linha['descricao']);
            array_push($perfis,$perfil);
        }
    }

    return $perfis;
}

function obterPerfil($codigo) {
    $conexao = Conexao::getInstance();

    $query = $conexao->prepare("select * from perfis where codigo = ?");
    $query->bind_param("i",$codigo);
    $query->execute();
    $query->store_result();
    $query->bind_result($codigoObtido, $descricao);

    if ($query->num_rows == 1) {
        $query->fetch();
        $perfil = new Perfil($codigoObtido,$descricao);
        return $perfil;
    } else {
        throw new Exception("Perfil de código " . $codigo . " não encontrado!");
    }
}
