<?php

require_once __DIR__."/../lib/banco.php";
require_once __DIR__."/../model/usuario.php";

function inserirUsuario($usuario) {
    try {

        $conexao = Conexao::getInstance();
        $query = $conexao->prepare("insert into usuarios (email,nome,senha,codigoPerfil) values (?,?,?,?)");
        $query->bind_param(
            "sssi",
            $usuario->getEmail(),
            $usuario->getNome(),
            $usuario->getHashSenha(),
            $usuario->getPerfil()->getCodigo()
        );
        $query->execute();

        $codigo = $conexao->insert_id;
        return obterUsuario($codigo);

    } catch (Exception $e) {
        throw new Exception("Erro ao inserir o usuário: " . $e->getMessage());
    }
}

function obterUsuario($codigo) {
    $conexao = Conexao::getInstance();

    $query = $conexao->prepare("select codigo, email, nome, codigoPerfil from usuarios where codigo = ?");
    $query->bind_param("i",$codigo);
    $query->execute();
    $query->store_result();
    $query->bind_result($codigoObtido, $email, $nome, $codigoPerfil);

    if ($query->num_rows == 1) {
        $query->fetch();
        $usuario = new Usuario(
            $email,
            $nome,
            $codigoPerfil
        );
        $usuario->setCodigo($codigoObtido);
        return $usuario;
    } else {
        throw new Exception("Usuário de código " . $codigo . " não encontrado!");
    }
}

function loginDoUsuario($email,$senha) {
    $conexao = Conexao::getInstance();

    $query = $conexao->prepare("select codigo, email, nome, codigoPerfil from usuarios where email = ? and senha = ?");
    $query->bind_param("ss",$email,$senha);
    $query->execute();
    $query->store_result();
    $query->bind_result($codigoObtido, $email, $nome, $codigoPerfil);

    if ($query->num_rows == 1) {
        $query->fetch();
        $usuario = new Usuario(
            $email,
            $nome,
            $codigoPerfil
        );
        $usuario->setCodigo($codigoObtido);
        return $usuario;
    } else {
        throw new Exception("Usuário sem acesso!");
    }
}

function emailExiste($email) {
    $conexao = Conexao::getInstance();
    $query = $conexao->prepare("select codigo, email, nome, codigoPerfil from usuarios where email = ?");
    $query->bind_param("s",$email);
    $query->execute();
    $query->store_result();
    $query->bind_result($codigoObtido, $email, $nome, $codigoPerfil);

    if ($query->num_rows == 1) {
        return true;
    } else {
        return false;
    }
}
