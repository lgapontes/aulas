<?php

require_once __DIR__."/../lib/banco.php";
require_once __DIR__."/../model/imagem.php";
require_once __DIR__."/../model/produto.php";

function factory($codigo, $descricao, $preco, $estoque, $codigoImagem, $nomeArquivo) {
    $imagem = new Imagem($nomeArquivo);
    $imagem->setCodigo($codigoImagem);

    $produto = new Produto(
        $descricao,
        $preco,
        $estoque,
        $imagem
    );
    $produto->setCodigo($codigo);

    return $produto;
}

function listarProdutos($comEstoque) {
    try {

        $conexao = Conexao::getInstance();

        $sql = "select p.codigo, p.descricao, p.preco, p.estoque, p.codigoImagem, i.nomeArquivo " .
            "from produtos p inner join imagens i on p.codigoImagem = i.codigo";

        if ( $comEstoque ) {
            $sql = $sql . " where p.estoque > 0";
        }

        $query = $conexao->prepare($sql);
        $query->execute();
        $query->store_result();
        $query->bind_result($codigo, $descricao, $preco, $estoque, $codigoImagem, $nomeArquivo);

        $produtos = array();

        if ( $query->num_rows > 0 ) {
            while ($query->fetch()) {
                $produto = factory($codigo, $descricao, $preco, $estoque, $codigoImagem, $nomeArquivo);
                array_push($produtos,$produto);
            }
        }

        return $produtos;

    } catch (Exception $e) {
        throw new Exception("Erro ao listar os produtos: " . $e->getMessage());
    }
}

function ref($arr) {
    $refs = array();
    foreach ($arr as $key => $val) $refs[$key] = &$arr[$key];
    return $refs;
}

function listarProdutosPorCodigo($codigos) {
    try {

        $conexao = Conexao::getInstance();

        // https://stackoverflow.com/questions/330268/i-have-an-array-of-integers-how-do-i-use-each-one-in-a-mysql-query-in-php
        $params = implode(",", array_fill(0, count($codigos), "?"));
        $query = $conexao->prepare(
            "select p.codigo, p.descricao, p.preco, p.estoque, p.codigoImagem, i.nomeArquivo " .
            "from produtos p inner join imagens i on p.codigoImagem = i.codigo where p.codigo in ($params)"
        );

        $types = str_repeat("i", count($codigos));                         // "iiii"
        $args = array_merge(array($types), $codigos);                      // ["iiii", 2, 4, 6, 8]
        call_user_func_array(array($query, 'bind_param'), ref($args)); // $query->bind_param("iiii", 2, 4, 6, 8)

        $query->execute();
        $query->store_result();
        $query->bind_result($codigo, $descricao, $preco, $estoque, $codigoImagem, $nomeArquivo);

        $produtos = array();

        if ( $query->num_rows > 0 ) {
            while ($query->fetch()) {
                $produto = factory($codigo, $descricao, $preco, $estoque, $codigoImagem, $nomeArquivo);
                array_push($produtos,$produto);
            }
        }

        return $produtos;

    } catch (Exception $e) {
        throw new Exception("Erro ao listar os produtos: " . $e->getMessage());
    }
}

function obterProduto($codigo) {
    try {

        $conexao = Conexao::getInstance();
        $query = $conexao->prepare(
            "select p.codigo, p.descricao, p.preco, p.estoque, p.codigoImagem, i.nomeArquivo " .
            "from produtos p inner join imagens i on p.codigoImagem = i.codigo where p.codigo = ?"
        );
        $query->bind_param("i",$codigo);
        $query->execute();
        $query->store_result();
        $query->bind_result($codigoObtido, $descricao, $preco, $estoque, $codigoImagem, $nomeArquivo);

        if ( $query->num_rows == 1 ) {
            $query->fetch();
            $produto = factory($codigoObtido, $descricao, $preco, $estoque, $codigoImagem, $nomeArquivo);
            return $produto;
        } else {
            throw new Exception("Produto de código " . $codigo . " não encontrado!");
        }

    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }
}

function inserirProduto($produto) {
    try {
        $conexao = Conexao::getInstance();

        $query = $conexao->prepare("insert into produtos (descricao, preco, estoque, codigoImagem) values (?,?,?,?)");
        $query->bind_param(
            "sdii",
            $produto->getDescricao(),
            $produto->getPreco(),
            $produto->getEstoque(),
            $produto->getImagem()->getCodigo()
        );
        $query->execute();

        $codigoInserido = $conexao->insert_id;
        return obterProduto($codigoInserido);

    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }
}

function atualizarProduto($produto) {
    try {
        $conexao = Conexao::getInstance();

        $query = $conexao->prepare(
            "update produtos set descricao=?, preco=?, estoque=?, codigoImagem=? " .
            "where codigo = ?"
        );
        $query->bind_param(
            "sdiii",
            $produto->getDescricao(),
            $produto->getPreco(),
            $produto->getEstoque(),
            $produto->getImagem()->getCodigo(),
            $produto->getCodigo()
        );
        $query->execute();

        if ($conexao->affected_rows != 1) {
            throw new Exception("Ocorreu um erro ao atualizar o produto de código " . $produto->codigo);
        }

        return obterProduto($produto->getCodigo());

    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }
}

function excluirProduto($codigoProduto) {
    $conexao = Conexao::getInstance();
    $produto = obterProduto($codigoProduto);
    $nomeArquivoImagem = $produto->getImagem()->getNomeArquivo();

    try {

        /* Desabilita o auto-commit do banco */
        $conexao->autocommit(false);

        /* Inicia uma transação */
        $conexao->begin_transaction();

        /* Excluir produto */
        $query = $conexao->prepare("delete from produtos where codigo = ?");
        $query->bind_param("i",$produto->getCodigo());
        $query->execute();

        /* Excluir imagem */
        excluirImagem($produto->getImagem()->getCodigo());

        /* Efetiva a transação e liga o auto-commmit */
        $conexao->commit();
        $conexao->autocommit(true);

        return $nomeArquivoImagem;

    } catch (Exception $e) {
        /* Rollback de todas as operações */
        $conexao->rollback();

        throw new Exception($e->getMessage());
    }
}

function inserirImagem($nomeArquivo) {
    try {
        $conexao = Conexao::getInstance();

        $query = $conexao->prepare("insert into imagens (nomeArquivo) values (?)");
        $query->bind_param("s",$nomeArquivo);
        $query->execute();

        $codigo = $conexao->insert_id;
        return obterImagem($codigo);

    } catch (Exception $e) {
        throw new Exception("Erro ao registrar o arquivo: " . $e->getMessage());
    }
}

function obterImagem($codigo) {
    $conexao = Conexao::getInstance();

    $query = $conexao->prepare("select * from imagens where codigo = ?");
    $query->bind_param("i",$codigo);
    $query->execute();
    $query->store_result();
    $query->bind_result($codigoObtido, $nomeArquivo);

    if ($query->num_rows == 1) {
        $query->fetch();
        $imagem = new Imagem($nomeArquivo);
        $imagem->setCodigo($codigoObtido);
        return $imagem;
    } else {
        throw new Exception("Imagem de código " . $codigo . " não encontrada!");
    }
}

function excluirImagem($codigo) {
    try {

        $conexao = Conexao::getInstance();
        $query = $conexao->prepare("delete from imagens where codigo = ?");
        $query->bind_param("i",$codigo);
        $query->execute();

        if ($conexao->affected_rows == 0) {
            throw new Exception("Imagem não encontrada!");
        }

    } catch (Exception $e) {
        throw new Exception("Erro ao excluir a imagem: " . $e);
    }
}

function realizarCompra($codigoUsuario,$carrinho) {
    $conexao = Conexao::getInstance();

    try {

        /* Desabilita o auto-commit do banco */
        $conexao->autocommit(false);

        /* Inicia uma transação */
        $conexao->begin_transaction();

        /* Registrando compra */
        $query = $conexao->prepare("insert into compras (codigoUsuario) values (?)");
        $query->bind_param("i",$codigoUsuario);
        $query->execute();
        $codigoCompra = $conexao->insert_id;

        /* Registrando produtos na compra */
        foreach($carrinho as $codigoProduto) {
            $query = $conexao->prepare("insert into comprasProdutos (codigoCompra,codigoProduto) values (?,?)");
            $query->bind_param("ii",$codigoCompra,$codigoProduto);
            $query->execute();
        }

        /* Ajustar estoque */
        $params = implode(",", array_fill(0, count($carrinho), "?"));
        $query = $conexao->prepare("update produtos set estoque = (estoque - 1) where codigo in ($params)");

        $types = str_repeat("i", count($carrinho));                    // "iiii"
        $args = array_merge(array($types), $carrinho);                 // ["iiii", 2, 4, 6, 8]
        call_user_func_array(array($query, 'bind_param'), ref($args)); // $query->bind_param("iiii", 2, 4, 6, 8)

        $query->execute();
        if ( $conexao->affected_rows != count($carrinho) ) {
            throw new Exception("Erro na contabilização do estoque!");
        }

        /* Efetiva a transação e liga o auto-commmit */
        $conexao->commit();
        $conexao->autocommit(true);

        return $codigoCompra;

    } catch (Exception $e) {
        /* Rollback de todas as operações */
        $conexao->rollback();

        throw new Exception($e->getMessage());
    }
}

function obterValorCompra($codigo) {
    $conexao = Conexao::getInstance();

    try {

        $query = $conexao->prepare(
            "select sum(p.preco) as total from produtos p " .
            "inner join comprasProdutos cp on cp.codigoProduto = p.codigo " .
            "where cp.codigoCompra = ?"
        );
        $query->bind_param("i",$codigo);
        $query->execute();
        $query->store_result();
        $query->bind_result($total);

        if ($query->num_rows > 0) {
            $query->fetch();

            if ($total == null) {
                throw new Exception("Erro ao obter os dados da compra!");
            }

            return $total;
        } else {
            throw new Exception("Erro ao obter os dados da compra!");
        }
    } catch(Exception $e) {
        throw new Exception($e->getMessage());
    }
}
