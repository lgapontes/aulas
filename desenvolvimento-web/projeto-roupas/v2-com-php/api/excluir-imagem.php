<?php

require_once __DIR__."/../model/imagem.php";
require_once __DIR__."/util.php";

if ($_SERVER["REQUEST_METHOD"] === "DELETE") {

    $body = file_get_contents("php://input");
    $dados = json_decode($body,true);

    if (!isset( $dados['codigo'])) {
        status400("Dados da imagem não recebidos!");
    }

    /* Excluir registro do banco de dados */
    $imagem = Imagem::obter($dados['codigo']);
    $nomeArquivo = $imagem->getNomeArquivo();
    $imagem->excluir();

    /* Excluir arquivo do disco */
    $arquivo = "../img/roupas/" . $nomeArquivo;
    unlink($arquivo);

    $retorno = array(
        "mensagem" => "Arquivo excluído com sucesso!"
    );
    status200($retorno);

} else {
    status405();
}
