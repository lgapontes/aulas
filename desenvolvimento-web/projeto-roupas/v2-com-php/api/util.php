<?php

function status200($json) {
    header('Content-Type: application/json');
    echo json_encode($json, JSON_UNESCAPED_UNICODE);
    die();
}

function status400($msg) {
    header("HTTP/1.1 400");
    echo '{"mensagem": "' . $msg . '"}';
    die();
}

function status404($msg) {
    header("HTTP/1.1 404");
    echo '{"mensagem": "' . $msg . '"}';
    die();
}

function status405() {
    header("HTTP/1.1 405");
    echo '{"mensagem": "Método não permitido!"}';
    die();
}
