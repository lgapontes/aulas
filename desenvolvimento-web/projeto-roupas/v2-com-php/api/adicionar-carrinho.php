<?php

session_start();

require_once __DIR__."/util.php";

if ($_SERVER["REQUEST_METHOD"] === "POST") {

    $body = file_get_contents("php://input");
    $dados = json_decode($body,true);

    if (!isset( $dados["codigo"])) {
        status400("Dados da produto não recebidos!");
    }

    $codigoProduto = $dados["codigo"];

    if ( !isset($_SESSION["carrinho"] ) ) {

        /* Carrinho não existe. Criando e salvando na sessão */
        $carrinho = array();
        array_push($carrinho,$codigoProduto);
        $_SESSION["carrinho"] = serialize($carrinho);

    } else {

        /* Adicionando item ao carrinho */
        $carrinho = unserialize($_SESSION["carrinho"]);
        array_push($carrinho,$codigoProduto);
        $_SESSION["carrinho"] = serialize($carrinho);

    }

    $retorno = array(
        "mensagem" => "Produto adicionado com sucesso!"
    );
    status200($retorno);

} else {
    status405();
}
