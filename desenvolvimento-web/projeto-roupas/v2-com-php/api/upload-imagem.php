<?php

require_once __DIR__."/../model/imagem.php";
require_once __DIR__."/util.php";

$src = $_FILES['file']['tmp_name'];
$nome = Imagem::gerarNomeArquivo($_FILES['file']['name']);
$targ = "../img/roupas/" . $nome;
move_uploaded_file($src, $targ);

try {
    $imagem = new Imagem($nome);
    $imagemSalva = $imagem->salvar();
    status200($imagemSalva);
} catch (Exception $e) {
    status400($e->getMessage());
}
