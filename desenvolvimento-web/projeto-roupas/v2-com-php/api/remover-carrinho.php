<?php

session_start();

require_once __DIR__."/util.php";

if ($_SERVER["REQUEST_METHOD"] === "DELETE") {

    $body = file_get_contents("php://input");
    $dados = json_decode($body,true);

    if (!isset( $dados["codigo"])) {
        status400("Dados da produto não recebidos!");
    }

    $codigoProduto = $dados["codigo"];

    if ( !isset($_SESSION["carrinho"] ) ) {
        status400("Erro ao encontrar o carrinho!");
    } else {

        /* Remove item ao carrinho */
        $carrinho = unserialize($_SESSION["carrinho"]);
        $index = array_search($codigoProduto,$carrinho);
        unset($carrinho[$index]);

        if ( count($carrinho) == 0 ) {
            unset($_SESSION["carrinho"]);            
        } else {
            $_SESSION["carrinho"] = serialize($carrinho);
        }

    }

    $retorno = array(
        "mensagem" => "Produto removido com sucesso!"
    );
    status200($retorno);

} else {
    status405();
}
