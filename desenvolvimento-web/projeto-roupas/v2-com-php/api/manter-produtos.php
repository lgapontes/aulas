<?php

require_once __DIR__."/../model/produto.php";
require_once __DIR__."/util.php";

function validarDadosFormulario($dados) {
    if (
        !isset( $dados["codigo"] ) ||
        !isset( $dados["descricao"] ) ||
        !isset( $dados["preco"] ) ||
        !isset( $dados["estoque"] ) ||
        !isset( $dados["imagem"] ) ||
        !isset( $dados["imagem"]["codigo"] )
    ) {
        status400("O formulário enviou dados inválidos!");
    }
}

if ($_SERVER["REQUEST_METHOD"] === "GET") {

    if ( isset($_GET["codigo"]) ) {

        // Obtém 1 produto
        try {
            $produto = Produto::obter( $_GET["codigo"] );
            status200($produtos);

        } catch (Exception $e) {
            status404($e->getMessage());
        }

    } else {

        // Obtém todos os produtos
        try {
            $produtos = Produto::listar();
            status200($produtos);
        } catch (Exception $e) {
            status404($e->getMessage());
        }

    }

} else if ($_SERVER["REQUEST_METHOD"] === "POST") {

    $body = file_get_contents("php://input");
    $dados = json_decode($body,true);
    validarDadosFormulario($dados);

    try {
        $produto = new Produto(
            $dados["descricao"],
            $dados["preco"],
            $dados["estoque"],
            $dados["imagem"]["codigo"]
        );

        $produtoInserido = $produto->inserir();
        status200($produtoInserido);

    } catch (Exception $e) {
        status400($e->getMessage());
    }

} else if ($_SERVER["REQUEST_METHOD"] === "PUT") {

    $body = file_get_contents("php://input");
    $dados = json_decode($body,true);
    validarDadosFormulario($dados);

    try {
        $produto = new Produto(
            $dados["descricao"],
            $dados["preco"],
            $dados["estoque"],
            $dados["imagem"]["codigo"]
        );
        $produto->setCodigo($dados["codigo"]);

        $produtoAlterado = $produto->atualizar();
        status200($produtoAlterado);

    } catch (Exception $e) {
        status400($e->getMessage());
    }

} else if ($_SERVER["REQUEST_METHOD"] === "DELETE") {

    $body = file_get_contents("php://input");
    $dados = json_decode($body,true);

    if ( !isset( $dados["codigo"] ) ) {
        status400("O formulário não enviou o código do produto!");
    }

    try {

        $nomeArquivoImagem = Produto::excluir($dados["codigo"]);

        /* Excluir arquivo do disco */
        $arquivo = "../img/roupas/" . $nomeArquivoImagem;
        unlink($arquivo);

        status200(array(
            "mensagem" => "Produto excluído com sucesso!"
        ));

    } catch (Exception $e) {
        status400($e->getMessage());
    }

} else {
    status405();
}
