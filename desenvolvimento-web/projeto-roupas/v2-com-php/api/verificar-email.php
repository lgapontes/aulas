<?php
header('Access-Control-Allow-Origin: *');

require_once __DIR__."/util.php";
require_once __DIR__."/../model/usuario.php";

if ( isset($_GET["email"]) ) {

    try {
        $email = $_GET["email"];
        $existe = Usuario::emailExiste($email);

        if ($existe) {
            $json = array(
                "existe" => true,
                "mensagem" => "Email já existe!"
            );
            status200($json);
        } else {
            $json = array(
                "existe" => false,
                "mensagem" => "Email disponível!"
            );
            status200($json);
        }

    } catch (Exception $e) {
        status400($e->getMessage());
    }

} else {
    status400("Email não informado!");
}
