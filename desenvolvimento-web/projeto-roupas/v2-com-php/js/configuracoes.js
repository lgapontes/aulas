
$(document).ready(function(){
    /* Obtem todos os produtos */
    $.get('api/manter-produtos.php',function(json){

        if (json.length == 0) {
            $('#lista-produtos > li > span.preload').text('Nenhum produto encontrado!');
        } else {
            var total = json.length;
            json.forEach(function(produto,index){
                novaLinha(produto,false);
                if ( (index+1) == total ) {
                    $('#lista-produtos > li > span.preload').hide();
                }
            });
        }

    }).fail(function(json){
        var retorno = JSON.parse(json.responseText);
        $('#lista-produtos > li > span.preload').text(retorno.mensagem);
    });
});

/* Realizar upload de imagens */
var inputFile = $('#file-carregar-imagem');
var inputCarregarImagem = $('#input-carregar-imagem');
var loadingCarregarImagem = $('#loading-carregar-imagem');
var mensagemFileError = $('form.editar-produto section.mensagem');

inputFile.change(function(){

    /* Esconde mensagem de erro, se houver */
    mensagemFileError.hide();

    /* Obtem arquivo selecionado */
    var file_data = inputFile.prop('files')[0];

    if (file_data["type"] === "image/png") {
        /* Arquivo da imagem válido */

        if ( file_data.size <= 200000 ) {
            /* Arquivo com tamanho válido */

            inputCarregarImagem.val( inputFile.val() );
            loadingCarregarImagem.show();

            var form_data = new FormData();
            form_data.append('file', file_data);
            $.ajax({
                url: "api/upload-imagem.php",
                type: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData:false
            }).done(function(json){

                /* Arquivo salvo */
                exibirImagem(json);

            }).fail(function(json){
                /* Falha ao enviar o arquivo */
                mensagemFileError.text(JSON.parse(json.responseText).mensagem);
                mensagemFileError.show();
                loadingCarregarImagem.hide();
            });
        } else {
            /* Tamanho do arquivo inválido */
            mensagemFileError.text('Arquivo muito grande (máximo 200 KB)');
            mensagemFileError.show();
            loadingCarregarImagem.hide();
        }

    } else {
        /* Arquivo da imagem inválido */
        mensagemFileError.text('Imagem inválida (use arquivos .png)');
        mensagemFileError.show();
        loadingCarregarImagem.hide();
    }
});

var formulario = $('form.editar-produto');
var botaoSecundario = $('#secundario');

$('main div ul li > span.editar').each(function(index){
    var botaoEditar = $(this);
    var linha = botaoEditar.parent();

    botaoEditar.click(function(){
        exibirEditarProduto(linha);
    });

    /* Código apenas enquanto não tem PHP */
    linha.find('input[name=codigo]').val(index+1);
    linha.find('input[name=estoque]').val(1);
});

$('a.novo-produto').click(function(){
    var produto = {
        codigo: "",
        descricao: "",
        preco: "",
        estoque: ""
    };
    exibirProduto(produto);

    $('form.editar-produto > h1').text("Incluir Produto");

    botaoSecundario.text('Cancelar').off().on('click',function(){
        formulario.hide();
    });
    formulario.show();
    rolarParaBaixo();
});

formulario.submit(function(event){
    event.preventDefault();

    var produto = {
        codigo: $('#codigo').val(),
        descricao: $('#descricao').val(),
        preco: parseFloat( $('#preco').val().replace(",",".") ),
        estoque: parseInt($('#estoque').val()),
        imagem: {
            codigo: parseInt( $('div.exibir-imagem > input[name=codigo-imagem]').val() ),
            nomeArquivo: $('div.exibir-imagem > input[name=nome-imagem]').val()
        }
    };

    /* Regras de negócio  */
    if (
        !produto.imagem.codigo ||
        produto.imagem.codigo === "" ||
        !produto.imagem.nomeArquivo ||
        produto.imagem.nomeArquivo === ""
    ) {
        mensagemFileError.text("Selecione uma imagem!");
        mensagemFileError.show();
        return;
    }

    if (produto.codigo === "") {

        $.post('api/manter-produtos.php',JSON.stringify(produto),function(json){

            /* Produto inserido com sucesso */
            formulario.hide();
            novaLinha(json);

        }).fail(function(json){

            /* Falha ao inserir */
            mensagemFileError.text(JSON.parse(json.responseText).mensagem);
            mensagemFileError.show();

        });

    } else {

        /* Converte o código para inteiro */
        produto.codigo = parseInt(produto.codigo);

        $.ajax({
            url: 'api/manter-produtos.php',
            method: 'PUT',
            data: JSON.stringify(produto)
        }).done(function(json){

            /* Produto atualizado com sucesso */
            formulario.hide();
            atualizarLinha(produto);

        }).fail(function(json){

            /* Falha ao atualizar */
            mensagemFileError.text(JSON.parse(json.responseText).mensagem);
            mensagemFileError.show();

        });
    }
});

function exibirProduto(produto) {
    $('#codigo').val(produto.codigo);
    $('#descricao').val(produto.descricao);
    $('#preco').val(produto.preco);
    $('#estoque').val(produto.estoque);

    if (produto.imagem != undefined && produto.imagem.nomeArquivo != undefined) {
        exibirImagem(produto.imagem);
    } else {
        exibirCarregarImagem();
    }
}

function exibirImagem(imagem) {
    $('div.carregar-imagem').hide();
    $('div.exibir-imagem').html("");

    var codigoImagem = $('<input>').attr('type','hidden').attr('name','codigo-imagem').val(imagem.codigo);
    var nomeImagem = $('<input>').attr('type','hidden').attr('name','nome-imagem').val(imagem.nomeArquivo);
    var tagImg = $('<img>').attr('src','img/roupas/' + imagem.nomeArquivo);
    var excluir = $('<a>').attr('href','#').addClass('excluir');
    excluir.on('click',function(evento){
        evento.preventDefault();

        $.ajax({
            url: 'api/excluir-imagem.php',
            method: 'DELETE',
            data: JSON.stringify({codigo: imagem.codigo})
        }).done(function(json){
            console.log(json.mensagem);
            excluir.parent().fadeOut(500,function(){
                codigoImagem.val("");
                nomeImagem.val("");
                exibirCarregarImagem();
            });
        }).fail(function(json){
            console.log(JSON.parse(json.responseText).mensagem);
        });

    });

    $('div.exibir-imagem').append(codigoImagem);
    $('div.exibir-imagem').append(nomeImagem);
    $('div.exibir-imagem').append(tagImg);
    $('div.exibir-imagem').append(excluir);

    $('div.exibir-imagem').show();
}

function exibirCarregarImagem() {
    $('#input-carregar-imagem').val("");
    $('#loading-carregar-imagem').hide();
    $('form.editar-produto section.mensagem').hide();
    $('div.exibir-imagem').hide();
    $('div.carregar-imagem').show();
}

function novaLinha(produto,novo=true) {
    var linha = $('<li>');
    var inputCodigo = $('<input>').attr('type','hidden').attr('name','codigo').val(produto.codigo);
    var inputEstoque = $('<input>').attr('type','hidden').attr('name','estoque').val(produto.estoque);
    var inputCodigoImagem = $('<input>').attr('type','hidden').attr('name','codigo-imagem').val(produto.imagem.codigo);
    var inputNomeImagem = $('<input>').attr('type','hidden').attr('name','nome-imagem').val(produto.imagem.nomeArquivo);
    var spanDescricao = $('<span>').addClass('descricao').text(produto.descricao);

    if (typeof produto.preco === 'string') {
        var precoFormatado = produto.preco;
    } else {
        var precoFormatado = produto.preco.toFixed(2).replace(".",",");
    }

    var spanPreco = $('<span>').addClass('preco').text(precoFormatado);
    var botaoEditar = $('<span>').addClass('editar');

    linha.append(inputCodigo);
    linha.append(inputEstoque);
    linha.append(inputCodigoImagem);
    linha.append(inputNomeImagem);
    linha.append(spanDescricao);
    linha.append(spanPreco);
    linha.append(botaoEditar);
    linha.hide();

    if (novo) {
        $('main div ul').append(linha);
        linha.fadeIn(2000);
    } else {
        $('main div ul').append(linha);
        linha.show();
    }

    botaoEditar.click(function(){
        exibirEditarProduto(linha);
    });
}

function atualizarLinha(produto) {
    var linha = $('main div ul li input[name=codigo][value=' + produto.codigo + ']').parent();
    linha.find('input[name=codigo]').val(produto.codigo);
    linha.find('input[name=estoque]').val(produto.estoque);
    linha.find('input[name=codigo-imagem]').val(produto.imagem.codigo);
    linha.find('input[name=nome-imagem]').val(produto.imagem.nomeArquivo);

    linha.find('span.descricao').text(produto.descricao);
    linha.find('span.preco').text(produto.preco);

    linha.hide().fadeIn(2000);
    rolarParaTag(linha);
}

function exibirEditarProduto(linha) {
    var produto = {
        codigo: linha.find('input[name=codigo]').val(),
        estoque: linha.find('input[name=estoque]').val(),
        imagem: {
            codigo: linha.find('input[name=codigo-imagem]').val(),
            nomeArquivo: linha.find('input[name=nome-imagem]').val(),
        },
        descricao: linha.find('span.descricao').text(),
        preco: linha.find('span.preco').text()
    };
    exibirProduto(produto);

    $('form.editar-produto > h1').text("Editar Produto");

    botaoSecundario.text('Excluir').off().on('click',function(){

        /* Converte o código para inteiro */
        produto.codigo = parseInt(produto.codigo);

        $.ajax({
            url: 'api/manter-produtos.php',
            method: 'DELETE',
            data: JSON.stringify(produto)
        }).done(function(json){

            /* Produto excluído com sucesso */
            formulario.hide();
            excluirProduto(linha);

        }).fail(function(json){

            /* Falha ao excluir */
            console.log(json);
            mensagemFileError.text("Erro ao excluir o produto!");
            mensagemFileError.show();

        });

    });

    formulario.show();
    rolarParaBaixo();
}

function excluirProduto(linha) {
    rolarParaTag(linha);
    $.when(linha.fadeOut(2000)).done(function(){
        linha.remove();
    });
}

function rolarParaBaixo() {
    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
}

function rolarParaTag(tag) {
    $('html,body').animate({scrollTop: tag.offset().top-100},'slow');
}
