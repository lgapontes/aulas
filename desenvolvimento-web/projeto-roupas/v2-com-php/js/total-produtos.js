
var tagTotalProdutos = $('#total-produtos');

function adicionarTotalProdutos() {
    var total = parseInt(tagTotalProdutos.text());
    total = total + 1;
    tagTotalProdutos.text(total);
    exibirTotalProdutos();
}

function removerTotalProdutos() {
    var total = parseInt(tagTotalProdutos.text());
    total = total - 1;
    tagTotalProdutos.text(total);
    exibirTotalProdutos();
}

function exibirTotalProdutos() {
    var total = parseInt(tagTotalProdutos.text());
    if (total > 0) {
        tagTotalProdutos.show();
    } else {
        tagTotalProdutos.hide();
    }
}
