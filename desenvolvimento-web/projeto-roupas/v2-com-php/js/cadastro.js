
function inicializarTooltip() {
    $('span.tooltip').tooltipster({
        timer: 3000
    });
}

$('#email').on("blur",function(){
    var email = $(this).val();

    /* Simulação consulta a API */
    var spanTooltip = $('span.tooltip');
    spanTooltip
        .removeClass('info')
        .addClass('loading')
        .tooltipster('content', 'Verificando e-mail...');

    var queryString = {email:email};

    $.get('api/verificar-email.php',queryString,function(json){

        if (json.existe) {
            spanTooltip
                .removeClass('loading success')
                .addClass('warning')
                .tooltipster('content',json.mensagem)
                .tooltipster('show');
        } else {
            spanTooltip
                .removeClass('loading warning')
                .addClass('success')
                .tooltipster('content',json.mensagem)
                .tooltipster('show');
        }

    }).fail(function(json){
        spanTooltip
            .removeClass('loading success')
            .addClass('warning')
            .tooltipster('content', JSON.parse(json.responseText).mensagem)
            .tooltipster('show');
    });
});

$(function(){
    inicializarTooltip();
});
