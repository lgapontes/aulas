function carregarImagensRoupas() {
    $('img.roupa').each(function(){
        var imagemRoupa = $(this);
        var imagemLoading = imagemRoupa.parent().find('img.loading');

        imagemRoupa.ready(function(){
            imagemLoading.addClass('loading-esconder');
            imagemRoupa.addClass('roupa-mostrar');
        });
    });
}

function configurarLinks() {
	var links = $('div.item a.adicionar');
	links.each(function(link){
        var link = $(this);
		link.on('click',function(evento){
            evento.preventDefault();
            var tagDiv = link.parent();
			var codigoProduto = tagDiv.find('input[type=hidden]').val();
			adicionarCarrinho(tagDiv, codigoProduto);
		})
	});
}

function rolarParaCima() {
    $('html, body').animate({scrollTop:0}, 'slow');
}

function adicionarCarrinho(tagDiv, codigoProduto) {
	$.post(
		'api/adicionar-carrinho.php',
		JSON.stringify({ codigo: codigoProduto }),
		function(json){

			console.log(json.mensagem);
            adicionarTotalProdutos();
			tagDiv.fadeOut(1000);

		}).fail(function(json){
			var mensagem = $('#mensagem-ajax');
			mensagem.find('span').text(
				JSON.parse(json.responseText).mensagem
			);
			mensagem.show();
			rolarParaCima();
			mensagem.fadeOut(5000);
		});
}

$(function(){
    carregarImagensRoupas();
    configurarLinks();
    exibirTotalProdutos();
});
