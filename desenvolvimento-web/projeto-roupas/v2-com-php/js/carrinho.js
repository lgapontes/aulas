$('ul li > span.excluir').each(function(){
    var botaoExcluir = $(this);
    var linha = botaoExcluir.parent();
    var codigoProduto = linha.find('input[type=hidden]').val();

    botaoExcluir.on('click',function(){
        removerCarrinho(linha, codigoProduto);
    });
});

$('form.concluir-compra').on('submit',function(evento){

});

function calcularTotal() {
    var total = 0.0;
    $('ul li > span.preco').each(function(){
        total += parseFloat($(this).text());
    });

    var footerTotal = $('main div.borda > footer');

    if (total == 0) {
        footerTotal.text('R$ 0,00').hide();
        var spanVazio = $('<span>').addClass('vazio').text('Nenhum produto encontrado!');
        $('main div.borda').append(spanVazio);
        $('form.concluir-compra').hide();
    } else {
        $('main div.borda > footer').text('R$ ' + total.toFixed(2));
    }
}

function removerCarrinho(linha, codigoProduto) {
    $.ajax({
        url: 'api/remover-carrinho.php',
        method: 'DELETE',
        data: JSON.stringify({ codigo: codigoProduto })
    }).done(function(json){
        console.log(json.mensagem);
        removerTotalProdutos();
        $.when(linha.fadeOut(1000)).done(function() {
            linha.remove();
            calcularTotal();
        });
    }).fail(function(json){
        var mensagem = $('#mensagem-ajax');
        mensagem.find('span').text(
            JSON.parse(json.responseText).mensagem
        );
        mensagem.show();
        mensagem.fadeOut(5000);
    });
}

$(function(){
    calcularTotal();
    exibirTotalProdutos();
});
