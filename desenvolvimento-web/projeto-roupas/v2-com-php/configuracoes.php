<?php
    require_once __DIR__."/lib/cabecalho.php";
?>

        <main>
            <?php
                require_once __DIR__."/lib/mensagens.php";
            ?>

            <div class="borda">
                <h1>
                    Lista de Produtos
                    <a href="#" class="novo-produto"></a>
                </h1>
                <ul id="lista-produtos">

                    <!--
                    <li>
                        <input name="codigo" type="hidden">
                        <input name="estoque" type="hidden">
                        <span class="descricao">Camiseta feminina branca</span>
                        <span class="preco">70,00</span>
                        <span class="editar"></span>
                    </li>
                    -->
                    <li>
                        <span class="preload"><img src="img/loading-black.gif" /></span>
                    </li>

                </ul>
            </div>

            <form class="editar-produto" method="POST">
                <h1>Editar Produto</h1>
                <div>
                    <label for="codigo">Código</label>
                    <input id="codigo" type="text" tabindex="1" readonly disabled>

                    <label for="descricao">Descrição</label>
                    <input id="descricao" type="text" required tabindex="2" autofocus>

                    <label for="preco">Preço</label>
                    <input id="preco" type="text" required tabindex="3"
                        onKeyPress="return(moeda(this,'.',',',event))">

                    <label for="estoque">Estoque</label>
                    <input id="estoque" type="number" step="0" min="0" required tabindex="4">

                    <div class="exibir-imagem"></div>
                    <div class="carregar-imagem">
                        <input type="file" name="file-carregar-imagem" id="file-carregar-imagem" />
                        <label for="file-carregar-imagem"><img src="img/upload.png" /></label>
                        <input type="text" id="input-carregar-imagem" disabled/>
                        <img src="img/loading-black.gif" id="loading-carregar-imagem" />
                    </div>

                    <section class="mensagem"></section>

                    <button type="submit">Salvar</button>
                    <button id="secundario" type="button">Excluir</button>
                </div>
            </form>
        </main>

        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/tooltipster.bundle.min.js"></script>
        <script type="text/javascript" src="js/configuracoes.js"></script>
        <script type="text/javascript" src="js/validar-moeda.js"></script>
    </body>
</html>
