<?php

error_reporting(E_ERROR | E_PARSE);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

class Conexao {

    private static $conexao;

    private function __construct() {}

    public static function getInstance() {
        if (!self::$conexao) {
            self::$conexao = new mysqli('127.0.0.1','roupas_user','123eja','roupas_database');
            mysqli_set_charset(self::$conexao, "utf8");
        }
        return self::$conexao;
    }

}
