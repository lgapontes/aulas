<?php
    session_start();
    require_once __DIR__."/../model/usuario.php";

    $usuarioLogado = isset($_SESSION["usuario"]);

    function totalItensCarrinho() {
        $carrinho = array();
        if ( isset($_SESSION["carrinho"]) ) {
            $carrinho = unserialize($_SESSION["carrinho"]);
        }
        return count($carrinho);
    }
?>

<!DOCTYPE HTML>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Roupas Legais</title>
        <link rel="icon" href="img/favicon.png">
        <link rel="stylesheet" type="text/css" href="css/reset.css" />
        <link href="https://fonts.googleapis.com/css?family=Bad+Script" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/tooltipster.bundle.min.css" />
        <link rel="stylesheet" type="text/css" href="css/estilo.css" />
    </head>
    <body>
        <header>
            <div>
                <a class="logo tamanho-logo" href="index.php">RL</a>
                <span class="titulo tamanho">Roupas Legais</span>
                <nav>
                    <ul>
                        <li>
                            <a href="cadastro.php">
                                <img src="img/cadastro.png" />
                                <span>Cadastro</span>
                            </a>
                        </li>
                        <li>
                            <?php
                                if ($usuarioLogado):
                            ?>
                                <a href="controller/efetuar-logout.php">
                                <img src="img/logout.png" />
                                <span>Logout</span>
                            </a>
                            <?php else: ?>
                                <a href="login.php">
                                    <img src="img/login.png" />
                                    <span>Login</span>
                                </a>
                            <?php endif; ?>
                        </li>
                        <li>
                            <a href="carrinho.php">
                                <img src="img/carrinho.png" />
                                <span>Carrinho</span>
                                <aside id="total-produtos" class="total-produtos"><?= totalItensCarrinho() ?></aside>
                            </a>
                        </li>

                        <?php
                            if ($usuarioLogado) {
                                $usuario = unserialize($_SESSION["usuario"]);
                                if (strcmp($usuario->getPerfil()->getDescricao(),"Vendedor") == 0) {
                                    ?>
                                        <li>
                                            <a href="configuracoes.php">
                                                <img src="img/configuracoes.png" />
                                                <span>Configurações</span>
                                            </a>
                                        </li>
                                    <?php
                                }
                            }
                        ?>

                    </ul>
                </nav>
            </div>
        </header>
