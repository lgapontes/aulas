<?php
    if ( isset($_SESSION["mensagem-erro"]) ):
        $mensagemErro = $_SESSION['mensagem-erro'];
        unset($_SESSION['mensagem-erro']);
?>
    <section class="mensagem warning">
        <img src="img/warning.png">
        <?= $mensagemErro ?>
    </section>
<?php endif; ?>

<?php
    if ( isset($_SESSION["mensagem-sucesso"]) ):
        $mensagemSucesso = $_SESSION['mensagem-sucesso'];
        unset($_SESSION['mensagem-sucesso']);
?>
    <section class="mensagem success">
        <img src="img/success.png">
        <?= $mensagemSucesso ?>

        <?php
            if (isset($_SESSION["compra"])):
        ?>
            <a target="_blank" href="controller/boleto/imprimir.php">
                <img src="img/impressora.png">
                Clique aqui para gerar o boleto!
            </a>
        <?php endif; ?>

    </section>
<?php endif; ?>
