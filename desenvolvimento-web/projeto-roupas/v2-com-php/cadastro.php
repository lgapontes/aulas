<?php
    require_once __DIR__."/lib/cabecalho.php";
    require_once __DIR__."/model/perfil.php";
?>

        <main>
            <?php
                require_once __DIR__."/lib/mensagens.php";
            ?>

            <form method="POST" action="controller/cadastrar-usuario.php">
                <h1>Novo Usuário</h1>
                <div>
                    <label for="email">E-mail</label>
                    <input id="email" name="email" class="validacao" type="email" required>
                    <span class="info tooltip" title="O e-mail será verificado."></span>

                    <label for="nome">Nome</label>
                    <input id="nome" name="nome" type="text" required>

                    <label for="senha">Senha</label>
                    <input id="senha" name="senha" type="password" required>

                    <label for="confirmarSenha">Confirmar</label>
                    <input id="confirmarSenha" name="confirmarSenha" type="password" required>

                    <label for="perfil">Perfil</label>
                    <select id="perfil" name="perfil" required>
                        <?php
                            $perfis = Perfil::listar();
                            foreach($perfis as $perfil) {
                                echo '<option value="' . $perfil->getCodigo() . '">' . $perfil->getDescricao() . '</option>';
                            }
                        ?>
                    </select>

                    <button type="submit">Cadastrar</button>
                </div>
            </form>
        </main>

        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/tooltipster.bundle.min.js"></script>
        <script type="text/javascript" src="js/cadastro.js"></script>
    </body>
</html>
