<?php

session_start();
require_once __DIR__."/../model/usuario.php";

if (
    !isset($_POST["email"]) ||
    !isset($_POST["senha"])
) {
    $_SESSION["mensagem-erro"] = "Dados de login não enviados!";
    header("Location: ../login.php");
    die();
}

try {

    $usuario = Usuario::login($_POST["email"],$_POST["senha"]);
    $_SESSION["usuario"] = serialize($usuario);

    $_SESSION["mensagem-sucesso"] = "Seja bem-vindo " . $usuario->getNome() . "!";

    if ( isset($_SESSION["carrinho"]) ) {
        header("Location: ../carrinho.php");
        die();
    } else {
        header("Location: ../index.php");
        die();
    }

} catch (Exception $e) {
    $_SESSION["mensagem-erro"] = $e->getMessage();
    header("Location: ../login.php");
    die();
}
