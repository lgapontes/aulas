<?php

require_once __DIR__."/../model/usuario.php";

session_start();

if (
    !isset($_POST["email"]) ||
    !isset($_POST["nome"]) ||
    !isset($_POST["perfil"]) ||
    !isset($_POST["senha"]) ||
    !isset($_POST["confirmarSenha"])
) {
    $_SESSION["mensagem-erro"] = "Dados do formulário não enviados!";
    header("Location: ../cadastro.php");
    die();
}

try {

    $email = $_POST["email"];

    $usuario = new Usuario(
        $email,
        $_POST["nome"],
        $_POST["perfil"]
    );

    // Definir e validar senha
    $usuario->definirSenha($_POST["senha"],$_POST["confirmarSenha"]);

    // Validar email
    $existe = Usuario::emailExiste($email);
    if ($existe) {
        $_SESSION["mensagem-erro"] = "Email já existe!";
        header("Location: ../cadastro.php");
        die();
    }

    // Salva dados do usuário
    $usuarioInserido = $usuario->salvar();
    $_SESSION["mensagem-sucesso"] = "Usuário inserido com sucesso!";
    header("Location: ../login.php");
    die();

} catch (Exception $e) {
    $_SESSION["mensagem-erro"] = $e->getMessage();
    header("Location: ../cadastro.php");
    die();
}
