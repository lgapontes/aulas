<?php

require_once __DIR__."/../model/produto.php";
require_once __DIR__."/../model/usuario.php";

session_start();
$usuarioLogado = isset($_SESSION["usuario"]);

if ($usuarioLogado) {

    if ( isset($_SESSION["carrinho"]) ) {
        try {

            /* Obtendo código dos produtos do carrinho */
            $carrinho = unserialize($_SESSION["carrinho"]);

            /* Obtendo usuário da sessão */
            $usuario = unserialize($_SESSION["usuario"]);

            /* Realizando compra */
            $codigoCompra = $usuario->realizarCompra($carrinho);

            /* Compra concluída */
            $_SESSION["compra"] = $codigoCompra;
            unset($_SESSION["carrinho"]);
            $_SESSION["mensagem-sucesso"] = "Compra realizada com sucesso!";
            header("Location: ../carrinho.php");
            die();

        } catch (Exception $e) {
            $_SESSION["mensagem-erro"] = $e->getMessage();
            header("Location: ../carrinho.php");
            die();
        }


    } else {
        $_SESSION["mensagem-erro"] = "Erro ao localizar o carrinho!";
        header("Location: ../login.php");
        die();
    }

} else {
    $_SESSION["mensagem-erro"] = "Realize o login antes de comprar!";
    header("Location: ../login.php");
    die();
}
