<?php
    require_once __DIR__."/lib/cabecalho.php";
    require_once __DIR__."/model/produto.php";
?>

        <main>
            <?php
                require_once __DIR__."/lib/mensagens.php";
            ?>

            <section id="mensagem-ajax" class="mensagem warning invisivel">
                <img src="img/warning.png">
                <span></span>
            </section>

            <div class="itens">
            <?php
                $produtos = Produto::listarComEstoque();

                $carrinho = array();
                if ( isset($_SESSION["carrinho"]) ) {
                    $carrinho = unserialize($_SESSION["carrinho"]);
                }

                foreach($produtos as $produto):
                    if ( !in_array($produto->getCodigo(), $carrinho) ):
                    ?>
                        <div class="item">
                           <input type="hidden" value="<?= $produto->getCodigo() ?>" />
                           <img class="roupa" src="img/roupas/<?= $produto->getImagem()->getNomeArquivo() ?>" />
                           <img class="loading" src="img/loading.gif" />
                           <a class="adicionar" href="#"></a>
                           <span class="nome"><?= $produto->getDescricao() ?></span>
                           <span class="preco"><?= number_format($produto->getPreco(), 2, ',', '.') ?></span>
                        </div>
                    <?php
                    endif;
                endforeach;
            ?>

              <!--
               <div class="item">
                   <img class="roupa" src="img/roupas/fac30f11515aa796ce39d4cb5c9fabbe8ec76426c3f606698988b9eab7bd902e.png" />
                   <img class="loading" src="img/loading.gif" />
                   <a class="adicionar" href="#"></a>
                   <span class="nome">Camiseta feminina branca</span>
                   <span class="preco">R$ 70,00</span>
               </div>
             -->

            </div>
        </main>

        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/tooltipster.bundle.min.js"></script>
        <script type="text/javascript" src="js/total-produtos.js"></script>
        <script type="text/javascript" src="js/index.js"></script>
    </body>
</html>
