<?php
    require_once __DIR__."/lib/cabecalho.php";
    require_once __DIR__."/model/produto.php";
?>

        <main>
            <?php
                require_once __DIR__."/lib/mensagens.php";
            ?>

            <section id="mensagem-ajax" class="mensagem warning invisivel">
                <img src="img/warning.png">
                <span></span>
            </section>

            <div class="borda">
                <h1>Carrinho</h1>
                <ul>
                    <?php
                        if ( isset($_SESSION["carrinho"]) ):
                            $carrinho = unserialize($_SESSION["carrinho"]);
                            $produtos = Produto::listarPorCodigo($carrinho);

                            foreach ($produtos as $produto):
                            ?>
                                <li>
                                    <input type="hidden" value="<?= $produto->getCodigo() ?>" />
                                    <span class="descricao"><?= $produto->getDescricao() ?></span>
                                    <span class="preco"><?= number_format($produto->getPreco(), 2, ',', '.') ?></span>
                                    <span class="excluir"></span>
                                </li>
                            <?php
                            endforeach;
                        endif;
                    ?>
                    <!--
                    <li>
                        <span class="descricao">Camiseta feminina branca</span>
                        <span class="preco">70,00</span>
                        <span class="excluir"></span>
                    </li>
                    -->
                </ul>
                <footer>Calculando...</footer>
            </div>

            <?php
                if ( isset($_SESSION["carrinho"]) ):
                ?>
                    <form class="concluir-compra" method="POST" action="controller/concluir-compra.php">
                        <button type="submit">
                            Concluir Compra
                            <img src="img/moeda.png">
                        </button>
                    </form>
                <?php
                endif;
            ?>
        </main>

        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/tooltipster.bundle.min.js"></script>
        <script type="text/javascript" src="js/total-produtos.js"></script>
        <script type="text/javascript" src="js/carrinho.js"></script>
    </body>
</html>
