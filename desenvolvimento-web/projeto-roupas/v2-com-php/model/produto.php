<?php

require_once __DIR__."/../repositorio/repositorio-produto.php";
require_once __DIR__."/imagem.php";

class Produto implements JsonSerializable {

    private $codigo;
    private $descricao;
    private $preco;
    private $estoque;
    private $imagem;

    public function __construct($descricao,$preco,$estoque,$imagem) {

        // Validação da descrição
        if ( strlen($descricao) == 0 ) {
            throw new Exception("Descrição inválida!");
        }
        $this->descricao = $descricao;

        // Validação do preço. Vide http://php.net/filter_var
        $precoConvertido = (float) $preco;
        if (filter_var($precoConvertido, FILTER_VALIDATE_FLOAT) && $precoConvertido > 0) {
            $this->preco = $precoConvertido;
        }
        else {
            throw new Exception("Preço deve ser maior que zero!");
        }

        // Validação do estoque
        $estoqueConvertido = intval($estoque);
        if ($estoqueConvertido >= 0) {
            $this->estoque = $estoqueConvertido;
        } else {
            throw new Exception("Estoque deve ser zero ou maior que zero!");
        }

        // Se for informado um código, obtem a imagem do banco
        // Caso contrário, define a própria imagem
        if ( is_int($imagem) ) {
            $this->imagem = Imagem::obter($imagem);
        } else {
            $this->imagem = $imagem;
        }
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function getDescricao() {
        return $this->descricao;
    }
    public function getPreco() {
        return $this->preco;
    }
    public function getEstoque() {
        return $this->estoque;
    }
    public function getImagem() {
        return $this->imagem;
    }

    public static function listar() {
        return listarProdutos(false);
    }

    public static function listarComEstoque() {
        return listarProdutos(true);
    }

    public static function listarPorCodigo($codigos) {
        return listarProdutosPorCodigo($codigos);
    }

    public static function obter($codigo) {
        return obterProduto($codigo);
    }

    public function inserir() {
        return inserirProduto($this);
    }

    public function atualizar() {
        return atualizarProduto($this);
    }

    public static function excluir($codigoProduto) {
        return excluirProduto($codigoProduto);
    }

    public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }
}
