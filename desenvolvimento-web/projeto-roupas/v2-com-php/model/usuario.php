<?php

require_once __DIR__."/../repositorio/repositorio-usuario.php";
require_once __DIR__."/../repositorio/repositorio-produto.php";
require_once __DIR__."/perfil.php";
require_once __DIR__."/produto.php";

class Usuario implements JsonSerializable {

    private $codigo;
    private $email;
    private $nome;
    private $senha;
    private $perfil;

    public function __construct($email,$nome,$codigoPerfil) {
        $this->email = $email;
        $this->nome = $nome;
        $this->perfil = Perfil::obter($codigoPerfil);
    }

    public function definirSenha($senha,$confirmacao) {
        if ($senha != $confirmacao) {
            throw new Exception("Confirmação de senha não confere!");
        }
        $this->senha = hash("sha256",$senha);
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function getEmail() {
        return $this->email;
    }
    public function getNome() {
        return $this->nome;
    }
    public function getPerfil() {
        return $this->perfil;
    }
    public function getHashSenha() {
        return $this->senha;
    }

    public function salvar() {
        return inserirUsuario($this);
    }

    public static function login($email,$senha) {
        $hashDaSenha = hash("sha256",$senha);
        return loginDoUsuario($email,$hashDaSenha);
    }

    public static function obter($codigo) {
        return obterUsuario($codigo);
    }

    public static function emailExiste($email) {
        if ( !isset($email) || strlen($email) == 0 ) {
            throw new Exception("Email inválido!");
        }

        // https://www.w3schools.com/php/php_form_url_email.asp
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception("Email inválido!");
        }

        return emailExiste($email);
    }

    public function realizarCompra($carrinho) {
        $produtos = Produto::listarPorCodigo($carrinho);
        foreach($produtos as $produto) {
            if ($produto->getEstoque() <= 0) {
                throw new Exception("Produto de código " . $produto->getCodigo() . " sem estoque!");
            }
        }

        $codigoCompra = realizarCompra($this->getCodigo(),$carrinho);
        return $codigoCompra;
    }

    public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }
}
