<?php

require_once __DIR__."/../repositorio/repositorio-perfil.php";

class Perfil implements JsonSerializable {

    private $codigo;
    private $descricao;

    public function __construct($codigo,$descricao) {
        $this->codigo = $codigo;
        $this->descricao = $descricao;
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function getDescricao() {
        return $this->descricao;
    }

    public static function listar() {
        return listarPerfis();
    }

    public static function obter($codigo) {
        return obterPerfil($codigo);
    }

    public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }
}
