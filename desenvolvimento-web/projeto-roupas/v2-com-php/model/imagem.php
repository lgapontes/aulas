<?php

require_once __DIR__."/../repositorio/repositorio-produto.php";

class Imagem implements JsonSerializable {

    private $codigo;
    private $nomeArquivo;

    public static function gerarNomeArquivo($nomeOriginal) {
        $nome = strval(time()) . "-" . strval(rand (0 , 9999)) . "-" . $nomeOriginal;
        return hash('sha256',$nome) . ".png";
    }

    public function __construct($nomeArquivo) {
        $this->nomeArquivo = $nomeArquivo;
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }
    public function getNomeArquivo() {
        return $this->nomeArquivo;
    }

    public function salvar() {
        return inserirImagem($this->nomeArquivo);
    }

    public static function obter($codigoImagem) {
        return obterImagem($codigoImagem);
    }

    public function excluir() {
        excluirImagem($this->codigo);
    }

    public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }
}
