$('#botao-logar').on('click',function(){
    var input = $('#input-nome');
    var nome = input.val();

    if (nome == "") {
        // Falha
        $('#falha').stop().show().fadeOut(3000);
    } else {
        // Sucesso
        input.attr('disabled',true);
        $(this).attr('disabled',true);
        var texto = 'Seja bem-vindo ' + nome + '!';            
        $('#sucesso').text(texto).show().fadeOut(3000);
    }
});