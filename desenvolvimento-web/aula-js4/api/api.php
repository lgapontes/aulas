<?php

    function retornarMensagem($mensagem) {
        $retorno = array();
        $retorno['mensagem'] = $mensagem;
        header('Content-Type: application/json');    
        echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
    }

    $banco = new mysqli('linu.com.br','linuc318_caduser','123eja','linuc318_cadastro_database');
    //$banco = new mysqli('localhost','cadastro_user','123eja','cadastro_database');

    if ($_SERVER['REQUEST_METHOD'] == 'GET') {

        if ( isset($_GET['cpf']) ) {

            $cpf = $_GET['cpf'];

            $resultado = $banco->query("select cpf, nome from usuarios where cpf = '{$cpf}'");

            if ($resultado->num_rows == 1) {
                $usuario = $resultado->fetch_assoc();
                header('Content-Type: application/json');
                echo json_encode($usuario, JSON_UNESCAPED_UNICODE);
            } else {
                header('HTTP/1.1 404');            
            }

        } else {

            $usuarios = array();
            
            $resultado = $banco->query('select cpf, nome from usuarios');
            
            if ($resultado->num_rows > 0) {
                while($usuario = $resultado->fetch_assoc()) {
                    array_push($usuarios,$usuario);
                }
            }

            header('Content-Type: application/json');
            echo json_encode($usuarios, JSON_UNESCAPED_UNICODE);  
        
        }

    } else if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        $body = file_get_contents("php://input");
        $json = json_decode($body,true);
        
        $comando = $banco->prepare("insert into usuarios (cpf,nome) values ('{$json['cpf']}','{$json['nome']}')");

        if (!$comando->execute()) {
            retornarMensagem("Não foi possível inserir o registro!");
        }

        retornarMensagem("Usuário cadastrado com sucesso!");

    } else if ($_SERVER['REQUEST_METHOD'] == 'PUT') {

        $body = file_get_contents("php://input");
        $json = json_decode($body,true);
        
        $banco->query("update usuarios set nome = '{$json['nome']}' where cpf = '{$json['cpf']}'");

        if ($banco->affected_rows == 0) {
            retornarMensagem("Não foi possível atualizar o registro!");
        }

        retornarMensagem("Usuário atualizado com sucesso!");

    } else if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {

        $body = file_get_contents("php://input");
        $json = json_decode($body,true);
        
        $banco->query("delete from usuarios where cpf = '{$json['cpf']}'");

        if ($banco->affected_rows == 0) {
            retornarMensagem("Não foi possível excluir o registro!");
        }

        retornarMensagem("Usuário excluído com sucesso!");
        
    }

    $banco->close();