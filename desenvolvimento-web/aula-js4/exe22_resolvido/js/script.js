$('#botao-logar').on('click',function(){
    var input = $('#input-nome');
    var nome = input.val();

    if (nome == "") {
        // Falha
        $('#falha').stop().show().fadeOut(3000);
    } else {
        // Sucesso
        input.attr('disabled',true);
        $(this).attr('disabled',true);
        var texto = 'Seja bem-vindo ' + nome + '!';            
        $('#sucesso').text(texto).show().fadeOut(3000);

        renderizarSistema();
    }
});

function renderizarSistema() {
    var divInputNome = $('#div-input-nome');
    divInputNome.toggleClass('is-loading');
    
    $.get('http://lgapontes.com/aulas/todoapp/api/tipos',function(tipos){        
        tipos.forEach(function(tipo,index){
            renderizarRadio(tipo,index == 0);
        });

        divInputNome.toggleClass('is-loading');
        $('#div-sistema').show();
    });
}

function renderizarRadio(tipo,checked) {
    var tagLabel = $('<label>').addClass('radio');
    var tagInput = $('<input>')
        .attr('type','radio')
        .attr('name','tipo')
        .attr('checked',checked)
        .val(tipo.codigo);        
    var tagDescricao = $('<span>').text(tipo.descricao);

    tagLabel.append(tagInput);
    tagLabel.append(tagDescricao);

    $('#div-tipos').append(tagLabel);
}

function renderizarTarefa(tarefa) {
    var tagDivColumn = $('<div>').addClass('column');
    var tagDivCard = $('<div>').addClass('card');
    var tagDivCardContent = $('<div>').addClass('card-content');
    var tagDivContent = $('<div>').addClass('content');

    var spanTexto = $('<span>').text(tarefa.texto);
    var spanTipo = $('<span>').addClass('tag is-light').text(tarefa.tipo);
    var spanData = $('<span>').addClass('is-size-7 has-text-grey-light').text(tarefa.dataCriacao);    
    tagDivContent.append(spanTexto)
    tagDivContent.append($('<br>'))
    tagDivContent.append(spanTipo)
    tagDivContent.append(spanData);
    tagDivCardContent.append(tagDivContent);
    tagDivCard.append(tagDivCardContent);

    var tagFooter = $('<footer>').addClass('card-footer');
    var linkApagar = $('<a>')
        .addClass('card-footer-item has-background-light has-text-grey-dark')
        .attr('href',"#")
        .text('Apagar');        
    var linkFeita = $('<a>')
        .addClass('card-footer-item has-background-success has-text-white')
        .attr('href',"#")
        .append(
            $('<strong>').text('Feita!')
        );
    tagFooter.append(linkApagar);
    tagFooter.append(linkFeita);
    tagDivCard.append(tagFooter);

    tagDivColumn.append(tagDivCard);
    tagDivColumn.click(function(evento){
        evento.preventDefault();

        if (evento.target == linkApagar.get(0)) {
            $(this).remove();
        }
        if (evento.target == linkFeita.get(0)) {
            $(this).remove();
        }
    });

    $('#tarefas').append(tagDivColumn);
}

$('form').on('submit',function(evento){
    evento.preventDefault();
    console.log('teste');

    var tarefa = {
        tipo: $(this).find('input[type=radio]:checked').val(),
        texto: $(this).find('textarea').val(),
        dataCriacao: (new Date()).toLocaleString()
    };
    renderizarTarefa(tarefa);
});