<?php

class Usuario {
    private $codigo;
    private $login;

    public function __construct($codigo,$login) {
        $this->codigo = $codigo;
        $this->login = $login;
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function getLogin() {
        return $this->login;
    }
}
