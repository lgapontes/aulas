<?php

require_once "conexao.php";
require_once "modelo.php";

function listarUsuarios() {
    $conexao = conectar();
    $resultado = $conexao->query('select codigo, login from usuarios');

    $lista = array();

    while ($linha = $resultado->fetch_assoc()) {
        $usuario = new Usuario(
            $linha['codigo'],
            $linha['login']
        );
        array_push($lista,$usuario);
    }

    return $lista;
}
