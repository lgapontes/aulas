<?php

require_once "conexao.php";
require_once "repositorio.php";

if (isset($_POST['codigo'])) {

    $repositorio = new Repositorio($conexao);
    $codigo = $_POST['codigo'];
    
    $resultadoExclusao = $repositorio->excluir($codigo);
    if ($resultadoExclusao) {
        header("Location: index.php?mensagem=3");
        die();
    } else {
        header("Location: index.php?mensagem=2");
        die();
    }

} else {
    header("Location: index.php?mensagem=2");
    die();
}
