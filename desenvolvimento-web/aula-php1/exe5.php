<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <style>
            header {
                display: block;
                line-height: 50px;
                width: 100%;
                position: fixed;
                left: 0;
                top: 0;
                background-color: #3f4354;
                box-shadow: 0 3px 3px black;
                color: white;
                font-size: 1.4em;
                text-align: center;
                font-weight: bold;
            }
            main {
                margin-top: 60px;
                font-size: 1.4em;
            }
        </style>
    </head>
    <body>
        <?php
            require_once 'cabecalho.php';
        ?>
        <main>
            <?php
                $conexao = new mysqli('127.0.0.1','roupas_user','123eja','roupas_database');

                $resultado = $conexao->query('select * from perfis');

                if ($resultado->num_rows > 0) {
                    while ($linha = $resultado->fetch_assoc()) {
                        echo $linha["codigo"] . " - " . $linha["descricao"] . "<br />";
                    }
                }

                $conexao->close();
            ?>
        </main>
    </body>
</html>
