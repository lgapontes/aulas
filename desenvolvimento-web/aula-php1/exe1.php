<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <style>
            main {
                font-size: 1.4em;
            }
            main > span {
                display: block;
                box-sizing: border-box;
                padding: 5px;
                float: left;
                width: 100%;
                margin-top: 5px;
                background-color: #e2e2e2;
            }
        </style>
    </head>
    <body>
        <main>
            <?php
                $nome = 'Fulano';
            ?>
            <span>
                <strong>Exibição 1:</strong>
                <?php
                    echo $nome;
                ?>
            </span>
            <span>
                <strong>Exibição 2:</strong>
                <?= $nome ?>
            </span>
            <span>
                <?php
                    for ($i=0; $i<10; $i++) {
                        echo $i . ' ';
                    }
                ?>
            </span>
            <?php
                $variavel = false;
                if ($variavel) {
                    ?>
                        <span>Legal!</span>
                    <?php
                } else {
                    ?>
                        <span>Ups...</span>
                    <?php
                }
            ?>
        </main>
    </body>
</html>
