<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <style>
            main {
                font-size: 1.4em;
            }
            main > span {
                display: block;
                box-sizing: border-box;
                padding: 5px;
                float: left;
                width: 100%;
                margin-top: 5px;
                background-color: #e2e2e2;
            }
        </style>
    </head>
    <?php
        // Exemplo de classe
        class Pessoa {
            private $nome;
            private $idade;
            public function __construct($nome) {
                $this->nome = $nome;
            }
            public function getNome() {
                return $this->nome;
            }
            public function getIdade() {
                return $this->idade;
            }
            public function setIdade($idade) {
                if ($idade > 0) {
                    $this->idade = $idade;
                }
            }
        }

        $joao = new Pessoa("João");
        $maria = new Pessoa("Maria");
        $maria->setIdade(20);
    ?>
    <body>
        <main>
            <span>
                <strong>Pessoa 1:</strong> <?= $joao->getNome() ?>
            </span>
            <span>
                <strong>Pessoa 2:</strong> <?= $maria->getNome() . ' com idade de ' . $maria->getIdade() ?>
            </span>
            <span>
                <?= $maria->nome ?>
            </span>
        </main>
    </body>
</html>
