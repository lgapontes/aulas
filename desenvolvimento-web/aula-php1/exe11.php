<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <style>

        </style>
    </head>
    <body>
        <?php

            if (isset($_GET['nome'])) {
                $nome = $_GET['nome'];
                echo "<strong>Valor do nome passado por query string:</strong> " . $nome;
            } else {
                echo "Nenhum nome informado!";
            }

            echo "<br>";

            if (isset($_GET['idade'])) {
                $idade = $_GET['idade'];
                echo "<strong>Valor de idade passado por query string:</strong> " . $idade;
            } else {
                echo "Nenhuma idade informada!";
            }

        ?>
    </body>
</html>
