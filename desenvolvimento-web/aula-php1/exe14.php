<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <style>
            form {
                box-sizing: border-box;
                width: 100%;
                max-width: 400px;
                margin: 0 auto;
                background-color: #e4e4e4;
                padding: 6px;
            }
            form::after {
                content: "";
                height: 0;
                display: block;
                clear: both;
            }
            input {
                box-sizing: border-box;
                display: block;
                float: right;
                width: 68%;
            }
            label {
                box-sizing: border-box;
                display: block;
                float: left;
                width: 28%;
            }
            div.form-item {
                display: block;
                box-sizing: border-box;
                width: 100%;
                height: 24px;
            }
            button {
                float: right;
            }

            span {
                max-width: 400px;
                display: block;
                box-sizing: border-box;
                margin: 6px auto;
                text-align: center;
                line-height: 26px;
            }

            span.falha {
                background-color: #fba92d;
                color: #78200d;
                border: 1px solid #78200d;
            }
            span.sucesso {
                background-color: #c0f4aa;;
                color: #4b6a23;
                border: 1px solid #4b6a23;
            }
        </style>
    </head>
    <body>
        <?php
            $mostrarFormulario = true;

            if (isset($_GET['mensagem'])) {
                $mensagem = $_GET['mensagem'];

                switch ($mensagem) {
                    case 0:
                        echo '<span class="falha">Usuário ou senha inválidos!</span>';
                        break;
                    case 1:
                        echo '<span class="sucesso">Login efetuado com sucesso!</span>';
                        $mostrarFormulario = false;
                        break;
                }
            }

            if ($mostrarFormulario):
        ?>
            <form method="POST" action="exe14-login.php">
                <div class="form-item">
                    <label for="login">Login:</label>
                    <input name="login" id="login" type="text" value="lgapontes" />
                </div>

                <div class="form-item">
                    <label for="senha">Senha:</label>
                    <input name="senha" id="senha" type="password" />
                </div>

                <button>Enviar</button>
            </form>
        <?php endif; ?>

    </body>
</html>
