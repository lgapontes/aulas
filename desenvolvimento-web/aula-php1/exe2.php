<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <style>
            main {
                font-size: 1.4em;
            }
            main > span {
                display: block;
                box-sizing: border-box;
                padding: 5px;
                float: left;
                width: 100%;
                margin-top: 5px;
                background-color: #e2e2e2;
            }
        </style>
    </head>
    <?php
        // Exemplo de função
        function somar($n1,$n2) {
            return $n1 + $n2;
        }
        $resultado = somar(5,10);

        // Arrays
        $frutas = array(
            0 => "Banana",
            1 => "Maça",
            2 => "Pera",
            "outro" => "Laranja"
        );
        $frutas["mais1"] = "Uva";
    ?>
    <body>
        <main>
            <span>
                <strong>Resultado da soma: <?= $resultado ?></strong>
            </span>
            <?php foreach ($frutas as $chave => $valor): ?>
                <span><?= $chave . " - " . $valor ?></span>
            <?php endforeach; ?>
        </main>
    </body>
</html>
