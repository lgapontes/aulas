<?php

require_once "banco.php";
require_once "modelo.php";

function listar() {
    $perfis = array();
    $conexao = conectar();
    $resultado = $conexao->query("select * from perfis order by codigo");

    if ($resultado->num_rows > 0) {
        while($linha = $resultado->fetch_assoc()) {
            $perfil = new Perfil($linha["codigo"],$linha["descricao"]);
            array_push($perfis,$perfil);
        }
    }
    return $perfis;
}

function obter($codigo) {

    $conexao = conectar();
    $resultado = $conexao->query("select * from perfis where codigo = " . $codigo);

    if ($resultado->num_rows > 0) {
        $linha = $resultado->fetch_assoc();
        $perfil = new Perfil($linha["codigo"],$linha["descricao"]);
        return $perfil;
    } else {
        return null;
    }

}

function inserir($descricao) {

    $conexao = conectar();
    $sql = "insert into perfis (descricao) values (?)";
    $query = $conexao->prepare($sql);
    $query->bind_param("s",$descricao);
    $query->execute();

}
