<?php

require_once "repositorio.php";

if ( isset($_POST['descricao']) ) {

    $descricao = $_POST['descricao'];

    inserir($descricao);

    header('Location: perfis.php');
    die();
} else {
    echo "Erro no envio do formulário!";
}
