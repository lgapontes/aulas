<?php

class Perfil {

    private $codigo;
    private $descricao;

    public function __construct($codigo,$descricao) {
        $this->codigo = $codigo;
        $this->descricao = $descricao;
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function getDescricao() {
        return $this->descricao;
    }

}
