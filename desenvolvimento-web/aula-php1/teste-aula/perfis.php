<!DOCTYPE HTML>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8" />
        <title>Perfis</title>
    </head>
    <body>
        <form action="controller.php" method="POST">
            <input type="text" name="descricao2" placeholder="Descrição">
            <button>Enviar</button>
        </form>

        <?php
            require_once "repositorio.php";
            if ( isset($_GET["codigo"]) ):
                $codigo = $_GET["codigo"];
                $perfil = obter($codigo);
                echo "Um único perfil: " . $perfil->getCodigo() . " - " . $perfil->getDescricao();
            else:
        ?>
            <ul>
                <?php
                    $perfis = listar();
                    foreach($perfis as $perfil) {
                        echo "<li>" .  $perfil->getCodigo() . " - " . $perfil->getDescricao() . "</li>";
                    }
                ?>
            </ul>
        <?php endif; ?>

    </body>
</html>
