<?php
    require_once "conexao.php";
    require_once "repositorio.php";
    $repositorio = new Repositorio($conexao);
?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="estilo.css" />
    </head>
    <body>
        <header>
            Cadastro de Perfis
        </header>
        <main>
            <?php
                // Controller para excluir
                $codigoExcluir = $_GET['excluir'];
                if ($codigoExcluir) {
                    $resultadoExclusao = $repositorio->excluir($codigoExcluir);
                    if ($resultadoExclusao) {
                        echo '<span class="mensagem">Registro ' . $codigoExcluir . ' excluído!</span>';
                    } else {
                        echo '<span class="mensagem">Nenhum registro foi excluído!</span>';
                    }
                }

                // Controler para incluir
                $descricao = $_POST['descricao'];
                if ($descricao) {
                    $resultadoInclusao = $repositorio->incluir($descricao);
                    if ($resultadoInclusao) {
                        echo '<span class="mensagem">Registro incluído com sucesso!</span>';
                    } else {
                        echo '<span class="mensagem">Não foi possível incluir!</span>';
                    }
                }
            ?>
            <table>
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Descrição</th>
                        <th>Opções</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $perfis = $repositorio->listar();
                        foreach ($perfis as $perfil):
                    ?>
                        <tr>
                            <td><?= $perfil->getCodigo() ?></td>
                            <td><?= $perfil->getDescricao() ?></td>
                            <td><a href="index.php?excluir=<?= $perfil->getCodigo() ?>">Excluir</a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <br />
            <form action="index.php" method="POST">
                <fieldset>
                    <legend>Novo Perfil</legend>
                    <label for="descricao">Descrição</label>
                    <input name="descricao" id="descricao" type="text" placeholder="Descrição do perfil"/>
                    <button>Salvar</button>
                </fieldset>
            </form>
        </main>
    </body>
</html>
