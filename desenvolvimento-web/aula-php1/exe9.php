<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <style>
            header {
                display: block;
                line-height: 50px;
                width: 100%;
                position: fixed;
                left: 0;
                top: 0;
                background-color: #3f4354;
                box-shadow: 0 3px 3px black;
                color: white;
                font-size: 1.4em;
                text-align: center;
                font-weight: bold;
            }
            main {
                margin-top: 60px;
                font-size: 1.4em;
            }
            span {
                color: #9c0d2f;
            }
        </style>
    </head>
    <body>
        <?php
            require_once 'cabecalho.php';
        ?>
        <main>
            <?php
                mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
                try {
                    $conexao = new mysqli('127.0.0.1','roupas_user','123eja','roupas_database');
                    $conexao->query("delete from perfis where descricao = 'SuperUser'");

                    if ($conexao->affected_rows > 0) {
                        echo "Registro excluído com sucesso!";
                    } else {
                        echo "Nenhum registro foi excluído!";
                    }
                    $conexao->close();
                } catch(Exception $erro) {
                    echo '<span><strong>Mensagem de erro:</strong> ' . $erro->getMessage() . '</span>';
                }
            ?>
        </main>
    </body>
</html>
