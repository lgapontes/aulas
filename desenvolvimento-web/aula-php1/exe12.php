<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <style>
            strong.erro {
                color: red;
            }
            strong.sucesso {
                color: blue;
            }
        </style>
    </head>
    <body>
        <?php

            if (isset($_GET['senha'])) {
                $senha = $_GET['senha'];
                if ($senha == '123eja') {
                    echo '<strong class="sucesso">Senha válida!</strong>';
                } else {
                    echo '<strong class="erro">Senha inválida!</strong>';
                }
            }

        ?>
        <form>
            <label for="senha">Senha:</label>
            <input name="senha" id="senha" type="password" />
            <button>Enviar</button>
        </form>
    </body>
</html>
