<?php
    session_start();

    $usuarioLogado = false;
    if ( isset($_SESSION['usuario']) ) {
        $usuarioLogado = true;
    }

    $existeMensagem = false;
    if ( isset($_SESSION['mensagem']) ) {
        $existeMensagem = true;
        $mensagem = $_SESSION['mensagem'];
        unset($_SESSION['mensagem']);
    }
?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>SuperQuiz</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/favicon.png" />
        <link rel="stylesheet" href="css/reset.css" />
        <link rel="stylesheet" href="css/estilo.css" />
    </head>
    <body>
        <div class="container">
            <header></header>
            <nav>
                <a href="index.php"><img src="img/index.png" alt="Iniciar Jogo" /></a>
                <?php if ($usuarioLogado): ?>
                    <a href="controller-logout.php"><img src="img/logout.png" alt="Logout" /></a>
                <?php else: ?>
                    <a href="login.php"><img src="img/login.png" alt="Login" /></a>
                <?php endif; ?>
                <a href="novo-usuario.php"><img src="img/novo-usuario.png" alt="Cadastrar Usuário" /></a>
                <a href="listar-perguntas.php"><img src="img/listar-perguntas.png" alt="Listar Perguntas" /></a>
                <a href="nova-pergunta.php"><img src="img/nova-pergunta.png" alt="Cadastrar Pergunta" /></a>
            </nav>
            <?php if ($existeMensagem): ?>
                <span class="mensagem"><?= $mensagem ?></span>
            <?php endif; ?>
