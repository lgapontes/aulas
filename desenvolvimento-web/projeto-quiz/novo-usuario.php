<?php
    require_once "cabecalho.php";
    require_once "modelo.php";
?>

<form action="controller-novo-usuario.php" method="POST">
    <h1>Novo Usuário</h1>
    <input type="text" name="login" placeholder="Digite o login" required tabindex="1" autofocus />
    <input type="password" name="senha" placeholder="Digite a senha" required tabindex="2" />
    <input type="password" name="confirmar" placeholder="Confirme a senha" required tabindex="3" />

    <button>Cadastrar</button>
</form>

<?php
    require_once "rodape.php";
?>
