<?php
    require_once "cabecalho.php";
    require_once "modelo.php";
    require_once "cache-nova-pergunta.php";

    session_start();
    if ( !isset($_SESSION["usuario"]) ):
?>
    <span class="mensagem">Acesso restrito!</span>
    <a href="login.php" class="link-acao">Fazer Login</a>
<?php
    else:
        $perguntaFormulario = novoCache();
        if ( isset($_SESSION["perguntaFormulario"]) ) {
            $perguntaFormulario = unserialize($_SESSION["perguntaFormulario"]);
        }
?>

    <form action="controller-nova-pergunta.php" method="POST">
        <h1>Nova Pergunta</h1>
        <textarea name="pergunta" placeholder="Digite a pergunta" required tabindex="1" autofocus><?= $perguntaFormulario["pergunta"] ?></textarea>
        <hr />
        <div class="resposta">
            <input type="radio" name="respostaCorreta" value="1" required tabindex="7"
                <?= ($perguntaFormulario["respostaCorreta"] == "1") ? "checked" : "" ?> />
            <textarea name="resposta1" placeholder="Digite a 1ª resposta" tabindex="2"><?= $perguntaFormulario["resposta1"] ?></textarea>
        </div>
        <div class="resposta">
            <input type="radio" name="respostaCorreta" value="2" required tabindex="7"
                <?= ($perguntaFormulario["respostaCorreta"] == "2") ? "checked" : "" ?> />
            <textarea name="resposta2" placeholder="Digite a 2ª resposta" tabindex="3"><?= $perguntaFormulario["resposta2"] ?></textarea>
        </div>
        <div class="resposta">
            <input type="radio" name="respostaCorreta" value="3" required tabindex="7"
                <?= ($perguntaFormulario["respostaCorreta"] == "3") ? "checked" : "" ?> />
            <textarea name="resposta3" placeholder="Digite a 3ª resposta" tabindex="4"><?= $perguntaFormulario["resposta3"] ?></textarea>
        </div>
        <div class="resposta">
            <input type="radio" name="respostaCorreta" value="4" required tabindex="7"
                <?= ($perguntaFormulario["respostaCorreta"] == "4") ? "checked" : "" ?> />
            <textarea name="resposta4" placeholder="Digite a 4ª resposta" tabindex="5"><?= $perguntaFormulario["resposta4"] ?></textarea>
        </div>
        <div class="resposta">
            <input type="radio" name="respostaCorreta" value="5" required tabindex="7"
                <?= ($perguntaFormulario["respostaCorreta"] == "5") ? "checked" : "" ?> />
            <textarea name="resposta5" placeholder="Digite a 5ª resposta" tabindex="6"><?= $perguntaFormulario["resposta5"] ?></textarea>
        </div>

        <button>Cadastrar</button>
    </form>

<?php endif; ?>
<?php
    require_once "rodape.php";
?>
