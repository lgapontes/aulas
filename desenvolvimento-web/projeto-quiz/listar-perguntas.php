<?php
    require_once "cabecalho.php";
    require_once "repositorio.php";
    require_once "modelo.php";

    session_start();
    if ( !isset($_SESSION["usuario"]) ):
?>
    <span class="mensagem">Acesso restrito!</span>
    <a href="login.php" class="link-acao">Fazer Login</a>
<?php else: ?>
    <table>
        <thead>
            <tr><th colspan="2">Perguntas</th></tr>
        </thead>
        <tbody>
            <?php
                $usuario = unserialize($_SESSION["usuario"]);
                $perguntas = obterTodasPerguntasUsuario($usuario->getCodigo());

                if (count($perguntas) == 0):
            ?>
                <tr>
                    <td class="info">Nenhuma pergunta cadastrada para este usuário!</td>
                </tr>
            <?php
                else:
                    foreach($perguntas as $pergunta):
            ?>
                <tr>
                    <td><?= htmlspecialchars($pergunta->getDescricao()) ?></td>
                    <td>
                        <form method="POST" action="controller-excluir-pergunta.php">
                            <input type="hidden" name="codigoPergunta" value="<?= $pergunta->getCodigo() ?>" />
                            <button>X</button>
                        </form>
                    </td>
                </tr>
            <?php
                    endforeach;
                endif;
            ?>
        </tbody>
    </table>
<?php endif; ?>
<?php
    require_once "rodape.php";
?>
