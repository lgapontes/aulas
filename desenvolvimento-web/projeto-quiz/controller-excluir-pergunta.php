<?php

require_once "repositorio.php";
require_once "modelo.php";

session_start();

if ( isset($_POST["codigoPergunta"]) ) {

    try {
        $codigoPergunta = $_POST["codigoPergunta"];
        excluirPergunta($codigoPergunta);

        $_SESSION["mensagem"] = "Pergunta excluída com sucesso!";
        header("Location: listar-perguntas.php");
        die();
    } catch(Exception $e) {
        $_SESSION["mensagem"] = $e->getMessage();
        header("Location: listar-perguntas.php");
        die();
    }

} else {
    $_SESSION["mensagem"] = "Formulário não enviado!";
    header("Location: listar-perguntas.php");
    die();
}
