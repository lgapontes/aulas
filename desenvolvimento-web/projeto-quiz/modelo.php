<?php

class Resposta implements JsonSerializable {
    private $codigo;
    private $descricao;
    private $correta;
    public function __construct($descricao,$correta) {
        $this->descricao = $descricao;
        $this->correta = $correta;
    }
    public function getCodigo() {
        return $this->codigo;
    }
    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }
    public function getDescricao() {
        return $this->descricao;
    }
    public function estaCorreta() {
        return $this->correta;
    }
    public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }
}

class Pergunta implements JsonSerializable {
    private $codigo;
    private $descricao;
    private $respostas;
    public function __construct($descricao) {
        $this->descricao = $descricao;
    }
    public function getCodigo() {
        return $this->codigo;
    }
    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }
    public function getDescricao() {
        return $this->descricao;
    }
    public function getRespostas() {
        return $this->respostas;
    }
    public function setRespostas($respostas) {
        $this->respostas = $respostas;
    }
    public function responder($codigoResposta) {
        foreach($this->respostas as $resposta) {
            if (
                $resposta->getCodigo() == $codigoResposta &&
                $resposta->estaCorreta()
            ) {
                return true;
            }
        }
        return false;
    }
    public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }
}

class Jogador implements JsonSerializable {
    private $vida;
    private $perguntas;
    private $indexPergunta;
    public function __construct($perguntas) {
        $this->vida = 2;
        $this->perguntas = $perguntas;
        $this->indexPergunta = -1;
    }
    public function getPerguntas() {
        return $this->perguntas;
    }
    public function existeProximaPergunta() {
        if ($this->indexPergunta < (count($this->perguntas) - 1)) {
            return true;
        }
        return false;
    }
    public function proximaPergunta() {
        if ( !$this->existeProximaPergunta() ) {
            throw new Exception("Não existem perguntas!");
        }
        $this->indexPergunta = $this->indexPergunta + 1;
        return $this->perguntas[ $this->indexPergunta ];
    }
    public function responderPergunta($codigoResposta) {
        $pergunta = $this->perguntas[ $this->indexPergunta ];
        $resultado = $pergunta->responder($codigoResposta);
        if (!$resultado) {
            $this->vida = $this->getVida() - 1;
        }
    }
    public function estaVivo() {
        if ($this->getVida() == 0) {
            return false;
        }
        return true;
    }
    public function getVida() {
        return intval($this->vida);
    }
    public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }
}

class Usuario implements JsonSerializable {
    private $codigo;
    private $login;
    private $senha;
    public function __construct($login) {
        if ( ($login == null) || ( strlen($login) < 4 ) ) {
            throw new Exception("Login muito pequeno!");
        }
        $this->login = $login;
    }
    public function getCodigo() {
        return $this->codigo;
    }
    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }
    public function definirSenha($senha,$confirmacao) {
        if ( strcmp($senha,$confirmacao) != 0 ) {
            throw new Exception("Senha e confirmação diferentes!");
        }
        if ( strlen($senha) < 4 ) {
            throw new Exception("Senha muito pequena!");
        }
        $this->senha = hash("sha256",$senha);
    }
    public function getSenha() {
        return $this->senha;
    }
    public function setSenha($senha) {
        $this->senha = hash("sha256",$senha);
    }

    public function getLogin() {
        return $this->login;
    }
    public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }
}
