<?php

require_once "repositorio.php";
require_once "modelo.php";

function perguntaJaSelecionada($perguntas,$perguntaAleatoria) {
    foreach($perguntas as $pergunta) {
        if ($pergunta->getCodigo() == $perguntaAleatoria->getCodigo()) {
            return true;
        }
    }
    return false;
}

function obterPerguntasParaJogo() {

    try {
        $todasPerguntas = obterTodasPerguntas();

        if ( count($todasPerguntas) < 3 ) {
            throw new Exception("Quantidade de perguntas insuficiente para jogar!");
        }

        $totalPerguntas = count($todasPerguntas);
        $perguntas = array();

        while ( count($perguntas) < 3 ) {
            $indexPerguntaAleatoria = rand(0, ($totalPerguntas - 1));
            $perguntaAleatoria = $todasPerguntas[ $indexPerguntaAleatoria ];
            if ( !perguntaJaSelecionada($perguntas,$perguntaAleatoria) ) {
                $respostas = obterRespostas($perguntaAleatoria->getCodigo());
                $perguntaAleatoria->setRespostas($respostas);
                array_push($perguntas,$perguntaAleatoria);
            }
        }

        return $perguntas;

    } catch(Exception $e) {
        throw new Exception("Erro: " . $e->getMessage());
    }

}

function salvarJogador($jogador) {
    $_SESSION["jogador"] = serialize($jogador);
}

function novoJogador() {
    $perguntas = obterPerguntasParaJogo();
    $jogador = new Jogador($perguntas);
    $_SESSION["jogador"] = serialize($jogador);
    return $jogador;
}

function obterJogador() {
    session_start();

    if ( isset($_SESSION["jogador"]) ) {
        if ( isset($_POST["resposta"]) ) {
            $jogador = unserialize($_SESSION["jogador"]);
            $codigoResposta = $_POST["resposta"];            
            $jogador->responderPergunta($codigoResposta);
            $_SESSION["jogador"] = serialize($jogador);
        } else {
            $jogador = novoJogador();
        }
    } else {
        $jogador = novoJogador();
    }

    return $jogador;
}
