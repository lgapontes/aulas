<?php

require_once "repositorio.php";
require_once "modelo.php";

session_start();

if ( isset($_POST["login"]) && isset($_POST["senha"]) && isset($_POST["confirmar"]) ) {

    try {
        $login = $_POST["login"];
        $senha = $_POST["senha"];
        $confirmar = $_POST["confirmar"];
        $usuario = new Usuario($login);
        $usuario->definirSenha($senha,$confirmar);

        if ( loginUsuarioExiste($usuario->getLogin()) ) {
            throw new Exception("Login já existe!");
        }
        $usuarioCadastrado = inserirUsuario($usuario);

        $_SESSION["mensagem"] = "Usuário cadastrado com sucesso!";
        header("Location: novo-usuario.php");
        die();
    } catch(Exception $e) {
        $_SESSION["mensagem"] = $e->getMessage();
        header("Location: novo-usuario.php");
        die();
    }

} else {
    $_SESSION["mensagem"] = "Formulário não enviado!";
    header("Location: novo-usuario.php");
    die();
}
