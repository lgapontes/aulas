<?php

require_once "repositorio.php";
require_once "modelo.php";
require_once "cache-nova-pergunta.php";

session_start();

function validarFormulario() {
    if (!(
        isset($_POST["pergunta"]) &&
        isset($_POST["respostaCorreta"]) &&
        isset($_POST["resposta1"]) &&
        isset($_POST["resposta2"]) &&
        isset($_POST["resposta3"]) &&
        isset($_POST["resposta4"]) &&
        isset($_POST["resposta5"])
    )) {
        throw new Exception("Formulário não enviado!");
    }
}

function obterRespostasFormulario() {
    $respostas = array();
    $respostaCorreta = intval( $_POST["respostaCorreta"] );
    $existeRespostaCorreta = false;

    for ($i=1;$i<6;$i++) {
        $key = "resposta" . $i;
        $descricao = $_POST[$key];
        salvaCache($key,$descricao);

        if ( strlen($descricao) < 3 ) {
            throw new Exception($i . "ª resposta inválida! Deve ter no mínimo 3 letras.");
        }
        $estaCorreta = ($i == $respostaCorreta);
        $resposta = new Resposta($descricao,$estaCorreta);

        array_push($respostas,$resposta);

        if ($estaCorreta) {
            $existeRespostaCorreta = true;
            salvaCache("respostaCorreta",$i);
        }
    }

    if ( !$existeRespostaCorreta ) {
        throw new Exception("Não existe uma resposta correta!");
    }

    return $respostas;
}

function obterPerguntaFormulario() {
    $descricao = $_POST["pergunta"];
    salvaCache("pergunta",$descricao);

    if ( strlen($descricao) < 3 ) {
        throw new Exception("A pergunta deve ter no mínimo 3 letras!");
    }
    $pergunta = new Pergunta($descricao);

    $respostas = obterRespostasFormulario();
    $pergunta->setRespostas($respostas);

    return $pergunta;
}

try {
    if ( !isset($_SESSION["usuario"]) ) {
        throw new Exception("Usuário não encontrado!");
    }
    $usuario = unserialize($_SESSION["usuario"]);

    validarFormulario();
    $pergunta = obterPerguntaFormulario();
    inserirPergunta($usuario->getCodigo(),$pergunta);

    limparAutoPreenchimento();
    $_SESSION["mensagem"] = "Pergunta cadastrada com sucesso!";
    header("Location: nova-pergunta.php");
    die();

} catch(Exception $e) {
    $_SESSION["mensagem"] = $e->getMessage();
    header("Location: nova-pergunta.php");
    die();
}
