<?php

require_once "repositorio.php";
require_once "modelo.php";

session_start();

if ( isset($_POST["login"]) && isset($_POST["senha"]) ) {

    $login = $_POST["login"];
    $senha = $_POST["senha"];
    $usuario = new Usuario($login);
    $usuario->setSenha($senha);

    try {
        $usuarioLogado = login($usuario);
        $_SESSION["usuario"] = serialize($usuarioLogado);
        header("Location: login.php");
        die();
    } catch(Exception $e) {
        $_SESSION["mensagem"] = $e->getMessage();
        header("Location: login.php");
        die();
    }

} else {
    $_SESSION["mensagem"] = "Formulário não enviado!";
    header("Location: login.php");
    die();
}
