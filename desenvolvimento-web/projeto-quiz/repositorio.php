<?php

require_once "conexao.php";
require_once "modelo.php";

function obterTodasPerguntasUsuario($codigoUsuario) {
    $conexao = Banco::getConexao();
    $query = $conexao->prepare("select codigo, descricao from perguntas where codigoUsuario = ?");
    $query->bind_param("i",$codigoUsuario);
    $query->execute();
    $query->store_result();
    $query->bind_result($codigo, $descricao);

    $perguntas = array();

    if ($query->num_rows > 0) {
        while ($query->fetch()) {
            $pergunta = new Pergunta($descricao);
            $pergunta->setCodigo($codigo);
            array_push($perguntas,$pergunta);
        }
    }

    return $perguntas;
}

function obterTodasPerguntas() {
    $conexao = Banco::getConexao();
    $query = $conexao->prepare(
        "select p.codigo, p.descricao from perguntas p " .
        "where (select count(*) from respostas r where r.codigoPergunta = p.codigo) = 5"
    );

    $query->execute();
    $query->store_result();
    $query->bind_result($codigo, $descricao);

    $perguntas = array();

    if ($query->num_rows > 0) {

        while ($query->fetch()) {
            $pergunta = new Pergunta($descricao);
            $pergunta->setCodigo($codigo);
            array_push($perguntas,$pergunta);
        }

        return $perguntas;

    } else {
        throw new Exception("Nenhuma pergunta encontrada!");
    }
}

function obterRespostas($codigoPergunta) {
    $conexao = Banco::getConexao();
    $query = $conexao->prepare("select codigo, descricao, correta from respostas where codigoPergunta = ?");
    $query->bind_param("i",$codigoPergunta);
    $query->execute();
    $query->store_result();
    $query->bind_result($codigo,$descricao,$correta);

    $respostas = array();

    if ($query->num_rows > 0) {

        while ($query->fetch()) {
            $resposta = new Resposta($descricao, $correta);
            $resposta->setCodigo($codigo);
            array_push($respostas,$resposta);
        }

        return $respostas;

    } else {
        throw new Exception("Nenhuma resposta encontrada!");
    }
}

function login($usuario) {
    $conexao = Banco::getConexao();
    $query = $conexao->prepare("select codigo, login from usuarios where login = ? and senha = ?");
    $query->bind_param("ss",$usuario->getLogin(),$usuario->getSenha());
    $query->execute();
    $query->store_result();
    $query->bind_result($codigo,$login);

    if ($query->num_rows == 1) {
        $query->fetch();
        $usuarioLogado = new Usuario($login);
        $usuarioLogado->setCodigo($codigo);

        return $usuarioLogado;
    } else {
        throw new Exception("Login ou senha inválidos!");
    }
}

function obterUsuario($codigoUsuario) {
    $conexao = Banco::getConexao();
    $query = $conexao->prepare("select codigo, login from usuarios where codigo = ?");
    $query->bind_param("i",$codigoUsuario);
    $query->execute();
    $query->store_result();
    $query->bind_result($codigo,$login);

    if ($query->num_rows == 1) {
        $query->fetch();
        $usuario = new Usuario($login);
        $usuario->setCodigo($codigo);
        return $usuario;
    } else {
        throw new Exception("Usuário não encontrado!");
    }
}

function loginUsuarioExiste($login) {
    $conexao = Banco::getConexao();
    $query = $conexao->prepare("select codigo, login from usuarios where login = ?");
    $query->bind_param("s",$login);
    $query->execute();
    $query->store_result();

    if ($query->num_rows > 0) {
        return true;
    } else {
        return false;
    }
}

function inserirUsuario($usuario) {
    $conexao = Banco::getConexao();
    $query = $conexao->prepare("insert into usuarios (login,senha) values (?,?)");
    $query->bind_param("ss",$usuario->getLogin(),$usuario->getSenha());
    $query->execute();
    $codigo = $conexao->insert_id;
    return obterUsuario($codigo);
}

function excluirPergunta($codigoPergunta) {
    $conexao = Banco::getConexao();
    $conexao->autocommit(false);

    try {
        $conexao->begin_transaction();

        $query = $conexao->prepare("delete from respostas where codigoPergunta = ?");
        $query->bind_param("i",$codigoPergunta);
        $query->execute();

        $query = $conexao->prepare("delete from perguntas where codigo = ?");
        $query->bind_param("i",$codigoPergunta);
        $query->execute();

        if ($conexao->affected_rows != 1) {
            throw new Exception("Erro ao excluir a pergunta!");
        }

        $conexao->commit();
        $conexao->autocommit(true);
    } catch(Exception $e) {
        $conexao->rollback();
        $conexao->autocommit(true);
        throw new Exception($e->getMessage());
    }
}

function inserirResposta($codigoPergunta,$resposta) {
    $conexao = Banco::getConexao();
    $query = $conexao->prepare("insert into respostas (codigoPergunta,descricao,correta) values (?,?,?)");
    $query->bind_param(
        "isi",
        $codigoPergunta,
        $resposta->getDescricao(),
        intval($resposta->estaCorreta())
    );
    $query->execute();
}

function inserirPergunta($codigoUsuario,$pergunta) {
    $conexao = Banco::getConexao();
    $conexao->autocommit(false);

    try {
        $conexao->begin_transaction();

        $query = $conexao->prepare("insert into perguntas (codigoUsuario,descricao) values (?,?)");
        $query->bind_param("is",$codigoUsuario,$pergunta->getDescricao());
        $query->execute();
        $codigoPergunta = $conexao->insert_id;

        foreach ($pergunta->getRespostas() as $resposta) {
            inserirResposta($codigoPergunta,$resposta);
        }

        $conexao->commit();
        $conexao->autocommit(true);
    } catch(Exception $e) {
        $conexao->rollback();
        $conexao->autocommit(true);
        throw new Exception($e->getMessage());
    }
}
