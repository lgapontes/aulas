<?php

/* Mantem dados na sessão para facilitar o preenchimento */

function novoCache() {
    $perguntaFormulario = array(
        "pergunta" => "",
        "respostaCorreta" => "",
        "resposta1" => "",
        "resposta2" => "",
        "resposta3" => "",
        "resposta4" => "",
        "resposta5" => ""
    );
    return $perguntaFormulario;
}

function limparAutoPreenchimento() {
    $perguntaFormulario = novoCache();
    $_SESSION["perguntaFormulario"] = serialize($perguntaFormulario);
}

function salvaCache($campo,$valor) {
    if ( isset($_SESSION["perguntaFormulario"]) ) {
        $perguntaFormulario = unserialize($_SESSION["perguntaFormulario"]);
        $perguntaFormulario[$campo] = $valor;
        $_SESSION["perguntaFormulario"] = serialize($perguntaFormulario);
    }
}
