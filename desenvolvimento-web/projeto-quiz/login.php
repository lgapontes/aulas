<?php
    require_once "cabecalho.php";
    require_once "modelo.php";

    if ($usuarioLogado):
        $usuario = unserialize($_SESSION["usuario"]);
?>
    <span class="mensagem">Seja bem-vindo <?= $usuario->getLogin() ?>!</span>
<?php else: ?>
    <form action="controller-login.php" method="POST">
        <h1>Login</h1>
        <input type="text" name="login" placeholder="Digite o login" required tabindex="1" autofocus/>
        <input type="password" name="senha" placeholder="Digite a senha" required tabindex="2" />

        <button>Logar</button>
    </form>
<?php
    endif;
    require_once "rodape.php";
?>
