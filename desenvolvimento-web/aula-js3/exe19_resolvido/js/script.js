// Elementos

var tagNome = document.querySelector('#nome');
var botaoLogar = document.querySelector('#logar');
var notificacaoSucesso = document.querySelector('#sucesso');
var notificacaoFalha = document.querySelector('#falha');

var tagDivSistema = document.querySelector('#sistema');
var tagDivNome = document.querySelector('#div-input-nome');

// Funções de apoio

function notificar(tag) {
    tag.classList.add('visivel');
    tag.classList.add('desvanecer');    
    
    setTimeout(function(){
        tag.classList.remove('visivel');
        tag.classList.remove('desvanecer');
    },4000);
}

function atualizarExperiencia(experiencia) {
    var spanExperiencia = document.querySelector('#experiencia');
    spanExperiencia.textContent = experiencia;

    spanExperiencia.classList.remove('is-success');
    spanExperiencia.classList.remove('is-warning');
    spanExperiencia.classList.remove('is-danger');
    if (experiencia < 0) {
        spanExperiencia.classList.add('is-danger');
    } else if (experiencia < 5) {
        spanExperiencia.classList.add('is-warning');
    } else {
        spanExperiencia.classList.add('is-success');
    }
}

function obterValorRadioButton(formulario) {
    var tipo = '';
    var tagRadios = formulario.querySelectorAll('input[type=radio]');    
    
    tagRadios.forEach(function(entry){
        if (entry.checked) {
            tipo = entry.value;
        }
    });
    
    return tipo;
}

function adicionarClasses(tag,classes) {
    classes.forEach(function(classe){
        tag.classList.add(classe);
    });
}

function renderizarTag(tag,classes,valor) {
    var tag = document.createElement(tag);
    adicionarClasses(tag,classes);
    if (valor) {
        tag.textContent = valor;
    }
    return tag;
}

function renderizarDiv(classe) {
    return renderizarTag('div',[classe]);
}

function renderizarImagemLoading() {
    return '<img class="link-card-loading" src="img/loading.gif" />';
}

function renderizarLinkApagar() {
    var linkApagar = document.createElement('a');
    linkApagar.classList.add('card-footer-item');
    linkApagar.classList.add('has-background-light');
    linkApagar.classList.add('has-text-grey-dark');
    linkApagar.classList.add('nao-permite-selecionar');    
    linkApagar.textContent = 'Apagar';

    return linkApagar;
}

function renderizarLinkFeita() {
    var linkFeita = document.createElement('a');
    linkFeita.classList.add('card-footer-item');
    linkFeita.classList.add('has-background-success');
    linkFeita.classList.add('has-text-white');
    linkFeita.classList.add('nao-permite-selecionar');
    var strong = document.createElement('strong');
    strong.textContent = 'Feita!';
    linkFeita.appendChild(strong);
    return linkFeita;
}

function renderizarInputHidden(valor) {
    var tagInput = document.createElement('input');
    tagInput.type = 'hidden';
    tagInput.value = valor;
    return tagInput;
}

function renderizarTarefa(tarefa) {

    // Criando nova tarefa no DOM
    var divColumn = renderizarDiv('column');
    var divCard = renderizarDiv('card');
    var divCardContent = renderizarDiv('card-content');
    var divContent = renderizarDiv('content');

    var tagInput = renderizarInputHidden(tarefa.codigo);

    var spanTexto = renderizarTag('span',[],tarefa.texto);
    var br = document.createElement('br');
    var spanTipo = renderizarTag('span',['tag','is-light'],tarefa.tipo);
    var spanData = renderizarTag('span',['is-size-7','has-text-grey-light'],tarefa.dataCriacao);

    var footer = renderizarTag('footer',['card-footer']);

    var linkApagar = renderizarLinkApagar();
    var linkFeita = renderizarLinkFeita();    

    // Organizando DOM
    divColumn.appendChild(divCard);
    divCard.appendChild(divCardContent);
    divCardContent.appendChild(divContent);
    divContent.appendChild(tagInput);
    divContent.appendChild(spanTexto);
    divContent.appendChild(br);
    divContent.appendChild(spanTipo);
    divContent.appendChild(spanData);
    divCard.appendChild(footer);
    footer.appendChild(linkApagar);
    footer.appendChild(linkFeita);

    // Capturando os eventos na tarefa
    divColumn.addEventListener('click',function(evento){
        var codigoTarefa = parseInt(divColumn.querySelector('input[type=hidden]').value);

        if (evento.target == linkApagar) {            
            linkApagar.classList.add('desativado');
            linkApagar.innerHTML = renderizarImagemLoading();

            apagarTarefa(codigoTarefa,function(usuario){
                divColumn.remove();
                atualizarExperiencia(usuario.experiencia);
            },function(erro){
                notificacaoFalha.textContent = erro;
                notificar(notificacaoFalha);
            });
        }
        if (evento.target == linkFeita) {
            linkFeita.classList.add('desativado');
            linkFeita.innerHTML = renderizarImagemLoading();

            concluirTarefa(codigoTarefa,function(usuario){
                divColumn.remove();
                atualizarExperiencia(usuario.experiencia);
            },function(erro){
                notificacaoFalha.textContent = erro;
                notificar(notificacaoFalha);
            });
        }
    });

    return divColumn;
}

// Lógica da aplicação
 
botaoLogar.addEventListener('click',function(){
    var nome = tagNome.value;
    
    if (nome.length == 0) {
        // Nome inválido
        notificacaoFalha.textContent = 'Nenhum nome foi digitado!';
        notificar(notificacaoFalha);
    } else {
        // Nome válido
        tagDivNome.classList.add('is-loading');

        login(nome,function(usuario){

            var tagInputCodigoUsuario = document.querySelector('#codigo-usuario');
            tagInputCodigoUsuario.value = usuario.codigo;

            var experiencia = parseInt(usuario.experiencia);
            atualizarExperiencia(experiencia);

            listarTipos(function(tipos){
                
                var divTipos = document.querySelector('#tipos');
                tipos.forEach(function(tipo){
                    
                    var labelRadio = document.createElement('label');
                    labelRadio.classList.add('radio');
                    
                    var inputTipo = document.createElement('input');
                    inputTipo.type = 'radio';
                    inputTipo.name = 'tipo';
                    inputTipo.value = tipo.codigo;
                    if (tipo.codigo == "1") {
                        inputTipo.checked = true;
                    }

                    var spanTexto = document.createElement('span');
                    spanTexto.textContent = tipo.descricao;

                    labelRadio.appendChild(inputTipo);
                    labelRadio.appendChild(spanTexto);
                    
                    divTipos.appendChild(labelRadio);
                    
                });

                var tarefas = document.querySelector('#lista-tarefas');
                usuario.tarefas.forEach(function(tarefa){
                    var tarefa = renderizarTarefa(tarefa);                    
                    tarefas.appendChild(tarefa);
                });

                tagNome.disabled = true;
                botaoLogar.disabled = true;
                tagDivSistema.classList.add('visivel');

                tagDivNome.classList.remove('is-loading');

                // Notificar
                notificacaoSucesso.textContent = 'Seja bem-vindo ' + nome + '!';
                notificar(notificacaoSucesso);
            },function(erro){
                tagDivNome.classList.remove('is-loading');
                
                // Notificar
                notificacaoFalha.textContent = erro;
                notificar(notificacaoFalha);
            });

        });
    }
})

var tagForm = document.querySelector('form');
tagForm.addEventListener('submit',function(evento){
    evento.preventDefault();
    var formulario = evento.target;

    var tagInputCodigoUsuario = document.querySelector('#codigo-usuario');    

    var tarefa = {
        codigoUsuario: tagInputCodigoUsuario.value,
        codigoTipo: obterValorRadioButton(formulario),
        texto: formulario.texto.value
    }

    incluirTarefa(tarefa,function(objeto){

        // Atualizar experiencia
        var experiencia = parseInt(objeto.usuario.experiencia);
        atualizarExperiencia(experiencia);

        // Renderizar tarefa
        var tagTarefa = renderizarTarefa(objeto.tarefa);    
        var tagTarefas = document.querySelector('#lista-tarefas');
        tagTarefas.appendChild(tagTarefa);

        // Limpar formulario
        formulario.reset();
        var tagRadios = formulario.querySelectorAll('input[type=radio]');
        tagRadios[0].checked = true;

    },function(erro){
        // Notificar
        notificacaoFalha.textContent = erro;
        notificar(notificacaoFalha);
    });
});