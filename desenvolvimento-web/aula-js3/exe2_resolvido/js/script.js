
function notificar(tag) {
    tag.classList.add('visivel');
    tag.classList.add('desvanecer');    
    
    setTimeout(function(){
        tag.classList.remove('visivel');
        tag.classList.remove('desvanecer');
    },4000);
}

function obterValorRadioButton(formulario) {
    var tipo = '';
    var tagRadios = formulario.querySelectorAll('input[type=radio]');    
    tagRadios.forEach(function(entry){
        if (entry.checked) {
            tipo = entry.value;
        }
    });
    return tipo;
}

function adicionarClasses(tag,classes) {
    classes.forEach(function(classe){
        tag.classList.add(classe);
    });
}

function renderizarTag(tag,classes,valor) {
    var tag = document.createElement(tag);
    adicionarClasses(tag,classes);
    if (valor) {
        tag.textContent = valor;
    }
    return tag;
}

function renderizarDiv(classe) {
    return renderizarTag('div',[classe]);
}

function renderizarLinkApagar() {
    var linkApagar = document.createElement('a');
    linkApagar.classList.add('card-footer-item');
    linkApagar.classList.add('has-background-light');
    linkApagar.classList.add('has-text-grey-dark');
    linkApagar.textContent = 'Apagar';

    linkApagar.addEventListener('click',function(){
        this.remove();
    });

    return linkApagar;
}

function renderizarLinkFeita() {
    var linkFeita = document.createElement('a');
    linkFeita.classList.add('card-footer-item');
    linkFeita.classList.add('has-background-success');
    linkFeita.classList.add('has-text-white');
    var strong = document.createElement('strong');
    strong.textContent = 'Feita!';
    linkFeita.appendChild(strong);
    return linkFeita;
}

function renderizarTarefa(tarefa) {

    // Criando nova tarefa no DOM
    var divColumn = renderizarDiv('column');
    var divCard = renderizarDiv('card');
    var divCardContent = renderizarDiv('card-content');
    var divContent = renderizarDiv('content');

    var spanTexto = renderizarTag('span',[],tarefa.texto);
    var br = document.createElement('br');
    var spanTipo = renderizarTag('span',['tag','is-light'],tarefa.tipo);
    var spanData = renderizarTag('span',['is-size-7','has-text-grey-light'],tarefa.data);

    var footer = renderizarTag('footer',['card-footer']);

    var linkApagar = renderizarLinkApagar();
    var linkFeita = renderizarLinkFeita();

    // Organizando DOM
    divColumn.appendChild(divCard);
    divCard.appendChild(divCardContent);
    divCardContent.appendChild(divContent);
    divContent.appendChild(spanTexto);
    divContent.appendChild(br);
    divContent.appendChild(spanTipo);
    divContent.appendChild(spanData);
    divCard.appendChild(footer);
    footer.appendChild(linkApagar);
    footer.appendChild(linkFeita);

    return divColumn;
}

// Lógica da aplicação

var tagNome = document.querySelector('#nome');
var botaoLogar = document.querySelector('#logar');
var notificacaoSucesso = document.querySelector('#sucesso');
var notificacaoFalha = document.querySelector('#falha');

var tagDivSistema = document.querySelector('#sistema');

botaoLogar.addEventListener('click',function(){
    var nome = tagNome.value;
    
    if (nome.length == 0) {
        // Nome inválido
        notificacaoFalha.textContent = 'Nenhum nome foi digitado!';
        notificar(notificacaoFalha);
    } else {
        // Nome válido
        notificacaoSucesso.textContent = 'Seja bem-vindo ' + nome + '!';
        notificar(notificacaoSucesso);

        tagNome.disabled = true;
        botaoLogar.disabled = true;
        tagDivSistema.classList.add('visivel');
    }
})

var tagForm = document.querySelector('form');
tagForm.addEventListener('submit',function(evento){
    evento.preventDefault();
    var formulario = evento.target;

    var tarefa = {
        tipo: obterValorRadioButton(formulario),
        texto: formulario.texto.value,
        data: moment().format('DD/MM/YYYY HH:mm:ss')
    }

    var tarefa = renderizarTarefa(tarefa);
    
    var tarefas = document.querySelector('#lista-tarefas');
    tarefas.appendChild(tarefa);

    formulario.reset();
});