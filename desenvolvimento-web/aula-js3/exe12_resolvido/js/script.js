
function notificar(tag) {
    tag.classList.add('visivel');
    tag.classList.add('desvanecer');    
    
    setTimeout(function(){
        tag.classList.remove('visivel');
        tag.classList.remove('desvanecer');
    },4000);
}

function atualizarExperiencia(pontos) {
    var spanExperiencia = document.querySelector('#experiencia');
    var experiencia = parseInt(spanExperiencia.textContent) + pontos;
    spanExperiencia.textContent = experiencia;

    spanExperiencia.classList.remove('is-success');
    spanExperiencia.classList.remove('is-warning');
    spanExperiencia.classList.remove('is-danger');
    if (experiencia < 0) {
        spanExperiencia.classList.add('is-danger');
    } else if (experiencia < 5) {
        spanExperiencia.classList.add('is-warning');
    } else {
        spanExperiencia.classList.add('is-success');
    }
}

function obterValorRadioButton(formulario) {
    var tipo = '';
    var tagRadios = formulario.querySelectorAll('input[type=radio]');    
    
    tagRadios.forEach(function(entry){
        if (entry.checked) {
            tipo = entry.value;
        }
    });
    
    return tipo;
}

function adicionarClasses(tag,classes) {
    classes.forEach(function(classe){
        tag.classList.add(classe);
    });
}

function renderizarTag(tag,classes,valor) {
    var tag = document.createElement(tag);
    adicionarClasses(tag,classes);
    if (valor) {
        tag.textContent = valor;
    }
    return tag;
}

function renderizarDiv(classe) {
    return renderizarTag('div',[classe]);
}

function renderizarLinkApagar() {
    var linkApagar = document.createElement('a');
    linkApagar.classList.add('card-footer-item');
    linkApagar.classList.add('has-background-light');
    linkApagar.classList.add('has-text-grey-dark');
    linkApagar.textContent = 'Apagar';

    return linkApagar;
}

function renderizarLinkFeita() {
    var linkFeita = document.createElement('a');
    linkFeita.classList.add('card-footer-item');
    linkFeita.classList.add('has-background-success');
    linkFeita.classList.add('has-text-white');
    var strong = document.createElement('strong');
    strong.textContent = 'Feita!';
    linkFeita.appendChild(strong);
    return linkFeita;
}

function renderizarTarefa(tarefa) {

    // Criando nova tarefa no DOM
    var divColumn = renderizarDiv('column');
    var divCard = renderizarDiv('card');
    var divCardContent = renderizarDiv('card-content');
    var divContent = renderizarDiv('content');

    var spanTexto = renderizarTag('span',[],tarefa.texto);
    var br = document.createElement('br');
    var spanTipo = renderizarTag('span',['tag','is-light'],tarefa.tipo);
    var spanData = renderizarTag('span',['is-size-7','has-text-grey-light'],tarefa.dataCriacao);

    var footer = renderizarTag('footer',['card-footer']);

    var linkApagar = renderizarLinkApagar();
    var linkFeita = renderizarLinkFeita();

    // Organizando DOM
    divColumn.appendChild(divCard);
    divCard.appendChild(divCardContent);
    divCardContent.appendChild(divContent);
    divContent.appendChild(spanTexto);
    divContent.appendChild(br);
    divContent.appendChild(spanTipo);
    divContent.appendChild(spanData);
    divCard.appendChild(footer);
    footer.appendChild(linkApagar);
    footer.appendChild(linkFeita);

    // Capturando os eventos na tarefa
    divColumn.addEventListener('click',function(evento){
        if (evento.target == linkApagar) {
            this.remove();
            atualizarExperiencia(-2);
        }
        if (evento.target == linkFeita) {            
            this.remove();
            atualizarExperiencia(1);
        }
    });

    return divColumn;
}

// Lógica da aplicação

var tagNome = document.querySelector('#nome');
var botaoLogar = document.querySelector('#logar');
var notificacaoSucesso = document.querySelector('#sucesso');
var notificacaoFalha = document.querySelector('#falha');

var tagDivSistema = document.querySelector('#sistema');
var tagDivNome = document.querySelector('#div-input-nome');
 
botaoLogar.addEventListener('click',function(){
    var nome = tagNome.value;
    
    if (nome.length == 0) {
        // Nome inválido
        notificacaoFalha.textContent = 'Nenhum nome foi digitado!';
        notificar(notificacaoFalha);
    } else {
        // Nome válido
        tagDivNome.classList.add('is-loading');

        login(nome,function(usuario){

            var tagInputCodigoUsuario = document.querySelector('#codigo-usuario');
            tagInputCodigoUsuario.value = usuario.codigo;

            listarTipos(function(tipos){
                
                var divTipos = document.querySelector('#tipos');
                tipos.forEach(function(tipo){
                    
                    var labelRadio = document.createElement('label');
                    labelRadio.classList.add('radio');
                    
                    var inputTipo = document.createElement('input');
                    inputTipo.type = 'radio';
                    inputTipo.name = 'tipo';
                    inputTipo.value = tipo.codigo;
                    if (tipo.codigo == "1") {
                        inputTipo.checked = true;
                    }

                    var spanTexto = document.createElement('span');
                    spanTexto.textContent = tipo.descricao;

                    labelRadio.appendChild(inputTipo);
                    labelRadio.appendChild(spanTexto);
                    
                    divTipos.appendChild(labelRadio);
                    
                });

                var tarefas = document.querySelector('#lista-tarefas');
                usuario.tarefas.forEach(function(tarefa){
                    var tarefa = renderizarTarefa(tarefa);                    
                    tarefas.appendChild(tarefa);
                });

                tagNome.disabled = true;
                botaoLogar.disabled = true;
                tagDivSistema.classList.add('visivel');

                tagDivNome.classList.remove('is-loading');

                // Notificar
                notificacaoSucesso.textContent = 'Seja bem-vindo ' + nome + '!';
                notificar(notificacaoSucesso);
            });

        });
    }
})

var tagForm = document.querySelector('form');
tagForm.addEventListener('submit',function(evento){
    evento.preventDefault();
    var formulario = evento.target;

    var tagInputCodigoUsuario = document.querySelector('#codigo-usuario');    

    var tarefa = {
        codigoUsuario: tagInputCodigoUsuario.value,
        codigoTipo: obterValorRadioButton(formulario),
        texto: formulario.texto.value
    }

    var tarefa = renderizarTarefa(tarefa);
    
    var tarefas = document.querySelector('#lista-tarefas');
    tarefas.appendChild(tarefa);

    formulario.reset();

    atualizarExperiencia(1);
});