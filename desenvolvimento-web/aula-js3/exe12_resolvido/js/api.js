function consumirAPI(url,metodo,callback,json) {
    var xhr = new XMLHttpRequest();
    xhr.open(metodo,url);

    xhr.addEventListener('load',function(){
        var objeto = JSON.parse(xhr.responseText);
        callback(objeto);
    });

    if (json != undefined) {
        xhr.send(json);
    } else {
        xhr.send();
    }    
}

function listarTipos(callback) {
    consumirAPI(
        'http://lgapontes.com/aulas/todoapp/api/tipos',
        'GET',
        callback
    );
}

function login(nome,callback) {
    consumirAPI(
        'http://lgapontes.com/aulas/todoapp/api/login',
        'POST',
        callback,
        JSON.stringify({
            "nome": nome
        })
    );
}

function incluirTarefa(tarefa,callback) {
    consumirAPI(
        'http://lgapontes.com/aulas/todoapp/api/tarefa',
        'POST',
        callback,
        JSON.stringify(tarefa)
    );
}