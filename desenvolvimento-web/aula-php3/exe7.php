<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/favicon.png">
        <style>
            h1 {
                text-align: center;
            }

            div {
                width: 100%;
                max-width: 800px;
                margin: 0 auto;
                padding: 10px;
                font-size: 1.2em;
                background-color: #e0e0e0;
                font-family: monospace;
            }

            button {
                display: block;
                font-size: 1.2em;
                margin: 10px auto;
            }

            span {
                color: red;
            }
        </style>
    </head>
    <body>

        <h1>Exemplo de Transação</h1>
        <div><strong>Usuário de código 1 comprando o produto 1.</strong><br /><br />
        <?php

            function mostrarUsuario($conexao) {
                $resultado = $conexao->query('select * from usuarios where codigo = 1');
                if ($resultado->num_rows > 0) {
                    $usuario = $resultado->fetch_assoc();
                    echo "Usuário: [Código = " . $usuario['codigo'] . ", Nome = " . $usuario['nome'] . "]<br />";
                }
            }
            function obterProdutos($conexao) {
                $resultado = $conexao->query('select * from produtos where codigo = 1');
                if ($resultado->num_rows > 0) {
                    $produto = $resultado->fetch_assoc();
                    return $produto;
                }
            }
            function mostrarProdutos($conexao) {
                $resultado = $conexao->query('select * from produtos where codigo = 1');
                $produto = obterProdutos($conexao);
                echo "Produto: [Código " . $produto['codigo'] . ", Descrição = " . $produto['descricao'] . ", Estoque = " . $produto['estoque'] . "]<br />";
            }

            $conexao = new mysqli('127.0.0.1','roupas_user','123eja','roupas_database');

            mostrarUsuario($conexao);
            echo "<br />";
            mostrarProdutos($conexao);

        ?>
        </div>
        <form method="POST" action="exe7.php">
            <input type="hidden" name="executar" value="true"  />
            <button>Executar...</button>
        </form>
        <div>
            <?php
                if (!isset($_POST['executar'])) {
                    echo "Clique no botão para executar!";
                } else {
                    echo "Executando operação...<br />";

                    echo "Configurando <i>MySQL Improved</i> para disparar erros<br />";
                    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

                    echo "Desligando comportamento padrão de autocommit do <i>MySQL Improved</i><br />";
                    $conexao->autocommit(false);

                    try {

                        echo "Iniciando transação<br />";
                        $conexao->begin_transaction();

                        echo "Verificando o estoque do produto<br />";
                        $produto = obterProdutos($conexao);
                        $estoque = $produto['estoque'];
                        if ( $estoque == 0 ) {
                            throw new Exception("Produto sem estoque!");
                        }

                        echo "Inserindo registro na tabela <i>compras</i><br />";
                        $conexao->query('insert into compras (codigoUsuario) values (1)');

                        echo "Obtendo código da compra inserida<br />";
                        $codigoCompra = $conexao->insert_id;

                        echo "Inserindo registro na tabela <i>comprasProdutos</i><br />";
                        $conexao->query('insert into comprasProdutos (codigoCompra,codigoProduto) values (' . $codigoCompra . ',1)');

                        echo "Reduzindo estoque do produto na tabela <i>produtos</i><br />";
                        $conexao->query('update produtos set estoque = ' . ($estoque-1) . ' where codigo = 1');

                        echo "Executando <i>commit</i><br />";
                        $conexao->commit();

                        echo "<br />";
                        echo "Dados do produto após o <i>commit</i><br />";
                        mostrarProdutos($conexao);

                    } catch (Exception $e) {
                        echo "Executando <i>rollback</i> por causa do erro: <span>" . $e->getMessage() . "</span><br />";
                        $conexao->rollback();
                    }

                }
            ?>
        </div>
        <?php
            $conexao->close();
        ?>
    </body>
</html>
