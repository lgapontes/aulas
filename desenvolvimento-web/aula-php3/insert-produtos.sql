insert into imagens (nomeArquivo) values
    ('fac30f11515aa796ce39d4cb5c9fabbe8ec76426c3f606698988b9eab7bd902e.png'),
    ('d0d67bf0df77691dd811727df09b57c793f4af03df82fdc93498d75567c82711.png'),
    ('76710ff841a95ec262f3c3fd75297b712f3f89b5158cd2df24d412fac71e7c2b.png'),
    ('e2eae2312c15bf1081e411df9e1fc6c1c4456e9dbf57bd53c0bffd1e74e07926.png'),
    ('63ae96c5de4352c76239526b5e4c20cb5cea75a2b3423defe5d9f35cf2e509cf.png'),
    ('f6f3cde161c292f02edae0f5fdfc6695b94540c8e5ace8cc133052d6a60bebe2.png'),
    ('3a3064fdb23f202462117d195cd2c0f5d0df0f2b455a12a681c0dd3f87f49a0e.png'),
    ('7c8ecda63d1b3265eeec765988e024c677cc9fc18842aa8df37df753bb09703a.png'),
    ('e62b5809cb9ba814e5dac42c2b43b2282d58c07869d1e2f14bfa1ef21e37a81f.png'),
    ('aa9258dcc965bc2a5a99f223350fce373eb36ec7f2e2d0a65f7f32b9cdbc3a8d.png'),
    ('5c0cceef74ec00ac71b73bfc9a7826cd46a29ce873c04926e94a19939813394b.png'),
    ('e071e285e54a42232297b20cf16ce03297e55f44edffcdcbff10297833180ac2.png'),
    ('c68ce0abb4b14b295547674deb167048645345ca92c183f01463ff0482444d08.png');

insert into produtos (descricao,preco,estoque,codigoImagem) values
    ('Camiseta feminina branca',70.00,100,(select codigo from imagens where nomeArquivo = 'fac30f11515aa796ce39d4cb5c9fabbe8ec76426c3f606698988b9eab7bd902e.png')),
    ('Camiseta de frio',120.00,100,(select codigo from imagens where nomeArquivo = 'd0d67bf0df77691dd811727df09b57c793f4af03df82fdc93498d75567c82711.png')),
    ('Camiseta feminina azul',41.99,50,(select codigo from imagens where nomeArquivo = '76710ff841a95ec262f3c3fd75297b712f3f89b5158cd2df24d412fac71e7c2b.png')),
    ('Camiseta masculina cinza',85.00,80,(select codigo from imagens where nomeArquivo = 'e2eae2312c15bf1081e411df9e1fc6c1c4456e9dbf57bd53c0bffd1e74e07926.png')),
    ('Camiseta feminina cinza',55.00,100,(select codigo from imagens where nomeArquivo = '63ae96c5de4352c76239526b5e4c20cb5cea75a2b3423defe5d9f35cf2e509cf.png')),
    ('Camiseta unix cinza',80.00,50,(select codigo from imagens where nomeArquivo = 'f6f3cde161c292f02edae0f5fdfc6695b94540c8e5ace8cc133052d6a60bebe2.png')),
    ('Boné azul',25.00,150,(select codigo from imagens where nomeArquivo = '3a3064fdb23f202462117d195cd2c0f5d0df0f2b455a12a681c0dd3f87f49a0e.png')),
    ('Camiseta preta',90.00,80,(select codigo from imagens where nomeArquivo = '7c8ecda63d1b3265eeec765988e024c677cc9fc18842aa8df37df753bb09703a.png')),
    ('Camiseta vermelha',74.99,100,(select codigo from imagens where nomeArquivo = 'e62b5809cb9ba814e5dac42c2b43b2282d58c07869d1e2f14bfa1ef21e37a81f.png')),
    ('Camiseta feminina branca',52.00,50,(select codigo from imagens where nomeArquivo = 'aa9258dcc965bc2a5a99f223350fce373eb36ec7f2e2d0a65f7f32b9cdbc3a8d.png')),
    ('Meias azuis',30.00,200,(select codigo from imagens where nomeArquivo = '5c0cceef74ec00ac71b73bfc9a7826cd46a29ce873c04926e94a19939813394b.png')),
    ('Camiseta cinza',40.00,100,(select codigo from imagens where nomeArquivo = 'e071e285e54a42232297b20cf16ce03297e55f44edffcdcbff10297833180ac2.png')),
    ('Camiseta branca',48.50,50,(select codigo from imagens where nomeArquivo = 'c68ce0abb4b14b295547674deb167048645345ca92c183f01463ff0482444d08.png'));
