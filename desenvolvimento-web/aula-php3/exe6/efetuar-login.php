<?php

session_start();

require_once "modelo.php";

function emailExiste() {
    return isset($_POST['email']);
}
function senhaExiste() {
    return isset($_POST['senha']);
}

try {

    if ( emailExiste() && senhaExiste() ) {

        $email = $_POST['email'];
        $senha = $_POST['senha'];
        $usuario = Usuario::login($email,$senha);
        $_SESSION['usuario'] = serialize($usuario);

        if ($usuario) {
            $_SESSION['mensagem-sucesso'] = 'Seja bem-vindo ' . $usuario->getNome() . '!';
            header('Location: index.php');
            die();
        } else {
            $_SESSION['mensagem-erro'] = 'Usuário não encontrado!';
            header('Location: index.php');
            die();
        }
    } else {
        $_SESSION['mensagem-erro'] = 'Usuário não encontrado!';
        header('Location: index.php');
        die();
    }

} catch(Exception $erro) {
    echo "<pre>";
    var_dump($erro);
    echo "</pre>";
    die();
}
