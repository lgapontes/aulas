<?php

require_once "modelo.php";

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

function exibirErro($erro) {
    echo "<pre>";
    var_dump($erro);
    echo "</pre>";
    die();
}

function conectar() {
    try {
        $conexao = new mysqli('127.0.0.1','roupas_user','123eja','roupas_database');
        return $conexao;
    } catch(Exception $erro) {
        exibirErro($erro);
    }
}

function listarPerfis() {
    try {

        $perfis = array();
        $conexao = conectar();
        $query = $conexao->prepare('select codigo, descricao from perfis');
        $query->execute();
        $resultado = $query->get_result();
        if ($resultado->num_rows > 0) {
            while($perfil = $resultado->fetch_assoc()) {
                array_push($perfis,new Perfil(
                    $perfil['codigo'],
                    $perfil['descricao']
                ));
            }
        }
        return $perfis;

    } catch(Exception $erro) {
        exibirErro($erro);
    }
}

function obterPerfil($codigoPerfil) {
    try {

        $conexao = conectar();
        $query = $conexao->prepare('select codigo, descricao from perfis where codigo = ?');
        $query->bind_param('i',$codigoPerfil);
        $query->execute();
        $resultado = $query->get_result();
        if ($resultado->num_rows == 1) {
            $perfil = $resultado->fetch_assoc();
            return new Perfil($perfil['codigo'],$perfil['descricao']);
        } else {
            return null;
        }

    } catch(Exception $erro) {
        exibirErro($erro);
    }
}

function existeEmail($email) {
    try {

        $perfis = array();
        $conexao = conectar();
        $query = $conexao->prepare('select email from usuarios where email = ?');
        $query->bind_param('s',$email);
        $query->execute();
        $resultado = $query->get_result();
        if ($resultado->num_rows > 0) {
            return true;
        } else {
            return false;
        }

    } catch(Exception $erro) {
        exibirErro($erro);
    }
}

function inserirUsuario($usuario) {
    try {

        $conexao = conectar();
        $query = $conexao->prepare('insert into usuarios (email,nome,senha,codigoPerfil) values (?,?,?,?)');

        $query->bind_param(
            'sssi',
            $usuario->getEmail(),
            $usuario->getNome(),
            $usuario->getHashSenha(),
            $usuario->getPerfil()->getCodigo()
        );
        $resultado = $query->execute();

        if (!$resultado) {
            throw new Exception("Erro ao inserir usuário!");
        }

    } catch(Exception $erro) {
        exibirErro($erro);
    }
}

function login($email,$senha) {
    try {

        $conexao = conectar();
        $query = $conexao->prepare(
            'select u.codigo, u.email, u.nome, u.codigoPerfil, p.descricao  from usuarios u ' .
            'inner join perfis p on p.codigo = u.codigoPerfil ' .
            'where email = ? and senha = ?'
        );
        $query->bind_param('ss',$email,$senha);
        $query->execute();
        $resultado = $query->get_result();

        if ($resultado->num_rows == 1) {
            $linha = $resultado->fetch_assoc();

            $perfil = new Perfil(
                $linha['codigoPerfil'],
                $linha['descricao']
            );

            $usuario = Usuario::fabrica(
                $linha['codigo'],
                $linha['email'],
                $linha['nome'],
                $perfil
            );

            return $usuario;
        } else {
            return null;
        }

    } catch(Exception $erro) {
        exibirErro($erro);
    }
}

/*
var_dump( listarPerfis()[0] ); echo "<br />";
var_dump( existeEmail('email@site.com') ); echo "<br />";
var_dump( existeEmail('guilherme@site.com') ); echo "<br />";
*/
