<?php
    session_start();
    require_once "modelo.php";

    if (isset($_SESSION['usuario'])) {
        $usuario = unserialize($_SESSION['usuario']);
    }

    if ( isset($_SESSION['mensagem-erro']) ) {
        $mensagemErro = $_SESSION['mensagem-erro'];
        unset($_SESSION['mensagem-erro']);
    }

?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/favicon.png">
        <link rel="stylesheet" type="text/css" href="css/style.css" />
    </head>
    <body>
        <?php if ( isset($mensagemErro) ): ?>
            <span class="error">
                <?= $mensagemErro ?>
            </span>
        <?php endif; ?>

        <form action="efetuar-cadastro.php" method="POST">
            <fieldset>
                <div class="item">
                    <label for="email">E-Mail</label>
                    <input type="text" id="email" name="email" tabindex="1"
                        value="<?= isset($usuario) ? $usuario->getEmail() : '' ?>"
                    />
                </div>
                <div class="item">
                    <label for="nome">Nome</label>
                    <input type="text" id="nome" name="nome" tabindex="2"
                        value="<?= isset($usuario) ? $usuario->getNome() : '' ?>"
                    />
                </div>
                <div class="item">
                    <label for="perfil">Perfil</label>
                    <select id="perfil" name="perfil" tabindex="3">
                        <?php
                            $perfis = Perfil::listar();
                            foreach($perfis as $perfil):
                        ?>
                            <option value="<?= $perfil->getCodigo() ?>"><?= $perfil->getDescricao() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="item">
                    <label for="senha">Senha</label>
                    <input type="password" id="senha" name="senha" tabindex="4" value="" />
                </div>
                <div class="item">
                    <label for="confirmar-senha">Confirmar</label>
                    <input type="password" id="confirmar-senha" name="confirmar-senha" tabindex="5" value="" />
                </div>
                <div class="item-checkbox">
                    <input type="checkbox" id="termos" name="termos" value="true" />
                    <label for="termos">Aceita termos?</label>
                </div>
                <button>Salvar</button>
                <a href="index.php">Cancelar</a>
            </fieldset>
        </form>

    </body>
</html>
