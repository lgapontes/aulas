<?php

require_once "repositorio.php";

class Perfil {
    private $codigo;
    private $descricao;

    public function __construct($codigo,$descricao) {
        $this->codigo = $codigo;
        $this->descricao = $descricao;
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function getDescricao() {
        return $this->descricao;
    }

    public static function listar() {
        return listarPerfis();
    }

    public static function obter($codigoPerfil) {
        return obterPerfil($codigoPerfil);
    }

    public function toString() {
        $str = "<strong>Código Perfil:</strong> {$this->codigo}<br />" .
               "<strong>Descrição Perfil:</strong> {$this->descricao}<br />";
        return $str;
    }
}

class Usuario {

    private $codigo;
    private $email;
    private $nome;
    private $perfil;
    private $senha;


    public function getCodigo() {
        return $this->codigo;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        if ( empty($email) ) {
            throw new Exception('E-mail inválido!');
        }
        if ( existeEmail($email) ) {
            throw new Exception("O e-mail <i>{$email}</i> já existe!");
        }

        $this->email = $email;
    }

    public function getNome() {
        return $this->nome;
    }

    public function setNome($nome) {
        if ( empty($nome) ) {
            throw new Exception('Nome inválido!');
        }

        $this->nome = $nome;
    }

    public function getPerfil() {
        return $this->perfil;
    }

    public function setPerfil($perfil) {
        if (!$perfil) {
            throw new Exception("Perfil não existe!");
        }
        $this->perfil = $perfil;
    }

    public function setSenha($senha,$confirmarSenha) {
        if ( empty($senha) || strlen($senha) < 5 ) {
            throw new Exception("A senha deve ter no mínimo 5 letras!");
        }
        if ($senha != $confirmarSenha) {
            throw new Exception("Confirmação de senha não confere!");
        }
        $this->senha = $senha;
    }

    public function toString() {
        $str = "";
        if ($this->codigo) $str = $str . "<strong>Código:</strong> {$this->codigo}<br />";
        if ($this->email) $str = $str . "<strong>Email:</strong> {$this->email}<br />";
        if ($this->nome) $str = $str . "<strong>Nome:</strong> {$this->nome}<br />";
        if ($this->perfil) $str = $str . $this->perfil->toString();
        return $str;
    }
}
