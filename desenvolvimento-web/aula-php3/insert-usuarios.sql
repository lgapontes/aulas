---- Exemplos de sucesso

insert into perfis (codigo,descricao) values (1,'Cliente');
insert into perfis (codigo,descricao) values (2,'Vendedor');

insert into usuarios (email,nome,senha,codigoPerfil)
    values ('email@site.com','Fulano','df19ec5a55cfeb9597d0bd235f37e4e15b7552989f43384017852cd5b1646a95',1);

insert into usuarios (email,nome,senha,codigoPerfil)
    values ('joao@site.com','João','df19ec5a55cfeb9597d0bd235f37e4e15b7552989f43384017852cd5b1646a95',1);

insert into usuarios (email,nome,senha,codigoPerfil)
    values ('maria@site.com','Maria','df19ec5a55cfeb9597d0bd235f37e4e15b7552989f43384017852cd5b1646a95',2);


---- Exemplos de erros

insert into usuarios (email,nome,senha,codigoPerfil) values ('pedro@site.com',null,'df19ec5a55cfeb9597d0bd235f37e4e15b7552989f43384017852cd5b1646a95',2);

insert into usuarios (email,nome,senha,codigoPerfil) values ('maria@site.com','Maria','df19ec5a55cfeb9597d0bd235f37e4e15b7552989f43384017852cd5b1646a95',2);
