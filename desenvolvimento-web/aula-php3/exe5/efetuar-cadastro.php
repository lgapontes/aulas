<?php

session_start();

require_once "modelo.php";

function validacaoSeguranca() {
    if ( !isset($_POST['email']) ) {
        throw new Exception("Email não enviado pelo formulário!");
    }
    if ( !isset($_POST['nome']) ) {
        throw new Exception("Nome não enviado pelo formulário!");
    }
    if ( !isset($_POST['perfil']) ) {
        throw new Exception("Perfil não enviado pelo formulário!");
    }
    if ( !isset($_POST['senha']) ) {
        throw new Exception("Senha não enviada pelo formulário!");
    }
    if ( !isset($_POST['confirmar-senha']) ) {
        throw new Exception("Confirmação de senha não enviada pelo formulário!");
    }
    if ( !isset($_POST['termos']) ) {
        throw new Exception("É necessário aceitar os termos!");
    }
}

try {
    validacaoSeguranca();

    $usuario = new Usuario();
    $usuario->setEmail($_POST['email']);
    $usuario->setNome($_POST['nome']);
    $usuario->setSenha($_POST['senha'],$_POST['confirmar-senha']);

    $perfil = Perfil::obter($_POST['perfil']);
    $usuario->setPerfil($perfil);

    $_SESSION['usuario'] = serialize($usuario);

    header('Location: cadastro.php');
    die();

} catch(Exception $erro) {
    $_SESSION['mensagem-erro'] = $erro->getMessage();
    header('Location: cadastro.php');
    die();
}
