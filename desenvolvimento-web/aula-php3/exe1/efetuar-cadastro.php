<?php

session_start();

require_once "modelo.php";

function emailExiste() {
    return isset($_POST['email']);
}
function nomeExiste() {
    return isset($_POST['nome']);
}

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
try {

    if ( !emailExiste() ) {
        throw new Exception("Email não enviado pelo formulário!");
    }
    if ( !nomeExiste() ) {
        throw new Exception("Nome não enviado pelo formulário!");
    }

    $usuario = new Usuario();
    $usuario->setEmail($_POST['email']);
    $usuario->setNome($_POST['nome']);

    $_SESSION['usuario'] = serialize($usuario);

    header('Location: cadastro.php');
    die();

} catch(Exception $erro) {
    $_SESSION['mensagem-erro'] = $erro->getMessage();
    header('Location: cadastro.php');
    die();
}
