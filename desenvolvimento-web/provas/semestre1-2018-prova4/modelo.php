<?php

class Pergunta {
    private $codigo;
    private $descricao;
    private $qtdeRespostas;

    public function __construct($codigo,$descricao,$qtdeRespostas) {
        $this->codigo = $codigo;
        $this->descricao = $descricao;
        $this->qtdeRespostas = $qtdeRespostas;
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function getDescricao() {
        return $this->descricao;
    }
    public function getQtdeRespostas() {
        return $this->qtdeRespostas;
    }
}
