<?php

require_once "conexao.php";
require_once "modelo.php";

function listarPerguntas() {
    $conexao = Banco::getConexao();
    $lista = array();
    $resultado = $conexao->query('select p.codigo, p.descricao, count(r.codigo) as qtde_respostas from perguntas p left join respostas r on p.codigo = r.codigoPergunta group by 1,2;');

    if ($resultado->num_rows > 0) {
        while ($linha = $resultado->fetch_assoc()) {
            $pergunta = new Pergunta(
                $linha['codigo'],
                $linha['descricao'],
                $linha['qtde_respostas']
            );
            array_push($lista,$pergunta);
        }
    }

    return $lista;
}
