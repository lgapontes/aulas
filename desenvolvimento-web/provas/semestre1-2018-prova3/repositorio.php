<?php

require_once "conexao.php";
require_once "modelo.php";

function listarUsuarios() {
    $conexao = Banco::getConexao();
    $lista = array();
    $resultado = $conexao->query('select * from usuarios');

    if ($resultado->num_rows > 0) {
        while ($linha = $resultado->fetch_assoc()) {
            $usuario = new Usuario($linha['codigo'],$linha['login']);
            array_push($lista,$usuario);
        }
    }

    return $lista;
}
