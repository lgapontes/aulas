<!DOCTYPE HTML>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8" />
        <title>Prova 3</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/favicon.ico">
        <style>
            main {
                display: block;
                box-sizing: border-box;
                max-width: 400px;
                min-height: 500px;
                margin: 0 auto;
                background-image: linear-gradient(to bottom right, #e6e6ed, #c8c8c8);
                box-shadow: 1px 1px 3px #000000;
            }

            main::after {
                content: "";
                display: block;
                height: 0;
                clear: both;
            }

            main h1 {
                font-size: 20px;
                font-weight: bold;
                text-align: center;
                padding: 10px;
            }

            table {
                width: 94%;
                margin: 0 auto;
                box-sizing: border-box;
            }

            table tr, th, td {
                border-collapse: collapse;
                border: 1px solid #000000;
                padding: 4px;
            }

            table th {
                background-color: #000000;
                color: #ffffff;
            }

            table th:first-child {
                width: 20%;
            }

            table th:last-child {
                width: 74%;
            }

            tr.loading {
                text-align: center;
                background-color: #ffffff;
                font-weight: bold;
            }

        </style>
    </head>
    <body>

        <main>
            <h1>Lista de Usuários</h1>

            <table id="tabela">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Login</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="loading">
                        <td colspan="2">Carregando...</td>
                    </tr>
                    <?php
                        require_once "repositorio.php";
                        $lista = listarUsuarios();
                        foreach ($lista as $usuario):
                    ?>
                        <tr class="outras">
                            <td><?= $usuario->getCodigo() ?></td>
                            <td><?= $usuario->getLogin() ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </main>

        <script>
            window.addEventListener('load', function(){
                document.querySelector('tr.loading').style.display = 'none';
            });
        </script>
    </body>
</html>
