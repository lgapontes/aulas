<?php

error_reporting(E_ERROR | E_PARSE);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

class Banco {

    private static $conexao;

    private function __construct() {}

    public static function getConexao() {
        if (!self::$conexao) {
            //self::$conexao = new mysqli('127.0.0.1','quiz_user','123eja','quiz_database');
            self::$conexao = new mysqli('linu.com.br','linuc318_quiz','123eja','linuc318_quiz_database');
            mysqli_set_charset(self::$conexao, "utf8");
        }
        return self::$conexao;
    }

}
