<?php
    require_once "cabecalho.php";
    require_once "controller-jogo.php";

    $jogador = obterJogador();
?>

<div class="vida">
    <img src="img/vida-<?= $jogador->getVida() ?>.png" />
</div>

<?php
    if ($jogador->estaVivo()):
        if ($jogador->existeProximaPergunta()):
            $pergunta = $jogador->proximaPergunta();
            salvarJogador($jogador);
?>
        <form action="index.php" method="POST">
            <label class="pergunta"><?= htmlspecialchars($pergunta->getDescricao()) ?></label>

            <?php foreach($pergunta->getRespostas() as $resposta): ?>
                <div class="resposta">
                    <input type="radio" name="resposta" value="<?= $resposta->getCodigo() ?>" required />
                    <label><?= htmlspecialchars($resposta->getDescricao()) ?></label>
                </div>
            <?php endforeach; ?>

            <button>Responder</button>
        </form>
    <?php else: ?>
        <span class="status-jogo">Parabéns!</span>
        <a href="index.php" class="link-acao">Recomeçar Jogo</a>
    <?php endif; ?>
<?php else: ?>
    <span class="status-jogo">Game Over</span>
    <a href="index.php" class="link-acao">Recomeçar Jogo</a>
<?php endif; ?>

<?php
    require_once "rodape.php";
?>
