<?php

require_once "conexao.php";
require_once "modelo.php";

function obterTodasPerguntas() {
    $conexao = Banco::getConexao();
    $query = $conexao->prepare(
        "select p.codigo, p.descricao from perguntas p " .
        "where (select count(*) from respostas r where r.codigoPergunta = p.codigo) = 5"
    );

    $query->execute();
    $query->store_result();
    $query->bind_result($codigo, $descricao);

    $perguntas = array();

    if ($query->num_rows > 0) {

        while ($query->fetch()) {
            $pergunta = new Pergunta($descricao);
            $pergunta->setCodigo($codigo);
            array_push($perguntas,$pergunta);
        }

        return $perguntas;

    } else {
        throw new Exception("Nenhuma pergunta encontrada!");
    }
}

function obterRespostas($codigoPergunta) {
    $conexao = Banco::getConexao();
    $query = $conexao->prepare("select codigo, descricao, correta from respostas where codigoPergunta = ?");
    $query->bind_param("i",$codigoPergunta);
    $query->execute();
    $query->store_result();
    $query->bind_result($codigo,$descricao,$correta);

    $respostas = array();

    if ($query->num_rows > 0) {

        while ($query->fetch()) {
            $resposta = new Resposta($descricao, $correta);
            $resposta->setCodigo($codigo);
            array_push($respostas,$resposta);
        }

        return $respostas;

    } else {
        throw new Exception("Nenhuma resposta encontrada!");
    }
}

function obterUsuario($codigoUsuario) {
    $conexao = Banco::getConexao();
    $query = $conexao->prepare("select codigo, login from usuarios where codigo = ?");
    $query->bind_param("i",$codigoUsuario);
    $query->execute();
    $query->store_result();
    $query->bind_result($codigo,$login);

    if ($query->num_rows == 1) {
        $query->fetch();
        $usuario = new Usuario($login);
        $usuario->setCodigo($codigo);
        return $usuario;
    } else {
        throw new Exception("Usuário não encontrado!");
    }
}
