
-- Deve ser utilizado com o usuário root
create database quiz_database CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
create user 'quiz_user'@'localhost' identified by '123eja';
grant all on quiz_database.* to 'quiz_user'@'localhost' identified by '123eja';

-- Deve ser utilizado com o usuário quiz_user
create table usuarios (
    codigo bigint auto_increment,
    login varchar(30) not null unique,
    senha varchar(64) not null,
    primary key (codigo)
);

create table perguntas (
    codigo bigint auto_increment,
    codigoUsuario bigint not null,
    descricao varchar(255) not null,
    foreign key (codigoUsuario) references usuarios(codigo),
    primary key (codigo)
);

create table respostas (
    codigo bigint auto_increment,
    codigoPergunta bigint not null,
    descricao varchar(255) not null,
    correta boolean not null,
    foreign key (codigoPergunta) references perguntas(codigo),
    primary key(codigo)
);

insert into usuarios (codigo,login,senha) values
    (1,'joao','df19ec5a55cfeb9597d0bd235f37e4e15b7552989f43384017852cd5b1646a95'),
    (2,'maria','df19ec5a55cfeb9597d0bd235f37e4e15b7552989f43384017852cd5b1646a95');

insert into perguntas (codigo,codigoUsuario,descricao) values
    (1,1,'O que significa HTML?');

insert into respostas (codigoPergunta,descricao,correta) values
    (1,'Home Tool Markup Language',false),
    (1,'Hyper Text Markup Language',true),
    (1,'Hyper Tag Model Language',false),
    (1,'Hyperlinks and Text Markup Language',false),
    (1,'Não tem nenhum significado',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (2,1,'Quem padroniza a especificação do HTML?');

insert into respostas (codigoPergunta,descricao,correta) values
    (2,'Mozilla',false),
    (2,'Microsoft',false),
    (2,'W3C',true),
    (2,'Google',false),
    (2,'Não há padronização',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (3,1,'Qual a última tag utilizada para títulos?');

insert into respostas (codigoPergunta,descricao,correta) values
    (3,'<h3>',false),
    (3,'<t6>',false),
    (3,'<title6>',false),
    (3,'<h6>',true),
    (3,'Não há tags reservadas para esta finalidade',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (4,1,'Qual tag deve ser utilizada para quebra de linhas?');

insert into respostas (codigoPergunta,descricao,correta) values
    (4,'<br>',true),
    (4,'<break>',false),
    (4,'<n>',false),
    (4,'<cr>',false),
    (4,'<return>',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (5,1,'Para que serve as tags <i> e <b> respectivamente?');

insert into respostas (codigoPergunta,descricao,correta) values
    (5,'São utiilzadas para criar links e estilizar textos em negrito.',false),
    (5,'A tag <i> não existe e a tag <b> estiliza textos em caixa-alta.',false),
    (5,'A tag <i> estiliza texto em itálico e a tag <b> estiliza texto em negrito.',true),
    (5,'Nenhuma das duas tags existem.',false),
    (5,'Servem para criar listas numeradas e pular linhas no texto.',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (6,1,'Qual é a maneira correta de criar um link?');

insert into respostas (codigoPergunta,descricao,correta) values
    (6,'<a name="http://site.com">Site</a>',false),
    (6,'<a>http://site.com</a>',false),
    (6,'<a href="http://site.com">Site</a>',true),
    (6,'<a url="http://site.com">Site</a>',false),
    (6,'<a h="http://site.com" text="Site" />',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (7,1,'Para que serve a tag <tr>?');

insert into respostas (codigoPergunta,descricao,correta) values
    (7,'Quebrar linha de texto na página',false),
    (7,'Criar linha em uma tabela',true),
    (7,'Criar coluna em uma tabela',false),
    (7,'Atribuir espaço antes do texto',false),
    (7,'Esta tag não existe',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (8,1,'Qual tag permite criar uma lista enumerada?');

insert into respostas (codigoPergunta,descricao,correta) values
    (8,'<list>',false),
    (8,'<dl>',false),
    (8,'<ol>',true),
    (8,'<ul>',false),
    (8,'Não existe tag para esta necessidade',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (9,1,'Qual é a sintaxe correta para criação de uma tag de entrada de texto (input) em um formulário?');

insert into respostas (codigoPergunta,descricao,correta) values
    (9,'<input type="text">',true),
    (9,'<textinput type="text">',false),
    (9,'<input type="textfield">',false),
    (9,'<textfield>',false),
    (9,'HTML não trabalha com formulários',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (10,1,'O que acontece se nós não definirmos a tag <title> (no <head> do documento HTML) para o título da página?');

insert into respostas (codigoPergunta,descricao,correta) values
    (10,'O site não será exibido.',false),
    (10,'O navegador vai redirecionar para o endereço da W3C, indicando ao usuário que a tag <title> está faltando.',false),
    (10,'O navegador mostrará uma mensagem de alerta de segurança.',false),
    (10,'O site será exibido, colocando no título da aba do navegador o nome do arquivo HTML da página.',true),
    (10,'Nenhuma das respostas anteriores.',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (11,2,'O que significa o PHP?');

insert into respostas (codigoPergunta,descricao,correta) values
    (11,'Não é um acrônimo, é apenas o nome da linguagem.',false),
    (11,'Private Home Page',false),
    (11,'Personal Hypertext Processor',false),
    (11,'PHP: Hypertext Preprocessor',true),
    (11,'Nenhuma das respostas anteriores.',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (12,2,'Quais são os delimitadores utilizados para envolver um script PHP?');

insert into respostas (codigoPergunta,descricao,correta) values
    (12,'<?php ?>',true),
    (12,'<&> </&>',false),
    (12,'<?php> </?>',false),
    (12,'<script> </script>',false),
    (12,'Não é necessário utilizar delimitadores.',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (13,2,'Como escrever "Hello World" em PHP?');

insert into respostas (codigoPergunta,descricao,correta) values
    (13,'"Hello World";',false),
    (13,'Document.Write("Hello World");',false),
    (13,'echo "Hello World";',true),
    (13,'echo "Hello World"',false),
    (13,'console.log("Hello World");',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (14,2,'Com qual símbolo devem iniciar as variáveis ​​no PHP?');

insert into respostas (codigoPergunta,descricao,correta) values
    (14,'_',false),
    (14,'!',false),
    (14,'&',false),
    (14,'$',true),
    (14,'Não é necessário utilizar símbolos!',false);

insert into perguntas (codigo,codigoUsuario,descricao) values
    (15,2,'Como obter informações de um formulário enviado pelo método "GET"?');

insert into respostas (codigoPergunta,descricao,correta) values
    (15,'Request.Form;',false),
    (15,'Request.QueryString;',false),
    (15,'$_GET[];',true),
    (15,'$GET["formulario"]',false),
    (15,'O PHP não permite usar GET por ser uma má prática no envio de formulários.',false);
