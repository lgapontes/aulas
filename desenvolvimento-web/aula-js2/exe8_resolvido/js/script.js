
function notificar(tag) {
    tag.classList.add('visivel');
    tag.classList.add('desvanecer');    
    
    setTimeout(function(){
        tag.classList.remove('visivel');
        tag.classList.remove('desvanecer');
    },4000);
}

var tagNome = document.querySelector('#nome');
var botaoLogar = document.querySelector('#logar');
var notificacaoSucesso = document.querySelector('#sucesso');
var notificacaoFalha = document.querySelector('#falha');

var tagDivSistema = document.querySelector('#sistema');

botaoLogar.addEventListener('click',function(){
    var nome = tagNome.value;
    
    if (nome.length == 0) {
        // Nome invÃ¡lido
        notificacaoFalha.textContent = 'Nenhum nome foi digitado!';
        notificar(notificacaoFalha);
    } else {
        // Nome vÃ¡lido
        notificacaoSucesso.textContent = 'Seja bem-vindo ' + nome + '!';
        notificar(notificacaoSucesso);

        tagNome.disabled = true;
        botaoLogar.disabled = true;
        tagDivSistema.classList.add('visivel');
    }
})

var tagForm = document.querySelector('form');
tagForm.addEventListener('submit',function(evento){
    evento.preventDefault();
    var formulario = evento.target;

    // Obtendo valor do tipo
    var tipo = '';
    var tagRadios = formulario.querySelectorAll('input[type=radio]');    
    tagRadios.forEach(function(entry){
        if (entry.checked) {
            tipo = entry.value;
        }
    });

    // Obtendo o texto da tarefa
    var texto = formulario.texto.value;

    // Criando nova tarefa no DOM
    var divColumn = document.createElement('div');
    divColumn.classList.add('column');

    var divCard = document.createElement('div');
    divCard.classList.add('card');

    var divCardContent = document.createElement('div');
    divCardContent.classList.add('card-content');

    var divContent = document.createElement('div');
    divContent.classList.add('content');

    var spanTexto = document.createElement('span');
    spanTexto.textContent = texto;

    var br = document.createElement('br');

    var spanTipo = document.createElement('span');
    spanTipo.classList.add('tag');
    spanTipo.classList.add('is-light');
    spanTipo.textContent = tipo;

    var footer = document.createElement('footer');
    footer.classList.add('card-footer');

    var linkApagar = document.createElement('a');
    linkApagar.classList.add('card-footer-item');
    linkApagar.classList.add('has-background-light');
    linkApagar.classList.add('has-text-grey-dark');
    linkApagar.textContent = 'Apagar';

    var linkFeita = document.createElement('a');
    linkFeita.classList.add('card-footer-item');
    linkFeita.classList.add('has-background-success');
    linkFeita.classList.add('has-text-white');
    var strong = document.createElement('strong');
    strong.textContent = 'Feita!';
    linkFeita.appendChild(strong);

    // Organizando DOM
    divColumn.appendChild(divCard);
    divCard.appendChild(divCardContent);
    divCardContent.appendChild(divContent);
    divContent.appendChild(spanTexto);
    divContent.appendChild(br);
    divContent.appendChild(spanTipo);
    divCard.appendChild(footer);
    footer.appendChild(linkApagar);
    footer.appendChild(linkFeita);

    var tarefas = document.querySelector('#lista-tarefas');
    tarefas.appendChild(divColumn);
});