function notificacao(tag) {
    tag.classList.add('mostrar');
    tag.classList.add('sumir');

    setTimeout(function(){
        tag.classList.remove('mostrar');
        tag.classList.remove('sumir');
    },4000);
}


// Exercício 1
var tagNome = document.querySelector('#nome');
var botaoLogar = document.querySelector('#logar');

// Exercício 2 e 3

var notificacaoSucesso = document.querySelector('#sucesso');
var notificacaoFalha = document.querySelector('#falha');

botaoLogar.addEventListener('click',function(){
    var nome = tagNome.value;
    console.log('Nome do usuário: ' + nome);

    if (nome.length == 0) {
        notificacao(notificacaoFalha);
    } else {
        tagNome.disabled = true;
        this.disabled = true;

        notificacaoSucesso.textContent = 'Seja bem-vindo ' + nome + '!';
        notificacao(notificacaoSucesso);

        document.querySelector('#sistema').classList.add('mostrar');
    }

});

var formulario = document.querySelector('#nova-tarefa');
formulario.addEventListener('submit',function(evento){
    evento.preventDefault();

    var descricao = evento.target.descricao.value;
    var tipo = "";

    var radios = evento.target.querySelectorAll('input[type=radio]');
    radios.forEach(function(radio){
       if (radio.checked) {
           tipo = radio.value;
       } 
    });

    var tarefa = {
        descricao: descricao,
        tipo: tipo
    };

    renderizarTarefa(tarefa);    
});

function renderizarTarefa(tarefa) {
    var tagDivColumn = document.createElement('div');
    tagDivColumn.classList.add('column');

    var tagDivCard = document.createElement('div');
    tagDivCard.classList.add('card');
    tagDivCard.textContent = tarefa.descricao;

    tagDivColumn.appendChild(tagDivCard);

    var tagTarefas = document.querySelector('#tarefas');
    tagTarefas.appendChild(tagDivColumn);
}

function renderizarTarefa_comTemplateString(tarefa) {
    var tagTarefa = `
    <div class="column">
        <div class="card">
            <div class="card-content">
                <div class="content">
                    ${tarefa.descricao}
                    <br />
                    <span class="tag is-light">${tarefa.tipo}</span>
                    <span class="is-size-7 has-text-grey-light">20:33 12/02/2018</span>
                </div>
            </div>
            <footer class="card-footer">						    
                <a href="#" class="card-footer-item has-background-light has-text-grey-dark">Apagar</a>
                <a href="#" class="card-footer-item has-background-success has-text-white"><strong>Feita!</strong></a>
            </footer>
        </div>
    </div>
    `;
    var tagTarefas = document.querySelector('#tarefas');
    tagTarefas.innerHTML = tagTarefa;
}