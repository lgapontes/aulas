use sistemadb;

create table salas (
    codigo bigint not null auto_increment,
    descricao varchar(100) not null,
    primary key (codigo)
);

insert into salas (descricao) values ('Sala 1');
insert into salas (descricao) values ('Sala 2');
insert into salas (descricao) values ('Sala 3');
commit;


create table apresentacoes (
    codigo bigint not null auto_increment,
    descricao varchar(100) not null,
    data datetime not null,
    pathImagem varchar(1024) not null,
    codigoSala bigint not null,
    primary key (codigo),
    foreign key (codigoSala) references salas(codigo)
);

insert into apresentacoes (descricao,data,pathImagem,codigoSala) values ('Palestra XML','2008-05-10 19:00:00','img/apresentacoes/img1.png',1);
insert into apresentacoes (descricao,data,pathImagem,codigoSala) values ('Palestra SQL','2008-05-11 17:30:00','img/apresentacoes/img2.png',3);
insert into apresentacoes (descricao,data,pathImagem,codigoSala) values ('Palestra NoSQL','2008-05-10 21:00:00','img/apresentacoes/img3.png',2);
insert into apresentacoes (descricao,data,pathImagem,codigoSala) values ('Palestra NodeJS','2008-05-10 14:10:00','img/apresentacoes/img4.png',2);
commit;