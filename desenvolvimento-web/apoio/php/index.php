<?php
	$servername = "localhost";
	$username = "usuario";
	$password = "123eja";
	$dbname = "sistemadb";

	$conexao = new mysqli($servername, $username, $password, $dbname);
	if ($conexao->connect_error) {
	    die("Erro ao conectar ao banco: " . $conn->connect_error);
	} 

	$sql = "select * from apresentacoes";
	$result = $conexao->query($sql);

	$json = new stdclass();
	$json->apresentacoes = array();
	$numero_linha = 0;

	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$json->apresentacoes[$numero_linha] = new stdclass();	
			$json->apresentacoes[$numero_linha]->codigo = $row["codigo"];
			$json->apresentacoes[$numero_linha]->descricao = $row["descricao"];
			$json->apresentacoes[$numero_linha]->data = $row["data"];
			$json->apresentacoes[$numero_linha]->pathImagem = $row["pathImagem"];
			$json->apresentacoes[$numero_linha]->codigoSala = $row["codigoSala"];	        
	        $numero_linha = $numero_linha + 1;
	    }
	}
	
	$conexao->close();
	
	echo json_encode($json);
?>