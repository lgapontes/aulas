<?php
	$servername = "localhost";
	$username = "linuc318_usuario";
	$password = "123eja";
	$dbname = "linuc318_produtos";

	$conexao = new mysqli($servername, $username, $password, $dbname);
	if ($conexao->connect_error) {
	    die("Erro ao conectar ao banco: " . $conn->connect_error);
	} 

	$sql = "select * from PRODUTOS";
	$result = $conexao->query($sql);

	$object = new stdclass();
	$object->retorno = new stdclass();

	$object->retorno->status_processamento = 3;
	$object->retorno->status = "OK";
	$object->retorno->pagina = 1;
	$object->retorno->numero_paginas = 1;
	$object->retorno->produtos = array();
	$numero_linha = 0;

	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$object->retorno->produtos[$numero_linha] = new stdclass();	
			$object->retorno->produtos[$numero_linha]->id = $row["ID"];
			$object->retorno->produtos[$numero_linha]->codigo = $row["CODIGO"];
			$object->retorno->produtos[$numero_linha]->nome = $row["NOME"];
			$object->retorno->produtos[$numero_linha]->preco = $row["PRECO"];
			$object->retorno->produtos[$numero_linha]->precoPromocional = $row["PRECO_PROMOCIONAL"];
			$object->retorno->produtos[$numero_linha]->precoCusto = $row["PRECO_CUSTO"];
			$object->retorno->produtos[$numero_linha]->precoCustoMedio = $row["PRECO_CUSTO_MEDIO"];
			$object->retorno->produtos[$numero_linha]->unidade = $row["UNIDADE"];	        
	        $numero_linha = $numero_linha + 1;
	    }
	}
	
	$conexao->close();
	
	echo json_encode($object);
?>