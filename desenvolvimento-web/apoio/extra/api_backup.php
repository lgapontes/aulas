<?php

	function obterMarcaDoProduto($conexao,$id) {
		$url = "https://api.tiny.com.br/api2/produto.obter.php";
		$parametros = "?token=def1469763de849a6681bf70bc20f04e59f31c52&formato=JSON&id=";

		$json = file_get_contents( $url . $parametros . strval($id) );
		$objeto = json_decode($json);

		if (strcmp($objeto->retorno->status,"OK") != 0) {
			die('Erro ao consultar produto individual: ' . $id);
		}

		return $objeto->retorno->produto->marca;
	}

	function salvarProdutosNoBanco($conexao,$codigo_atualizacao_antigo) {

		// Registra o timestamp da atualizacao dos produtos
		$sql_timestamp = "INSERT INTO LEITURAS_API (ID,EM_ATUALIZACAO,DATA_ATUALIZACAO) VALUES (null,TRUE,null)";
		if ( !($conexao->query($sql_timestamp) === TRUE) ) {
			die( "Error: " . $sql_timestamp . "<br>" . $conexao->error );
		}

		// Obtem o codigo de atualizacao
		$sql = "select ID from LEITURAS_API WHERE EM_ATUALIZACAO = TRUE";
		$result = $conexao->query($sql);
		$codigo_atualizacao = 0;

		if ($result->num_rows == 1) {
			$row = $result->fetch_assoc();
			$codigo_atualizacao = strtotime($row["ID"]);
		} else {
			die( "Erro ao buscar o codigo da atualizacao" );
		}

		// Obtem os dados
		$pagina = 1;
		$numero_paginas = 99999;

		$url = "https://api.tiny.com.br/api2/produtos.pesquisa.php";
		$parametros = "?token=def1469763de849a6681bf70bc20f04e59f31c52&formato=JSON&pagina=";

		$produtos = array();
		$contador_produtos = 0;

		while($pagina <= $numero_paginas) {
			$json = file_get_contents( $url . $parametros . strval($pagina) );
			$objeto = json_decode($json);

			$pagina = intval($objeto->retorno->pagina);
			$numero_paginas = intval($objeto->retorno->numero_paginas);

			foreach ($objeto->retorno->produtos as $entry) {
				$produtos[$contador_produtos] = new stdclass();
				$produtos[$contador_produtos]->id 							= $entry->produto->id;
				$produtos[$contador_produtos]->idLeitura 				= $codigo_atualizacao;
				$produtos[$contador_produtos]->codigo 					= $entry->produto->codigo;
				$produtos[$contador_produtos]->nome 						= $entry->produto->nome;
				$produtos[$contador_produtos]->marca 						= ""; // Vai obter posteriormente
				$produtos[$contador_produtos]->preco 						= $entry->produto->preco;
				$produtos[$contador_produtos]->precoPromocional = $entry->produto->preco_promocional;
				$produtos[$contador_produtos]->precoCusto 			= $entry->produto->preco_custo;
				$produtos[$contador_produtos]->precoCustoMedio 	= $entry->produto->preco_custo_medio;
				$produtos[$contador_produtos]->unidade 					= $entry->produto->unidade;
				$produtos[$contador_produtos]->tipoVariacao 		= $entry->produto->tipoVariacao;
				$produtos[$contador_produtos]->observacoes 			= $entry->produto->obs;

				$contador_produtos++;
			}

			$pagina++;
		}

		// Obtem a marca dos produtos
		foreach ($produtos as $produto) {
			$marca = obterMarcaDoProduto($conexao,$produto->id);
			$produto->marca = $marca;

			$sql = "INSERT INTO PRODUTOS (" .
				"ID,ID_LEITURA,CODIGO,NOME,MARCA,PRECO,PRECO_PROMOCIONAL,PRECO_CUSTO,PRECO_CUSTO_MEDIO,UNIDADE,TIPO_VARIACAO,OBSERVACOES,DATA_ATUALIZACAO) " .
				"values (" . $produto->id . "," . $codigo_atualizacao . ",'" . $produto->codigo . "','" .
				$produto->nome . "','" . $produto->marca . "'," . $produto->preco . "," .
				$produto->precoPromocional . "," . $produto->precoCusto . "," . $produto->precoCustoMedio . ",'" .
				$produto->unidade . "','" . $produto->tipoVariacao . "','" . $produto->observacoes . "',NULL)";

			if ( !($conexao->query($sql) === TRUE) ) {
			    die( "Error: " . $sql . "<br>" . $conexao->error );
			}

			// Espera 3 segundos para consultar no máximo 20 produtos por minuto
			sleep(3);
		}

		// Atualizar leitura para fechar carregamento dos produtos novos
		$sql_atualizar_leitura = "update LEITURAS_API set EM_ATUALIZACAO = FALSE WHERE ID =" . $codigo_atualizacao;
		if ( !($conexao->query($sql_atualizar_leitura) === TRUE) ) {
			die( "Error: " . $sql_atualizar_leitura . "<br>" . $conexao->error );
		}

		// Apagar produtos
		$sql_deletar_produtos = "delete from PRODUTOS where ID_LEITURA = " . $codigo_atualizacao_antigo;
		if ( !($conexao->query($sql_deletar_produtos) === TRUE) ) {
			die( "Error: " . $sql_deletar_produtos . "<br>" . $conexao->error );
		}
	}

	class MyThread extends Thread{

    private $conexao;
		private $codigo_atualizacao_antigo;

    public function __construct($c,$a) {
        $this->conexao = $c;
				$this->codigo_atualizacao_antigo = $a;
    }

		function run(){
			salvarProdutosNoBanco($this->conexao,$this->codigo_atualizacao_antigo);
    }
	}

	function precisaBuscarProdutos($conexao) {
		$sql = "select ID, DATA_ATUALIZACAO from LEITURAS_API WHERE EM_ATUALIZACAO = FALSE ORDER BY DATA_ATUALIZACAO DESC LIMIT 1";
		$result = $conexao->query($sql);
		$codigo_atualizacao = 0;

		if ($result->num_rows == 1) {
			$row = $result->fetch_assoc();
			$codigo_atualizacao = strtotime($row["ID"]);
			$data_atualizacao = strtotime($row["DATA_ATUALIZACAO"]);
			$minutos = abs(time() - $data_atualizacao) / 60;

			if ($minutos < 720) { // 12 horas
				return array(False, $codigo_atualizacao);
			}
		}

		// Se precisar atualizar, deve-se verificar se ja existe a carga assincrona
		$sql = "select ID from LEITURAS_API WHERE EM_ATUALIZACAO = TRUE";
		$result = $conexao->query($sql);

		if ($result->num_rows > 0) {
				return array(False, $codigo_atualizacao);
		}

		return array(True, $codigo_atualizacao);
	}

	function obterProdutosCache($conexao,$codigo_atualizacao) {
		$sql = "select * from PRODUTOS where ID_LEITURA = " . $codigo_atualizacao;
		$result = $conexao->query($sql);

		$object = new stdclass();
		$object->listaProdutos = array();
		$numero_linha = 0;

		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
						$object->listaProdutos[$numero_linha] = new stdclass();
						$object->listaProdutos[$numero_linha]->id = $row["ID"];
						$object->listaProdutos[$numero_linha]->codigo = $row["CODIGO"];
						$object->listaProdutos[$numero_linha]->nome = $row["NOME"];
						$object->listaProdutos[$numero_linha]->marca = $row["MARCA"];
						$object->listaProdutos[$numero_linha]->preco = $row["PRECO"];
						$object->listaProdutos[$numero_linha]->precoPromocional = $row["PRECO_PROMOCIONAL"];
						$object->listaProdutos[$numero_linha]->precoCusto = $row["PRECO_CUSTO"];
						$object->listaProdutos[$numero_linha]->precoCustoMedio = $row["PRECO_CUSTO_MEDIO"];
						$object->listaProdutos[$numero_linha]->unidade = $row["UNIDADE"];
						$object->listaProdutos[$numero_linha]->tipoVariacao = $row["TIPO_VARIACAO"];
						$object->listaProdutos[$numero_linha]->observacoes = $row["OBSERVACOES"];
						$object->listaProdutos[$numero_linha]->dataAtualizacao = $row["DATA_ATUALIZACAO"];
		        $numero_linha = $numero_linha + 1;
		    }
		}

		return $object;
	}

	// **********************************************************************************************************

	// Token que deve ser informado no header
	$token = "SJSfmhLXnJ";

	// Token informado pelo usuário
	$token_usuario = $_GET['token'];

	// Se token estiver inválido, retorna código 401
	if (strcmp($token,$token_usuario) != 0) {
		header('HTTP/1.0 401');
		die('Token invalido: 401');
	}

	// Dados de conexao com banco
	$servername = "localhost";
	$username = "usuario";
	$password = "123eja";
	$dbname = "sistemadb";

	$conexao = new mysqli($servername, $username, $password, $dbname);
	if ($conexao->connect_error) {
	    die("Erro ao conectar ao banco: " . $conn->connect_error);
	}

	// Verifica se precisa atualizar o cache
	$precisa = precisaBuscarProdutos($conexao);


	if ($precisa[0]) {

		// Execucao assincrona
		$worker = new MyThread($conexao,$precisa[1]);
		$worker->start();
	}

	// Obtem dados do cache
	$json = obterProdutosCache($conexao);

	// Fecha conexao
	$conexao->close();

	echo json_encode($json);
?>
