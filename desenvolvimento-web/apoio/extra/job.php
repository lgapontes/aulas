<?php

	function obterMarcaDoProduto($conexao,$id) {
		$url = "https://api.tiny.com.br/api2/produto.obter.php";
		$parametros = "?token=def1469763de849a6681bf70bc20f04e59f31c52&formato=JSON&id=";

		$json = file_get_contents( $url . $parametros . strval($id) );
		$objeto = json_decode($json);

		if (strcmp($objeto->retorno->status,"OK") != 0) {
			die('Erro ao consultar produto individual: ' . $id);
		}

		return $objeto->retorno->produto->marca;
	}

	function salvarProdutosNoBanco($conexao,$codigo_atualizacao_antigo) {

		// Registra o timestamp da atualizacao dos produtos
		$sql_timestamp = "INSERT INTO LEITURAS_API (ID,EM_ATUALIZACAO,DATA_ATUALIZACAO) VALUES (null,TRUE,null)";
		if ( !($conexao->query($sql_timestamp) === TRUE) ) {
			die( "Error: " . $sql_timestamp . "<br>" . $conexao->error );
		}

		// Obtem o codigo de atualizacao
		$sql = "select ID from LEITURAS_API WHERE EM_ATUALIZACAO = TRUE";
		$result = $conexao->query($sql);
		$codigo_atualizacao = 0;

		if ($result->num_rows == 1) {
			$row = $result->fetch_assoc();
			$codigo_atualizacao = strtotime($row["ID"]);
		} else {
			die( "Erro ao buscar o codigo da atualizacao" );
		}

		// Obtem os dados
		$pagina = 1;
		$numero_paginas = 99999;

		$url = "https://api.tiny.com.br/api2/produtos.pesquisa.php";
		$parametros = "?token=def1469763de849a6681bf70bc20f04e59f31c52&formato=JSON&pagina=";

		$produtos = array();
		$contador_produtos = 0;

		while($pagina <= $numero_paginas) {
			$json = file_get_contents( $url . $parametros . strval($pagina) );
			$objeto = json_decode($json);

			$pagina = intval($objeto->retorno->pagina);
			$numero_paginas = intval($objeto->retorno->numero_paginas);

			foreach ($objeto->retorno->produtos as $entry) {
				$produtos[$contador_produtos] = new stdclass();
				$produtos[$contador_produtos]->id 							= $entry->produto->id;
				$produtos[$contador_produtos]->idLeitura 				= $codigo_atualizacao;
				$produtos[$contador_produtos]->codigo 					= $entry->produto->codigo;
				$produtos[$contador_produtos]->nome 						= $entry->produto->nome;
				$produtos[$contador_produtos]->marca 						= ""; // Vai obter posteriormente
				$produtos[$contador_produtos]->preco 						= $entry->produto->preco;
				$produtos[$contador_produtos]->precoPromocional = $entry->produto->preco_promocional;
				$produtos[$contador_produtos]->precoCusto 			= $entry->produto->preco_custo;
				$produtos[$contador_produtos]->precoCustoMedio 	= $entry->produto->preco_custo_medio;
				$produtos[$contador_produtos]->unidade 					= $entry->produto->unidade;
				$produtos[$contador_produtos]->tipoVariacao 		= $entry->produto->tipoVariacao;
				$produtos[$contador_produtos]->observacoes 			= $entry->produto->obs;

				$contador_produtos++;
			}

			$pagina++;
		}

		// Obtem a marca dos produtos
		foreach ($produtos as $produto) {
			$marca = obterMarcaDoProduto($conexao,$produto->id);
			$produto->marca = $marca;

			// Inseri no banco de dados
			$sql = "INSERT INTO PRODUTOS (" .
				"ID,ID_LEITURA,CODIGO,NOME,MARCA,PRECO,PRECO_PROMOCIONAL,PRECO_CUSTO,PRECO_CUSTO_MEDIO,UNIDADE,TIPO_VARIACAO,OBSERVACOES,DATA_ATUALIZACAO) " .
				"values (" . $produto->id . "," . $codigo_atualizacao . ",'" . $produto->codigo . "','" .
				$produto->nome . "','" . $produto->marca . "'," . $produto->preco . "," .
				$produto->precoPromocional . "," . $produto->precoCusto . "," . $produto->precoCustoMedio . ",'" .
				$produto->unidade . "','" . $produto->tipoVariacao . "','" . $produto->observacoes . "',NULL)";

			if ( !($conexao->query($sql) === TRUE) ) {
			    die( "Error: " . $sql . "<br>" . $conexao->error );
			}

			// Espera 3 segundos para consultar no máximo 20 produtos por minuto
			sleep(3);
		}

		// Atualizar leitura para fechar carregamento dos produtos novos
		$sql_atualizar_leitura = "update LEITURAS_API set EM_ATUALIZACAO = FALSE WHERE ID =" . $codigo_atualizacao;
		if ( !($conexao->query($sql_atualizar_leitura) === TRUE) ) {
			die( "Error: " . $sql_atualizar_leitura . "<br>" . $conexao->error );
		}

		// Apagar produtos antigos
		$sql_deletar_produtos = "delete from PRODUTOS where ID_LEITURA = " . $codigo_atualizacao_antigo;
		if ( !($conexao->query($sql_deletar_produtos) === TRUE) ) {
			die( "Error: " . $sql_deletar_produtos . "<br>" . $conexao->error );
		}
	}

	
	
	function versaoDoCache($conexao) {
		$sql = "select ID from LEITURAS_API WHERE EM_ATUALIZACAO = FALSE ORDER BY DATA_ATUALIZACAO DESC LIMIT 1";
		$result = $conexao->query($sql);
		$codigo_atualizacao = 0;

		if ($result->num_rows == 1) {
			$row = $result->fetch_assoc();
			$codigo_atualizacao = strtotime($row["ID"]);					
		}

		return $codigo_atualizacao;
	}
	

	// **********************************************************************************************************	

	// Dados de conexao com banco
	$servername = "localhost";
	$username = "usuario";
	$password = "123eja";
	$dbname = "sistemadb";

	$conexao = new mysqli($servername, $username, $password, $dbname);
	if ($conexao->connect_error) {
	    die("Erro ao conectar ao banco: " . $conn->connect_error);
	}

	// Obtem o código do último cache salvo no banco local
	$codigoCache = versaoDoCache($conexao);
	
	// Atualizar produtos
	salvarProdutosNoBanco($conexao,$codigoCache);	

	// Fecha conexao
	$conexao->close();

	echo "Carga executou com sucesso em " . date('Y-m-d H:i:s') . "\n";
?>
