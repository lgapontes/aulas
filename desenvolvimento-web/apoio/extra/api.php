<?php

	function versaoDoCache($conexao) {
		$sql = "select ID from LEITURAS_API WHERE EM_ATUALIZACAO = FALSE ORDER BY DATA_ATUALIZACAO DESC LIMIT 1";
		$result = $conexao->query($sql);
		$codigo_atualizacao = 0;

		if ($result->num_rows == 1) {
			$row = $result->fetch_assoc();
			$codigo_atualizacao = strtotime($row["ID"]);					
		}

		return $codigo_atualizacao;
	}

	function obterProdutosCache($conexao,$codigo_atualizacao) {
		$sql = "select * from PRODUTOS where ID_LEITURA = " . $codigo_atualizacao;
		$result = $conexao->query($sql);

		$object = new stdclass();
		$object->listaProdutos = array();
		$numero_linha = 0;

		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
						$object->listaProdutos[$numero_linha] = new stdclass();
						$object->listaProdutos[$numero_linha]->id = $row["ID"];
						$object->listaProdutos[$numero_linha]->codigo = $row["CODIGO"];
						$object->listaProdutos[$numero_linha]->nome = $row["NOME"];
						$object->listaProdutos[$numero_linha]->marca = $row["MARCA"];
						$object->listaProdutos[$numero_linha]->preco = $row["PRECO"];
						$object->listaProdutos[$numero_linha]->precoPromocional = $row["PRECO_PROMOCIONAL"];
						$object->listaProdutos[$numero_linha]->precoCusto = $row["PRECO_CUSTO"];
						$object->listaProdutos[$numero_linha]->precoCustoMedio = $row["PRECO_CUSTO_MEDIO"];
						$object->listaProdutos[$numero_linha]->unidade = $row["UNIDADE"];
						$object->listaProdutos[$numero_linha]->tipoVariacao = $row["TIPO_VARIACAO"];
						$object->listaProdutos[$numero_linha]->observacoes = $row["OBSERVACOES"];
						$object->listaProdutos[$numero_linha]->dataAtualizacao = $row["DATA_ATUALIZACAO"];
		        $numero_linha = $numero_linha + 1;
		    }
		}

		return $object;
	}

	// **********************************************************************************************************

	// Token que deve ser informado no header
	$token = "SJSfmhLXnJ";

	// Token informado pelo usuário
	$token_usuario = $_GET['token'];

	// Se token estiver inválido, retorna código 401
	if (strcmp($token,$token_usuario) != 0) {
		header('HTTP/1.0 401');
		die('Token invalido: 401');
	}

	// Dados de conexao com banco
	$servername = "localhost";
	$username = "usuario";
	$password = "123eja";
	$dbname = "sistemadb";

	$conexao = new mysqli($servername, $username, $password, $dbname);
	if ($conexao->connect_error) {
	    die("Erro ao conectar ao banco: " . $conn->connect_error);
	}

	// Obtem o código do último cache salvo no banco local
	$codigoCache = versaoDoCache($conexao);

	// Obtem dados do cache
	$json = obterProdutosCache($conexao,$codigoCache);

	// Fecha conexao
	$conexao->close();

	echo json_encode($json);
?>
