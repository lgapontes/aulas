<?php
	$servername = "localhost";
	$username = "linuc318_usuario";
	$password = "123eja";
	$dbname = "linuc318_produtos";

	$conexao = new mysqli($servername, $username, $password, $dbname);
	if ($conexao->connect_error) {
	    die("Erro ao conectar ao banco: " . $conn->connect_error);
	} 

	$sql = "select * from PRODUTOS";
	$result = $conexao->query($sql);

	$object = new stdclass();
	$object->listaProdutos = array();
	$numero_linha = 0;

	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$object->listaProdutos[$numero_linha] = new stdclass();	
			$object->listaProdutos[$numero_linha]->id = $row["ID"];
			$object->listaProdutos[$numero_linha]->codigo = $row["CODIGO"];
			$object->listaProdutos[$numero_linha]->nome = $row["NOME"];
			$object->listaProdutos[$numero_linha]->preco = $row["PRECO"];
			$object->listaProdutos[$numero_linha]->precoPromocional = $row["PRECO_PROMOCIONAL"];
			$object->listaProdutos[$numero_linha]->precoCusto = $row["PRECO_CUSTO"];
			$object->listaProdutos[$numero_linha]->precoCustoMedio = $row["PRECO_CUSTO_MEDIO"];
			$object->listaProdutos[$numero_linha]->unidade = $row["UNIDADE"];	        
	        $numero_linha = $numero_linha + 1;
	    }
	}
	
	$conexao->close();
	
	echo json_encode($object);
?>