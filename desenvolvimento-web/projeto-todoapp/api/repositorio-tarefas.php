<?php

    function obterTarefa($conexao,$codigo) {
        $resultado = $conexao->query("select tarefas.*, tipos.descricao as tipo from tarefas join tipos on tipos.codigo=tarefas.codigoTipo where tarefas.codigo = {$codigo}");
        if (!$resultado) {
            throw new Exception("Erro no banco de dados: " . $conexao->error);
        }
        $numeroLinhas = $resultado->num_rows;

        if ($numeroLinhas == 1) {
            return $resultado->fetch_assoc();
        } else {
            return null;
        }
    }

    function obterTarefas($conexao,$codigoUsuario) {
        $tarefas = array();

        $query = "select tarefas.*, tipos.descricao as tipo from tarefas join tipos on tipos.codigo=tarefas.codigoTipo order by tarefas.codigo";
        if ($codigoUsuario != null) {
            $query = "select tarefas.*, tipos.descricao as tipo from tarefas join tipos on tipos.codigo=tarefas.codigoTipo where tarefas.concluida=false and tarefas.codigoUsuario = {$codigoUsuario} order by tarefas.codigo";
        }

        $resultado = $conexao->query($query);
        if (!$resultado) {
            throw new Exception("Erro no banco de dados: " . $conexao->error);
        }

        if ($resultado->num_rows > 0) {
            while($tarefa = $resultado->fetch_assoc()) {
                array_push($tarefas,$tarefa);
            }
        }

        return $tarefas;
    }

    function inserirTarefa($conexao,$tarefa) {
        $resultadoInsercao = $conexao->query(
            "insert into tarefas (codigoUsuario,codigoTipo,texto) values " .
            "({$tarefa['codigoUsuario']},{$tarefa['codigoTipo']},'{$tarefa['texto']}')"
        );
        if (!$resultadoInsercao) {
            throw new Exception("Erro no banco de dados: " . $conexao->error);
        }
        $codigoInserido = $conexao->insert_id;

        $resultado = $conexao->query("select tarefas.*,tipos.descricao as tipo from tarefas inner join tipos on tarefas.codigoTipo = tipos.codigo where tarefas.codigo = {$codigoInserido}");
        return $resultado->fetch_assoc();
    }

    function excluirTarefa($conexao,$codigoTarefa) {
        $resultado = $conexao->query("delete from tarefas where codigo = {$codigoTarefa}");
        if (!$resultado) {
            throw new Exception("Erro no banco de dados: " . $conexao->error);
        }

        if ($conexao->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    function concluirTarefa($conexao,$codigoTarefa) {
        $resultado = $conexao->query("update tarefas set concluida = true where codigo = {$codigoTarefa}");
        if (!$resultado) {
            throw new Exception("Erro no banco de dados: " . $conexao->error);
        }

        if ($conexao->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    function obterTipos($conexao) {
        $tipos = array();
        $resultado = $conexao->query("select * from tipos order by codigo");
        if (!$resultado) {
            throw new Exception("Erro no banco de dados: " . $conexao->error);
        }

        if($resultado->num_rows > 0) {
            while($tipo = $resultado->fetch_assoc()) {
                array_push($tipos,$tipo);
            }
        }

        return $tipos;
    }