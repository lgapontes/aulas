<?php
    include("conexao.php");
    include("repositorio-tarefas.php");    

    try {

        /*
            Este trecho é necessário caso o navegador faça um OPTIONS
            para verificar quais são os métodos permitidos.
        */
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            header("Access-Control-Allow-Methods: GET,POST,OPTIONS,DELETE,PUT");
            die();
        
        } else if ($_SERVER["REQUEST_METHOD"] === "GET") {
            $conexao = conectar();
            $tipos = obterTipos($conexao);
            header('Content-Type: application/json');
            echo json_encode($tipos, JSON_UNESCAPED_UNICODE);
            desconectar($conexao);
        } else {
            header("HTTP/1.1 405");
            die();
        }

    } catch (Exception $e) {
        header("HTTP/1.1 500");
        die();
    }