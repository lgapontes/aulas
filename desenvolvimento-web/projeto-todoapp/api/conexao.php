<?php
    function conectar() {
        // Conexao local
        //$conexao = new mysqli('localhost','todoapp_user','123eja','todoapp_database');
        
        // Conexao remota
        $conexao = new mysqli('linu.com.br','linuc318_todoapp','123eja','linuc318_todoapp_database');
        
        if ($conexao->connect_error) {
            throw new Exception("Erro no banco de dados: " . $conexao->connect_error);
        }
        $conexao->set_charset("utf8");
        return $conexao;
    }

    function desconectar($conexao) {
        $conexao->close();
    }