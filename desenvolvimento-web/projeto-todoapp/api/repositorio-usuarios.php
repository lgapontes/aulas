<?php

    function obterTodosUsuarios($conexao) {
        $usuarios = array();
        $resultado = $conexao->query("select * from usuarios");
        if (!$resultado) {
            throw new Exception("Erro no banco de dados: " . $conexao->error);
        }

        if ($resultado->num_rows > 0) {
            while($usuario = $resultado->fetch_assoc()) {
                array_push($usuarios,$usuario);
            }
        }

        return $usuarios;
    }

    function obterUsuario($conexao,$nome) {
        $resultado = $conexao->query("select * from usuarios where nome = '{$nome}'");
        if (!$resultado) {
            throw new Exception("Erro no banco de dados: " . $conexao->error);
        }

        if ($resultado->num_rows == 1) {
            return $resultado->fetch_assoc();
        } else {
            return null;
        }
    }

    function inserirUsuario($conexao,$nome) {
        $resultadoInsercao = $conexao->query("insert into usuarios (nome) values ('{$nome}');");
        if (!$resultadoInsercao) {
            throw new Exception("Erro no banco de dados: " . $conexao->error);
        }
        $codigoInserido = $conexao->insert_id;

        $resultado = $conexao->query("select * from usuarios where codigo = {$codigoInserido}");
        return $resultado->fetch_assoc();
    }

    function alterarExperiencia($conexao,$codigo,$experiencia) {
        $resultadoAtualizacao = $conexao->query("update usuarios set experiencia = experiencia + {$experiencia} where codigo = {$codigo}");
        if (!$resultadoAtualizacao) {
            throw new Exception("Erro no banco de dados: " . $conexao->error);
        }

        $resultado = $conexao->query("select * from usuarios where codigo = {$codigo}");
        if (!$resultado) {
            throw new Exception("Erro no banco de dados: " . $conexao->error);
        }
        if ($resultado->num_rows != 1) {
            throw new Exception("Houve um erro ao buscar o usuario de codigo {$codigo}");
        }
        return $resultado->fetch_assoc();
    }