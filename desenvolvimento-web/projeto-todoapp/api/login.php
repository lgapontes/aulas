<?php
    include("conexao.php");
    include("repositorio-usuarios.php");
    include("repositorio-tarefas.php");

    try {

        /*
            Este trecho é necessário caso o navegador faça um OPTIONS
            para verificar quais são os métodos permitidos.
        */
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            header("Access-Control-Allow-Methods: GET,POST,OPTIONS,DELETE,PUT");
            die();
        
        } else if ($_SERVER["REQUEST_METHOD"] === "POST") {

            $conexao = conectar();

            $body = file_get_contents('php://input');
            $json = json_decode($body, true);

            if ($json == null) {
                header('HTTP/1.0 400');
                die();
            }
            if (!array_key_exists("nome",$json)) {
                header('HTTP/1.0 400');
                die();
            }
        
            $nome = $json["nome"];

            $usuario = obterUsuario($conexao,$nome);
        
            if ($usuario == null) {
                // Usuario nao existe
                $usuario = inserirUsuario($conexao,$nome);            
                $tarefas = array();
                $usuario["tarefas"] = $tarefas;
                header('Content-Type: application/json');
                echo json_encode($usuario,JSON_UNESCAPED_UNICODE);
            } else {
                // Usuario encontrado
                $tarefas = obterTarefas($conexao,$usuario["codigo"]);
                $usuario["tarefas"] = $tarefas;
                header('Content-Type: application/json');
                echo json_encode($usuario,JSON_UNESCAPED_UNICODE);
            }    
            
            desconectar($conexao);
        
        } else {
            header("HTTP/1.1 405");
            die();
        }

    } catch (Exception $e) {
        header("HTTP/1.0 500");
        die();
    }