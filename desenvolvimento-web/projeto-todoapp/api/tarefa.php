<?php
    include("conexao.php");
    include("repositorio-tarefas.php");
    include("repositorio-usuarios.php");

    try {

        $conexao = conectar();

        /*
            Este trecho é necessário caso o navegador faça um OPTIONS
            para verificar quais são os métodos permitidos.
        */
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            header("Access-Control-Allow-Methods: GET,POST,OPTIONS,DELETE,PUT");
            die();
        
        } else if ($_SERVER["REQUEST_METHOD"] === "POST") {

            $body = file_get_contents("php://input");
            $json = json_decode($body,true);
            
            if (
                !array_key_exists("codigoUsuario",$json) ||
                !array_key_exists("codigoTipo",$json) ||
                !array_key_exists("texto",$json)
            ) {
                header("HTTP/1.1 400");
                die();
            }

            $codigoUsuario = $json["codigoUsuario"];
            $experiencia = 1;

            $tarefa = inserirTarefa($conexao,$json);
            $usuario = alterarExperiencia($conexao,$codigoUsuario,$experiencia);

            $retorno = array();
            $retorno["usuario"] = $usuario;
            $retorno["tarefa"] = $tarefa;

            header('Content-Type: application/json');
            echo json_encode($retorno,JSON_UNESCAPED_UNICODE);        
        
        } else if ($_SERVER["REQUEST_METHOD"] === "PUT") {

            $body = file_get_contents("php://input");
            $json = json_decode($body,true);

            if (!array_key_exists("codigo",$json)) {
                header("HTTP/1.1 400");
                die();
            }

            $codigo = $json["codigo"];
            $experiencia = 1;

            $resultado = concluirTarefa($conexao,$codigo);            

            if ($resultado) {

                $tarefa = obterTarefa($conexao,$codigo);
                $codigoUsuario = $tarefa["codigoUsuario"];
                $usuario = alterarExperiencia($conexao,$codigoUsuario,$experiencia);
                
                header('Content-Type: application/json');
                echo json_encode($usuario,JSON_UNESCAPED_UNICODE);
            
            } else {
                header("HTTP/1.1 400");
                die();
            }

        } else if ($_SERVER["REQUEST_METHOD"] === "DELETE") {

            $body = file_get_contents("php://input");
            $json = json_decode($body,true);

            if (!array_key_exists("codigo",$json)) {
                header("HTTP/1.1 400");
                die();
            }     
            $codigo = $json["codigo"];
            $experiencia = -2;

            $tarefa = obterTarefa($conexao,$codigo);            

            if ($tarefa == null) {
                header("HTTP/1.1 400");
                die();
            } else {
                $resultado = excluirTarefa($conexao,$codigo);

                if ($resultado) {

                    $codigoUsuario = $tarefa["codigoUsuario"];
                    $usuario = alterarExperiencia($conexao,$codigoUsuario,$experiencia);
                    
                    header('Content-Type: application/json');
                    echo json_encode($usuario,JSON_UNESCAPED_UNICODE);

                } else {
                    header("HTTP/1.1 400");
                    die();
                }
            }

        } else {
            header("HTTP/1.1 405");
            die();
        }

        desconectar($conexao);

    } catch (Exception $e) {
        header("HTTP/1.1 500");
    }