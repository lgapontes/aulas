function consumirAPI(metodo,url,sucesso,falha,json) {
    var xhr = new XMLHttpRequest();
    xhr.open(metodo,url);
    xhr.timeout = 10000;

    xhr.addEventListener('load',function(){        
        if (xhr.status == 200) {
            var json = JSON.parse(xhr.responseText);
            sucesso(json);
        } else {
            falha(xhr.status + ': ' + xhr.statusText);
        }
    });
    xhr.addEventListener('timeout',function(){
        falha('Não foi possível obter o conteúdo!');
    });
    xhr.addEventListener('error',function(evento){      
        falha('Ocorreu um erro ao obter o conteúdo!');
    });

    if (json != undefined) {
        xhr.send(json);
    } else {
        xhr.send();
    }    
}

function listarTipos(sucesso,falha) {
    consumirAPI(
        'GET',
        'http://lgapontes.com/aulas/todoapp/api/tipos',
        sucesso,
        falha
    );
}

function login(nome,sucesso,falha) {
    consumirAPI(
        'POST',
        'http://lgapontes.com/aulas/todoapp/api/login',
        sucesso,
        falha,
        JSON.stringify({
            nome: nome
        })
    );
}

function incluirTarefa(tarefa,sucesso,falha) {
    consumirAPI(
        'POST',
        'http://lgapontes.com/aulas/todoapp/api/tarefa',
        sucesso,
        falha,
        JSON.stringify(tarefa)
    );
}

function concluirTarefa(codigo,sucesso,falha) {
    consumirAPI(
        'PUT',
        'http://lgapontes.com/aulas/todoapp/api/tarefa/concluir',
        sucesso,
        falha,
        JSON.stringify({
            codigo: codigo
        })
    );
}

function apagarTarefa(codigo,sucesso,falha) {
    consumirAPI(
        'DELETE',
        'http://lgapontes.com/aulas/todoapp/api/tarefa/apagar',
        sucesso,
        falha,
        JSON.stringify({
            codigo: codigo
        })
    );
}