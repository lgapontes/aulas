$('#botao-logar').on('click',function(){
    var input = $('#input-nome');
    var nome = input.val();

    if (nome == "") {
        // Falha
        $('#falha')
            .text('Nenhum nome foi digitado!')
            .stop().show().fadeOut(3000);
    } else {
        // Sucesso
        input.attr('disabled',true);
        $(this).attr('disabled',true);
        var texto = 'Seja bem-vindo ' + nome + '!';            
        $('#sucesso').text(texto).show().fadeOut(3000);

        logar(nome);
    }
});

function logar(nome) {
    $.post(
        'http://lgapontes.com/aulas/todoapp/api/login',
        JSON.stringify({nome: nome}),
    function(usuario){
        renderizarXP(usuario.experiencia);
        
        usuario.tarefas.forEach(function(tarefa){
            renderizarTarefa(tarefa);
        });

        renderizarSistema(usuario);
    });
}

function renderizarXP(experiencia) {
    var tagXP = $('#experiencia');
    tagXP.removeClass('is-success')
         .removeClass('is-warning')
         .removeClass('is-danger');

    if (experiencia < 0) {
        tagXP.addClass('is-danger').text(experiencia);
    } else if (experiencia < 5) {
        tagXP.addClass('is-warning').text(experiencia);
    } else {
        tagXP.addClass('is-success').text(experiencia);
    }
}

function renderizarSistema(usuario) {
    var divInputNome = $('#div-input-nome');
    divInputNome.toggleClass('is-loading');
    
    $('#codigo-usuario').val(usuario.codigo);

    $.get('http://lgapontes.com/aulas/todoapp/api/tipos',function(tipos){        
        tipos.forEach(function(tipo,index){
            renderizarRadio(tipo,index == 0);
        });

        divInputNome.toggleClass('is-loading');
        $('#div-sistema').show();
    });
}

function renderizarRadio(tipo,checked) {
    var tagLabel = $('<label>').addClass('radio');
    var tagInput = $('<input>')
        .attr('type','radio')
        .attr('name','tipo')
        .attr('checked',checked)
        .val(tipo.codigo);        
    var tagDescricao = $('<span>').text(tipo.descricao);

    tagLabel.append(tagInput);
    tagLabel.append(tagDescricao);

    $('#div-tipos').append(tagLabel);
}

function renderizarTarefa(tarefa) {
    var tagDivColumn = $('<div>').addClass('column');
    var tagDivCard = $('<div>').addClass('card');
    var tagDivCardContent = $('<div>').addClass('card-content');
    var tagDivContent = $('<div>').addClass('content');

    var inputCodigo = $('<input>').attr('type','hidden').val(tarefa.codigo);
    var spanTexto = $('<span>').text(tarefa.texto);
    var spanTipo = $('<span>').addClass('tag is-light').text(tarefa.tipo);
    var spanData = $('<span>').addClass('is-size-7 has-text-grey-light').text(tarefa.dataCriacao);    
    tagDivContent.append(inputCodigo);
    tagDivContent.append(spanTexto);
    tagDivContent.append($('<br>'));
    tagDivContent.append(spanTipo);
    tagDivContent.append(spanData);
    tagDivCardContent.append(tagDivContent);
    tagDivCard.append(tagDivCardContent);

    var tagFooter = $('<footer>').addClass('card-footer');
    var linkApagar = $('<a>')
        .addClass('card-footer-item has-background-light has-text-grey-dark')
        .attr('href',"#")
        .text('Apagar');        
    var linkFeita = $('<a>')
        .addClass('card-footer-item has-background-success has-text-white')
        .attr('href',"#")
        .append(
            $('<strong>').text('Feita!')
        );
    tagFooter.append(linkApagar);
    tagFooter.append(linkFeita);
    tagDivCard.append(tagFooter);

    tagDivColumn.append(tagDivCard);
    tagDivColumn.click(function(evento){
        evento.preventDefault();

        var divTarefa = $(this);
        var codigoTarefa = divTarefa.find('input[type=hidden]').val();

        var imagem = $('<img>').addClass('link-card-loading').attr('src','img/loading.gif');

        if (evento.target == linkApagar.get(0)) {            
            linkApagar.addClass('desativado');
            linkApagar.html(imagem);
            
            $.ajax({
                method: 'DELETE',
                url: 'http://lgapontes.com/aulas/todoapp/api/tarefa/apagar',
                data: JSON.stringify({codigo: codigoTarefa})
            }).done(function(usuario){
                renderizarXP(usuario.experiencia);
                divTarefa.remove();
            }).fail(function(){
                $('#falha')
                    .text('Ocorreu um erro ao apagar a tarefa!')
                    .stop().show().fadeOut(3000);
            });
        }
        if (evento.target == linkFeita.get(0)) {
            linkFeita.addClass('desativado');
            linkFeita.html(imagem);
            
            $.ajax({
                method: 'PUT',
                url: 'http://lgapontes.com/aulas/todoapp/api/tarefa/concluir',
                data: JSON.stringify({codigo: codigoTarefa})
            }).done(function(usuario){
                renderizarXP(usuario.experiencia);
                divTarefa.remove();
            }).fail(function(){
                $('#falha')
                    .text('Ocorreu um erro ao realizar a tarefa!')
                    .stop().show().fadeOut(3000);
            });
        }
    });

    $('#tarefas').append(tagDivColumn);
}

$('form').on('submit',function(evento){
    evento.preventDefault();

    var tarefa = {
        codigoUsuario: $('#codigo-usuario').val(),
        codigoTipo: $(this).find('input[type=radio]:checked').val(),
        texto: $(this).find('textarea').val()
    };

    $.post(
        'http://lgapontes.com/aulas/todoapp/api/tarefa',
        JSON.stringify(tarefa),
        function(dados) {
            renderizarXP(dados.usuario.experiencia);            
            renderizarTarefa(dados.tarefa);
        }
    ).fail(function(){
        $('#falha')
            .text('Ocorreu um erro ao inserir a tarefa!')
            .stop().show().fadeOut(3000);
    });
});