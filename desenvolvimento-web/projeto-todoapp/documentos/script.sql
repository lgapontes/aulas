drop table if exists tarefas;
drop table if exists usuarios;
drop table if exists tipos;

create table tipos (
    codigo bigint auto_increment,
    descricao varchar(10) not null unique,
    primary key (codigo)
);

create table usuarios (
    codigo bigint auto_increment,
    nome varchar(40) not null unique,
    experiencia int default 0,
    primary key (codigo)
);

create table tarefas (
    codigo bigint auto_increment,
    codigoUsuario bigint not null,
    codigoTipo bigint not null,
    dataCriacao timestamp default current_timestamp,
    texto varchar(255) not null,
    concluida boolean default false,
    primary key (codigo),
    foreign key (codigoUsuario) references usuarios(codigo),
    foreign key (codigoTipo) references tipos(codigo)
);