<?php

class Perfil {
    private $codigo;
    private $descricao;

    public function __construct($codigo,$descricao) {
        $this->codigo = $codigo;
        $this->descricao = $descricao;
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function getDescricao() {
        return $this->descricao;
    }

    public function toString() {
        $str = "<strong>Código Perfil:</strong> {$this->codigo}<br />" .
               "<strong>Descrição Perfil:</strong> {$this->descricao}<br />";
        return $str;
    }
}

class Usuario {

    private $codigo;
    private $email;
    private $nome;
    private $perfil;

    public function __construct($codigo,$email,$nome,$perfil) {
        $this->codigo = $codigo;
        $this->email = $email;
        $this->nome = $nome;
        $this->perfil = $perfil;
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function getEmail() {
        return $this->email;
    }
    public function getNome() {
        return $this->nome;
    }
    public function getPerfil() {
        return $this->perfil;
    }

    public function toString() {
        $str = "<strong>Código:</strong> {$this->codigo}<br />" .
               "<strong>Email:</strong> {$this->email}<br />" .
               "<strong>Nome:</strong> {$this->nome}<br />" .
               $this->perfil->toString();
        return $str;
    }
}
