<?php
    session_start();
    require_once "modelo.php";

    $usuarioLogado = isset($_SESSION['usuario']);
    if ($usuarioLogado) {
        $usuario = unserialize($_SESSION['usuario']);
    }

    if ( isset($_SESSION['mensagem-erro']) ) {
        $mensagemErro = $_SESSION['mensagem-erro'];
        unset($_SESSION['mensagem-erro']);
    }

    if ( isset($_SESSION['mensagem-sucesso']) ) {
        $mensagemSucesso = $_SESSION['mensagem-sucesso'];
        unset($_SESSION['mensagem-sucesso']);
    }

?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/favicon.png">
        <link rel="stylesheet" type="text/css" href="css/style.css" />
    </head>
    <body>
        <?php if ( isset($mensagemErro) ): ?>
            <span class="error">
                <?= $mensagemErro ?>
            </span>
        <?php endif; ?>

        <?php if ( isset($mensagemSucesso) ): ?>
            <span class="success">
                <?= $mensagemSucesso ?>
            </span>
        <?php endif; ?>

        <?php if (!$usuarioLogado): ?>
        <form action="efetuar-login.php" method="POST">
            <fieldset>
                <div class="item">
                    <label for="email">E-Mail</label>
                    <input type="text" id="email" name="email" tabindex="1" required value="email@site.com" />
                </div>
                <div class="item">
                    <label for="email">Senha</label>
                    <input type="password" id="senha" name="senha" tabindex="2" required value="123eja" />
                </div>
                <button>Login</button>
            </fieldset>
        </form>
        <?php else: ?>
            <form action="efetuar-logout.php" method="POST">
                <fieldset>
                    <button class="logout">Realizar Logout</button>
                </fieldset>
            </form>
            <pre><?= $usuario->toString() ?></pre>
        <?php endif; ?>

    </body>
</html>
