<?php

session_start();

require_once "modelo.php";

function emailExiste() {
    return isset($_POST['email']);
}
function senhaExiste() {
    return isset($_POST['senha']);
}

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
try {

    if ( emailExiste() && senhaExiste() ) {

        $conexao = new mysqli('127.0.0.1','roupas_user','123eja','roupas_database');

        $email = $_POST['email'];
        $senha = $_POST['senha'];

        $query = $conexao->prepare(
            'select u.codigo, u.email, u.nome, u.codigoPerfil, p.descricao  from usuarios u ' .
            'inner join perfis p on p.codigo = u.codigoPerfil ' .
            'where email = ? and senha = ?'
        );
        $query->bind_param("ss",$email,hash('sha256', $senha));
        $query->execute();
        $resultado = $query->get_result();

        if ($resultado->num_rows == 1) {
            $linha = $resultado->fetch_assoc();

            $perfil = new Perfil(
                $linha['codigoPerfil'],
                $linha['descricao']
            );

            $usuario = new Usuario(
                $linha['codigo'],
                $linha['email'],
                $linha['nome'],
                $perfil
            );

            $_SESSION['usuario'] = serialize($usuario);

            $_SESSION['mensagem-sucesso'] = 'Seja bem-vindo ' . $usuario->getNome() . '!';
            header('Location: index.php');
            die();

        } else {
            $_SESSION['mensagem-erro'] = 'Usuário não encontrado!';
            header('Location: index.php');
            die();
        }
    } else {
        $_SESSION['mensagem-erro'] = 'Usuário não encontrado!';
        header('Location: index.php');
        die();
    }

} catch(Exception $erro) {
    echo "<pre>";
    var_dump($erro);
    echo "</pre>";
    die();
}
