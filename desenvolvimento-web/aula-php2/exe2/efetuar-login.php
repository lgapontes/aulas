<?php

function emailExiste() {
    return isset($_POST['email']);
}
function senhaExiste() {
    return isset($_POST['senha']);
}

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
try {

    if ( emailExiste() && senhaExiste() ) {

        $conexao = new mysqli('127.0.0.1','roupas_user','123eja','roupas_database');

        $email = $_POST['email'];
        $senha = $_POST['senha'];

        $query = $conexao->prepare('select nome from usuarios where email = ? and senha = ?');
        $query->bind_param("ss",$email,hash('sha256', $senha));
        $query->execute();
        $resultado = $query->get_result();

        if ($resultado->num_rows == 1) {
            $linha = $resultado->fetch_assoc();

            // Define um Cookie. Neste caso, ele vai durar até o usuário fechar o browser
            setcookie('usuario',$linha['nome']);

            header('Location: index.php?success=true');
            die();

        } else {
            header('Location: index.php?error=true');
            die();
        }
    } else {
        header('Location: index.php?error=true');
        die();
    }

} catch(Exception $erro) {
    echo "<pre>";
    var_dump($erro);
    echo "</pre>";
    die();
}
