<?php
    require_once "conexao.php";
    require_once "repositorio.php";
    $repositorio = new Repositorio($conexao);
?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>PHP</title>
        <link rel="icon" href="favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="estilo.css" />
    </head>
    <body>
        <header>
            Cadastro de Perfis
        </header>
        <main>
            <?php
                if (isset($_GET['mensagem'])) {
                    $mensagem = $_GET['mensagem'];
                    switch ($mensagem) {
                        case 0:
                            echo '<span class="mensagem">Não foi possível incluir!</span>';
                            break;
                        case 1:
                            echo '<span class="mensagem">Registro incluído com sucesso!</span>';
                            break;
                        case 2:
                            echo '<span class="mensagem">Nenhum registro foi excluído!</span>';
                            break;
                        case 3:
                            echo '<span class="mensagem">Registro excluído com sucesso!</span>';
                            break;
                    }
                }
            ?>
            <table>
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Descrição</th>
                        <th>Opções</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $perfis = $repositorio->listar();
                        foreach ($perfis as $perfil):
                    ?>
                        <tr>
                            <td><?= $perfil->getCodigo() ?></td>
                            <td><?= $perfil->getDescricao() ?></td>
                            <td>
                                <form method="POST" action="excluir-perfil.php">
                                    <input type="hidden" name="codigo"
                                        value="<?= $perfil->getCodigo() ?>">
                                    <button class="link">Excluir</button>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <br />
            <form action="incluir-perfil.php" method="POST">
                <fieldset>
                    <legend>Novo Perfil</legend>
                    <label for="descricao">Descrição</label>
                    <input name="descricao" id="descricao" type="text" placeholder="Descrição do perfil"/>
                    <button class="form">Salvar</button>
                </fieldset>
            </form>
        </main>
    </body>
</html>
