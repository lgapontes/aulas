<?php

require_once "perfil.php";

class Repositorio {

    private $conexao;

    public function __construct($conexao) {
        $this->conexao = $conexao;
    }

    private function exibirErro($erro) {
        echo "<pre>";
        var_dump($erro);
        echo "</pre>";
        die();
    }

    public function listar() {
        try {
            $perfis = array();

            $resultado = $this->conexao->query('select * from perfis');

            if ($resultado->num_rows > 0) {
                while($linha = $resultado->fetch_assoc()) {
                    $perfil = new Perfil($linha['codigo'],$linha['descricao']);
                    array_push($perfis,$perfil);
                }
            }

            return $perfis;
        } catch (Exception $erro) {
            $this->exibirErro($erro);
        }
    }

    public function excluir($codigo) {
        try {
            $this->conexao->query('delete from perfis where codigo = ' . $codigo);

            /*
                Código para proteger de SQL Injection

                $stmt = $this->conexao->prepare("delete from perfis where codigo = ?");
                $stmt->bind_param("i", $codigo);
                $stmt->execute();
            */

            return ($this->conexao->affected_rows > 0);
        } catch (Exception $erro) {
            $this->exibirErro($erro);
        }
    }

    public function incluir($descricao) {
        try {
            $resultado = $this->conexao->query("insert into perfis (descricao) values ('" . $descricao . "')");
            return $resultado;
        } catch (Exception $erro) {
            $this->exibirErro($erro);
        }
    }

}
