<?php

require_once "conexao.php";
require_once "repositorio.php";

if (isset($_POST['descricao'])) {

    $repositorio = new Repositorio($conexao);
    $descricao = $_POST['descricao'];
    $resultadoInclusao = $repositorio->incluir($descricao);

    if ($resultadoInclusao) {
        header("Location: index.php?mensagem=1");
        die();
    } else {
        header("Location: index.php?mensagem=0");
        die();
    }

} else {
    header("Location: index.php?mensagem=0");
    die();
}
