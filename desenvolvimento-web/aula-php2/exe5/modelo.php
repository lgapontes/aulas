<?php

class Usuario {

    private $codigo;
    private $email;
    private $nome;

    public function __construct($codigo,$email,$nome) {
        $this->codigo = $codigo;
        $this->email = $email;
        $this->nome = $nome;
    }

    public function getCodigo() {
        return $this->codigo;
    }
    public function getEmail() {
        return $this->email;
    }
    public function getNome() {
        return $this->nome;
    }

    public function toString() {
        $str = "<strong>Código:</strong> {$this->codigo}<br />" .
               "<strong>Email:</strong> {$this->email}<br />" .
               "<strong>Nome:</strong> {$this->nome}<br />";
        return $str;
    }
}
