package com.lgapontes.calculadora;

import android.content.Context;
import android.content.SharedPreferences;

public class Repositorio {

    private SharedPreferences preferences;

    public Repositorio(Context context) {
        this.preferences = context.getSharedPreferences("calculadora", Context.MODE_PRIVATE);
    }

    public void salvar(String valor) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString("visor",valor);
        editor.commit();
    }

    public String obter() {
        return this.preferences.getString("visor",null);
    }

}
