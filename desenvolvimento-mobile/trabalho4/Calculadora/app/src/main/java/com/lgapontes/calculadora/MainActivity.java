package com.lgapontes.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private boolean resultadoNaTela;
    private Repositorio repositorio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.editText = (EditText) findViewById(R.id.visor);

        this.repositorio = new Repositorio(this);
        String valorInicial = this.repositorio.obter();
        if (valorInicial != null) {
            if (valorInicial.indexOf("R") == 0) {
                this.resultadoNaTela = true;
            }
            this.editText.setText(valorInicial);
        } else {
            this.resultadoNaTela = false;
        }
    }

    private void definirValorVisor(String valor) {
        this.repositorio.salvar(valor);
        this.editText.setText(valor);
    }

    public String obterOperador(String valor) {
        String[] operadores = new String[]{
                "+", "-", "*", "/"
        };
        for (int i=0; i<operadores.length; i++) {
            if (valor.indexOf(operadores[i]) > -1) {
                return operadores[i];
            }
        }
        return null;
    }

    private String obterValorBotao(View view) {
        Button button = (Button) view;
        return button.getText().toString();
    }

    public void digitarOperador(View view) {
        if (!this.resultadoNaTela) {
            String operador = obterValorBotao(view);
            String valorDoVisor = this.editText.getText().toString();

            if (valorDoVisor.length() > 0) {
                this.resultadoNaTela = false;
                boolean naoExisteOperadorNoVisor = (obterOperador(valorDoVisor) == null);

                if (naoExisteOperadorNoVisor) {
                    definirValorVisor( valorDoVisor + operador );
                }
            }
        }
    }

    private String calcular(int a, int b, String operador) {
        if (operador.equals("+")) {
            return String.valueOf(a + b);
        } else if (operador.equals("-")) {
            return String.valueOf(a - b);
        } else if (operador.equals("*")) {
            return String.valueOf(a * b);
        } else if (operador.equals("/")) {
            return String.valueOf((new Float(a)) / (new Float(b)));
        } else {
            return "";
        }
    }

    public void calcularResultado(View view) {
        if (!this.resultadoNaTela) {
            String valorDoVisor = this.editText.getText().toString();
            String operador = obterOperador(valorDoVisor);

            if (operador == null) {
                Toast.makeText(this, "Erro no cálculo!", Toast.LENGTH_SHORT).show();
            } else {
                int indexOperador = valorDoVisor.indexOf(operador);
                int numeroEsquerda = Integer.parseInt(valorDoVisor.substring(0, indexOperador));
                int numeroDireita = Integer.parseInt(valorDoVisor.substring(indexOperador+1, valorDoVisor.length()));
                String resultado = calcular(numeroEsquerda,numeroDireita,operador);
                definirValorVisor("Resultado: " + resultado);
                this.resultadoNaTela = true;
            }
        }
    }

    public void apagarVisor(View view) {
        this.resultadoNaTela = false;
        definirValorVisor("");
    }

    public void digitarNumero(View view) {
        if (this.resultadoNaTela) {
            definirValorVisor("");
            this.resultadoNaTela = false;
        }

        String digito = obterValorBotao(view);
        boolean digitoNaoEhZero = !digito.equals("0");
        String valorDoVisor = this.editText.getText().toString();

        /* Verifica se há zero à esquerda */
        if (digitoNaoEhZero) {
            definirValorVisor( valorDoVisor + digito );
        } else {
            String operadorDoVisor = obterOperador(valorDoVisor);
            boolean naoExisteOperadorNoVisor = (operadorDoVisor == null);

            if (naoExisteOperadorNoVisor) {
                if (valorDoVisor.length() > 0) {
                    definirValorVisor( valorDoVisor + digito );
                }
            } else {
                int indexOperador = valorDoVisor.indexOf(operadorDoVisor);
                boolean operadorNaoEhUltimoDigito = (indexOperador < (valorDoVisor.length() - 1));

                if (operadorNaoEhUltimoDigito) {
                    definirValorVisor( valorDoVisor + digito );
                }
            }
        }

    }
}
