package com.lgapontes.myhome;

public class Endereco {

    private int codigo;
    private String logradouro;
    private int numero;
    private int cep;
    private String cidade;

    public Endereco() {
        this.codigo = 1;
        this.logradouro = "";
        this.cidade = "";
    }

    public boolean jaFoiCadastrado() {
        return !this.logradouro.equals("");
    }

    @Override
    public String toString() {
        String mapa = this.logradouro;
        if (numero > 0) mapa += ", " + numero;
        if (cep > 0) mapa += ", CEP " + cep;
        if (!cidade.equals("")) mapa += ", " + cidade;
        return mapa;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getCep() {
        return cep;
    }

    public void setCep(int cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
}
