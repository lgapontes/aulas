package com.lgapontes.myhome;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Repositorio extends SQLiteOpenHelper {

    private static String BANCO_DADOS = "MyHome.db";
    private static int VERSAO = 1;

    public Repositorio(Context context) {
        super(context, BANCO_DADOS, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL("CREATE TABLE enderecos (codigo INTEGER PRIMARY KEY NOT NULL, logradouro VARCHAR(100) NOT NULL, numero INTEGER, cep INTEGER, cidade VARCHAR(100))");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {}

    public void salvar(Endereco endereco) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("codigo",endereco.getCodigo());
        content.put("logradouro",endereco.getLogradouro());
        content.put("numero",endereco.getNumero());
        content.put("cep",endereco.getCep());
        content.put("cidade",endereco.getCidade());

        if (obter().jaFoiCadastrado()) {
            db.update("enderecos",content,"codigo=?", new String[]{"1"});
        } else {
            db.insert("enderecos",null,content);
        }
    }

    public Endereco obter() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from enderecos where codigo = 1", null);
        Endereco endereco = new Endereco();
        while(cursor.moveToNext()) {
            endereco.setLogradouro( cursor.getString(cursor.getColumnIndex("logradouro")) );
            endereco.setNumero( cursor.getInt(cursor.getColumnIndex("numero")) );
            endereco.setCep( cursor.getInt(cursor.getColumnIndex("cep")) );
            endereco.setCidade( cursor.getString(cursor.getColumnIndex("cidade")) );
        }
        return endereco;
    }
}
