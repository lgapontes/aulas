package com.lgapontes.myhome;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText logradouro;
    private EditText numero;
    private EditText cep;
    private EditText cidade;
    private Repositorio repositorio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        repositorio = new Repositorio(this);

        logradouro = findViewById(R.id.logradouro);
        numero = findViewById(R.id.numero);
        cep = findViewById(R.id.cep);
        cidade = findViewById(R.id.cidade);

        obter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.layout_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_salvar:
                salvar();
                break;
            case R.id.menu_mapa:
                abrirMapa();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private Endereco obterEnderecoDosCampos() {
        String logradouro = this.logradouro.getText().toString();
        if (logradouro.equals("")) {
            Toast.makeText(this, "O campo logradouro é obrigatório!", Toast.LENGTH_SHORT).show();
            return null;
        } else {
            Endereco endereco = new Endereco();
            endereco.setLogradouro(logradouro);
            endereco.setNumero(Integer.parseInt(numero.getText().toString()));
            endereco.setCep(Integer.parseInt(cep.getText().toString()));
            endereco.setCidade(cidade.getText().toString());
            return endereco;
        }
    }

    private void abrirMapa() {
        Endereco endereco = obterEnderecoDosCampos();
        if (endereco != null) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("geo:0,0?q=" + endereco.toString()));
            startActivity(intent);
        }
    }

    private void salvar() {
        Endereco endereco = obterEnderecoDosCampos();
        if (endereco != null) {
            repositorio.salvar(endereco);
            Toast.makeText(this, "Endereço salvo!", Toast.LENGTH_SHORT).show();
        }
    }

    private void obter() {
        Endereco endereco = repositorio.obter();
        logradouro.setText(endereco.getLogradouro());
        cidade.setText(endereco.getCidade());

        if (endereco.getNumero() > 0) {
            numero.setText(Integer.toString(endereco.getNumero()));
        }
        if (endereco.getCep() > 0) {
            cep.setText(Integer.toString(endereco.getCep()));
        }
    }
}
