
CREATE TABLE enderecos (
    codigo INTEGER PRIMARY KEY NOT NULL,
    logradouro VARCHAR(100) NOT NULL,
    numero INTEGER,
    cep INTEGER,
    cidade VARCHAR(100)
);

select * from enderecos where codigo = 1;
