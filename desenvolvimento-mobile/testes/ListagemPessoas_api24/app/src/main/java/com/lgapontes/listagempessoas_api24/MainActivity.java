package com.lgapontes.listagempessoas_api24;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Pessoa> pessoas = Arrays
                .asList(new String[]{"Seiya", "Shiryu", "Hyoga", "Shun", "Ikki"})
                .stream().map(x -> { return new Pessoa(x); }).collect(Collectors.toList());

        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, pessoas);
        ListView listView = (ListView) findViewById(R.id.lista);
        listView.setAdapter(adapter);
    }
}
