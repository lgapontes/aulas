package com.lgapontes.aula4_listagempessoas_v2;

import java.io.Serializable;

public class Pessoa implements Serializable {

    private Long index;
    private String nome;
    private String sobrenome;
    private Boolean estudante;

    public Pessoa(String nome, String sobrenome, Boolean estudante) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.estudante = (estudante == null) ? false : estudante;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public Boolean getEstudante() {
        return estudante;
    }

    public void setEstudante(Boolean estudante) {
        this.estudante = estudante;
    }

    @Override
    public String toString() {
        return this.nome + " " + sobrenome +
                (this.estudante ? " (estudante)" : "");
    }

}


