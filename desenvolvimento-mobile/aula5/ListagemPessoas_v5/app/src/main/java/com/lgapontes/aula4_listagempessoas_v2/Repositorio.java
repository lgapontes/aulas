package com.lgapontes.aula4_listagempessoas_v2;

import java.util.ArrayList;
import java.util.List;

public class Repositorio {

    private static final Repositorio singleton = new Repositorio();
    public static Repositorio getInstance() {
        return singleton;
    }

    private List<Pessoa> pessoas;

    private Repositorio() {
        this.pessoas = new ArrayList<Pessoa>();
    }

    public List<Pessoa> listar() {
        return this.pessoas;
    }

    public void inserir(Pessoa pessoa) {
        this.pessoas.add(pessoa);
    }
}

