package com.lgapontes.aula4_listagempessoas_v2;

public class Pessoa {

    private String nome;
    private String sobrenome;
    private Boolean estudante;

    public Pessoa(String nome, String sobrenome, Boolean estudante) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.estudante = (estudante == null) ? false : estudante;
    }

    @Override
    public String toString() {
        return this.nome + " " + sobrenome +
                (this.estudante ? " (estudante)" : "");
    }

}


