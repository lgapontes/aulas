package com.lgapontes.aula4_listagempessoas_v2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

public class InserirPessoaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inserir_pessoa);

        Button cancelar = findViewById(R.id.incluir_pessoa_cancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                voltarParaListagem();
            }
        });

        Button salvar = findViewById(R.id.incluir_pessoa_salvar);
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView textViewNome = findViewById(R.id.incluir_pessoa_nome);
                String nome = textViewNome.getText().toString();

                TextView textViewSobrenome = findViewById(R.id.incluir_pessoa_sobrenome);
                String sobrenome = textViewSobrenome.getText().toString();

                Switch switchEstudante = findViewById(R.id.incluir_pessoa_estudante);
                Boolean estudante = switchEstudante.isChecked();

                Pessoa pessoa = new Pessoa(nome,sobrenome,estudante);
                Repositorio.getInstance().inserir(pessoa);

                voltarParaListagem();
            }
        });
    }

    private void voltarParaListagem() {
        finish();
    }

}
