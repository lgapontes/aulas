package com.lgapontes.aula4_listagempessoas_v2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class InserirPessoaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inserir_pessoa);

        Button cancelar = findViewById(R.id.incluir_pessoa_cancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                voltarParaListagem();
            }
        });

        Button salvar = findViewById(R.id.incluir_pessoa_salvar);
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                voltarParaListagem();
            }
        });
    }

    private void voltarParaListagem() {
        Intent voltar = new Intent(this,ListagemActivity.class);
        startActivity(voltar);
    }

}
