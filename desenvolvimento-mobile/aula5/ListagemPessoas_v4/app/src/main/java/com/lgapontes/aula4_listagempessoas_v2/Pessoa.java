package com.lgapontes.aula4_listagempessoas_v2;

public class Pessoa {

    private String nome;
    private String sobrenome;

    public Pessoa(String nome, String sobrenome) {
        this.nome = nome;
        this.sobrenome = sobrenome;
    }

    @Override
    public String toString() {
        return this.nome + " " + sobrenome;
    }

}
