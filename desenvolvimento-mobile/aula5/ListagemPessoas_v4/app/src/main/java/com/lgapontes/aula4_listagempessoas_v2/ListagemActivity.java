package com.lgapontes.aula4_listagempessoas_v2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListagemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem);

        List<Pessoa> pessoas = criarPessoas();
        ListView listView = (ListView) findViewById(R.id.lista_pessoas);

        ArrayAdapter<Pessoa> adapter = new ArrayAdapter<Pessoa>(
                this,
                android.R.layout.simple_list_item_1,
                pessoas
        );
        listView.setAdapter(adapter);
    }

    public void clickBotaoNovo(View view) {
        Intent abrirInserirPessoa = new Intent(this,InserirPessoaActivity.class);
        startActivity(abrirInserirPessoa);
    }

    private List<Pessoa> criarPessoas() {
        Pessoa[] lista = new Pessoa[]{
              new Pessoa("João","da Silva"),
                new Pessoa("Maria","da Silva"),
                new Pessoa("José","da Silva"),
                new Pessoa("Pedro","da Silva"),
                new Pessoa("João","de Oliveira"),
                new Pessoa("Maria","de Oliveira"),
                new Pessoa("José","de Oliveira"),
                new Pessoa("Pedro","de Oliveira"),
                new Pessoa("Tiago","de Mattos"),
                new Pessoa("Manoel","de Mattos"),
                new Pessoa("José","de Mattos"),
                new Pessoa("Joaquim","de Mattos"),
                new Pessoa("Alex","de Mattos"),
                new Pessoa("Fulano","de Mattos"),
                new Pessoa("Ciclano","de Mattos")
        };

        return Arrays.asList(lista);
    }

}
