package com.lgapontes.aula4_listagempessoas_v2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

public class ManterPessoaActivity extends AppCompatActivity {

    private Pessoa pessoa;
    private TextView textViewNome;
    private TextView textViewSobrenome;
    private Switch switchEstudante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manter_pessoa);

        textViewNome = (TextView) findViewById(R.id.incluir_pessoa_nome);
        textViewSobrenome = (TextView) findViewById(R.id.incluir_pessoa_sobrenome);
        switchEstudante = (Switch) findViewById(R.id.incluir_pessoa_estudante);
        
        Intent intent = getIntent();
        pessoa = (Pessoa) intent.getSerializableExtra("pessoa");
        if (pessoa != null) {
            textViewNome.setText(pessoa.getNome());
            textViewSobrenome.setText(pessoa.getSobrenome());
            switchEstudante.setChecked(pessoa.getEstudante());
        }

        // Resto do código ...

        Button cancelar = findViewById(R.id.incluir_pessoa_cancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                voltarParaListagem();
            }
        });

        Button salvar = findViewById(R.id.incluir_pessoa_salvar);
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nome = textViewNome.getText().toString();
                String sobrenome = textViewSobrenome.getText().toString();
                Boolean estudante = switchEstudante.isChecked();

                if (pessoa == null) {
                    pessoa = new Pessoa(nome,sobrenome,estudante);
                    Repositorio.getInstance().inserir(pessoa);
                } else {
                    pessoa.setNome(nome);
                    pessoa.setSobrenome(sobrenome);
                    pessoa.setEstudante(estudante);
                    Repositorio.getInstance().alterar(pessoa);
                }

                voltarParaListagem();
            }
        });
    }

    private void voltarParaListagem() {
        finish();
    }

}
