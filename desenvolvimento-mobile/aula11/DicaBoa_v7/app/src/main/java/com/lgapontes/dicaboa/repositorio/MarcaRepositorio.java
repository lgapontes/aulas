package com.lgapontes.dicaboa.repositorio;

import com.lgapontes.dicaboa.modelo.Marca;

import java.util.ArrayList;
import java.util.List;

public class MarcaRepositorio implements IRepositorio<Marca> {

    private Repositorio repositorio;

    public MarcaRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public List<Marca> listar() {
        return this.repositorio.listarMarcas();
    }
}
