package com.lgapontes.dicaboa.repositorio;

import com.lgapontes.dicaboa.modelo.Processador;
import java.util.ArrayList;
import java.util.List;

public class ProcessadorRepositorio implements IRepositorio<Processador> {

    private Repositorio repositorio;

    public ProcessadorRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public List<Processador> listar() {
        return this.repositorio.listarProcessadores();
    }
}
