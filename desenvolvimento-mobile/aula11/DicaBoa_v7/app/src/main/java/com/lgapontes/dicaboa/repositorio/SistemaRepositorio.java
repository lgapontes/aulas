package com.lgapontes.dicaboa.repositorio;

import com.lgapontes.dicaboa.modelo.Sistema;

import java.util.ArrayList;
import java.util.List;

public class SistemaRepositorio implements IRepositorio<Sistema> {

    private Repositorio repositorio;

    public SistemaRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public List<Sistema> listar() {
        return this.repositorio.listarSistemas();
    }
}
