package com.lgapontes.dicaboa.modelo;

public interface IDetalhe {

    public void setCodigo(long codigo);
    public void setNome(String nome);

}
