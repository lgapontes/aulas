package com.lgapontes.dicaboa.repositorio;

import android.util.Log;

import com.lgapontes.dicaboa.modelo.IDetalhe;
import com.lgapontes.dicaboa.modelo.Marca;

import java.util.ArrayList;
import java.util.List;

public class MarcaRepositorio implements IRepositorio<IDetalhe> {

    private Repositorio repositorio;

    public MarcaRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public List<IDetalhe> listar() {
        List<IDetalhe> lista = this.repositorio.listarDetalhes(Marca.class, "marcas");

        for (IDetalhe d : lista) {
            Marca m = (Marca) d;
            Log.i("TESTE", d.toString());
        }

        return lista;
    }
}
