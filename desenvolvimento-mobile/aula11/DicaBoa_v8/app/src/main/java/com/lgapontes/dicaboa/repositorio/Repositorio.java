package com.lgapontes.dicaboa.repositorio;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.lgapontes.dicaboa.modelo.IDetalhe;
import com.lgapontes.dicaboa.modelo.Marca;
import com.lgapontes.dicaboa.modelo.Processador;
import com.lgapontes.dicaboa.modelo.Sistema;

import java.util.ArrayList;
import java.util.List;

public class Repositorio extends SQLiteOpenHelper {

    private static String BANCO_DADOS = "DicaBoa.db";
    private static int VERSAO = 1;

    public Repositorio(Context context) {
        super(context, BANCO_DADOS, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL("create table marcas (codigo BIGINT PRIMARY KEY, nome VARCHAR(40) NOT NULL)");
            db.execSQL("create table sistemas (codigo BIGINT PRIMARY KEY,nome VARCHAR(40) NOT NULL)");
            db.execSQL("create table processadores (codigo BIGINT PRIMARY KEY,nome VARCHAR(40) NOT NULL)");
            db.execSQL("create table computadores (uuid CHAR(36) PRIMARY KEY,codigoMarca BIGINT NOT NULL, codigoSistema BIGINT NOT NULL,codigoProcessador BIGINT NOT NULL,usado BOOLEAN NOT NULL, tipo VARCHAR(40) NOT NULL,memoriaRam INT NOT NULL,hd INT NOT NULL,ssd INT NOT NULL,preco VARCHAR(40) NOT NULL, nota DOUBLE NOT NULL, FOREIGN KEY (codigoMarca) REFERENCES marcas(codigo), FOREIGN KEY (codigoSistema) REFERENCES sistemas(codigo), FOREIGN KEY (codigoProcessador) REFERENCES processadores(codigo))");
            popularTabela(db, "marcas", new String[] {
                    "Acer", "Dell", "Lenovo", "Samsung", "HP",
                    "Apple", "LG", "AOC", "Asus", "Avell"
            });
            popularTabela(db, "sistemas", new String[] {
                    "Windows 8", "Windows 10", "Ubuntu 18",
                    "Debian", "macOS X"
            });
            popularTabela(db, "processadores", new String[] {
                    "Intel i3 2GHz", "Intel i5 2,5GHz", "Intel i5 3GHz",
                    "Intel i7 3GHz", "Intel i9 3,5GHz"
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void popularTabela(SQLiteDatabase db, String tabela, String[] itens) {
        for (int i=0;i<itens.length;i++) {
            ContentValues content = new ContentValues();
            int codigo = i+1;
            content.put("codigo",codigo);
            content.put("nome",itens[i]);
            db.insert(tabela,null,content);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    public List<IDetalhe> listarDetalhes(Class<? extends IDetalhe> clazz, String tabela) {
        List<IDetalhe> lista = new ArrayList<IDetalhe>();
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + tabela, null);
            while(cursor.moveToNext()) {
                IDetalhe novo = clazz.newInstance();
                novo.setCodigo(cursor.getLong(cursor.getColumnIndex("codigo")));
                novo.setNome(cursor.getString(cursor.getColumnIndex("nome")));
                lista.add(novo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

}
