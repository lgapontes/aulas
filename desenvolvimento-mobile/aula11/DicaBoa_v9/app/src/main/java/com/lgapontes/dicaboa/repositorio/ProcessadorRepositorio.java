package com.lgapontes.dicaboa.repositorio;

import com.lgapontes.dicaboa.modelo.IDetalhe;
import com.lgapontes.dicaboa.modelo.Processador;
import java.util.ArrayList;
import java.util.List;

public class ProcessadorRepositorio implements IRepositorio<IDetalhe> {

    private Repositorio repositorio;

    public ProcessadorRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public List<IDetalhe> listar() {
        return this.repositorio.listarDetalhes(Processador.class, "processadores");
    }
}
