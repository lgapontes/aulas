package com.lgapontes.dicaboa.modelo;

public class Processador implements IDetalhe {

    private long codigo;
    private String nome;

    public Processador() {}

    public Processador(long codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    public long getCodigo() { return codigo; }

    @Override
    public void setCodigo(long codigo) { this.codigo = codigo; }

    public String getNome() { return nome; }

    @Override
    public void setNome(String nome) { this.nome = nome; }

    @Override
    public String toString() {
        return nome;
    }
}
