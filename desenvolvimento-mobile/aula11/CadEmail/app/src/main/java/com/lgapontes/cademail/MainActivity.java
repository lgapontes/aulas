package com.lgapontes.cademail;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private void salvar() {
        TextView textView = (TextView) findViewById(R.id.edit_email);
        String email = textView.getText().toString();
        Repositorio db = new Repositorio(this);
        db.cadastrarContato(new Contato(email));
        db.close();
        Toast.makeText(this,"Contato cadastrado!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.layout_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_salvar:
                salvar();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
