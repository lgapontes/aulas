package com.lgapontes.cademail;

import java.util.UUID;

public class Contato {

    private String uuid;
    private String email;

    public Contato(String email) {
        this.uuid = UUID.randomUUID().toString();
        this.email = email;
    }
    public Contato(String uuid, String email) {
        this.uuid = uuid;
        this.email = email;
    }

    public String getUUID() { return this.uuid; }

    public String getEmail() { return this.email; }

    @Override
    public String toString() { return this.email; }
}