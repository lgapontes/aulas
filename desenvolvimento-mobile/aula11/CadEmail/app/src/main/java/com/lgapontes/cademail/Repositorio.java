package com.lgapontes.cademail;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.UUID;

public class Repositorio extends SQLiteOpenHelper {

    public Repositorio(Context context) {
        super(context, "CadEmail.db", null, 1);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql ="CREATE TABLE contatos (uuid CHAR(36) PRIMARY KEY,email VARCHAR(100) not null);";
        db.execSQL(sql);
        String cadastroInicial =
                "insert into contatos values ('" + UUID.randomUUID().toString() + "','joao@site.com');" +
                "insert into contatos values ('" + UUID.randomUUID().toString() + "','maria@site.com');" +
                "insert into contatos values ('" + UUID.randomUUID().toString() + "','laura@site.com');";
        executarBatch(db, cadastroInicial);
    }

    private void executarBatch(SQLiteDatabase db, String sql) {
        for (String comando : sql.split(";")) {
            db.execSQL(comando);
        }
    }

    public void cadastrarContato(Contato contato) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "insert into contatos (uuid,email) values ('" +
                contato.getUUID() + "','" + contato.getEmail() +
                "');";
        executarBatch(db, sql);
    }
}