$(listarPersonagens);

function listarPersonagens() {
    $('img.loading').show();
    $.get('http://lgapontes.com/aulas/rpgistas/api/personagens.php',function(json){
        let listarPersonagens = $('#lista-personagens');
        listarPersonagens.empty();
        json.forEach(personagem => {
            let li = $('<li>').html(
                `<div class="imagem">
                    <img src="${personagem.classe.imagem}" alt="${personagem.classe.nome}"  />
                </div>
                <section>
                    <span class="nome">${personagem.classe.nome}: ${personagem.nome}</span>
                    <span class="descricao">${personagem.classe.descricao}</span>
                    <span class="atributos">FOR: ${personagem.classe.forca}</span>
                    <span class="atributos">DES: ${personagem.classe.destreza}</span>
                    <span class="atributos">INT: ${personagem.classe.inteligencia}</span>
                </section>`
            );
            listarPersonagens.append(li);
        });
        $('img.loading').hide();
    });
}

setInterval(listarPersonagens,5000);
