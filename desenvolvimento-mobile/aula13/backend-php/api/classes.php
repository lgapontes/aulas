<?php
    require_once __DIR__."/../libs/util.php";
    require_once __DIR__."/../libs/modelo.php";
    require_once __DIR__."/../libs/repositorio.php";

    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        if ( isset($_GET["codigo"]) ) {
            // Obtem uma classe
            try {
                $codigo = $_GET["codigo"];
                $classe = Repositorio::getInstance()->obterClasse($codigo);
                status200($classe);
            } catch (Exception $e) { status404($e->getMessage()); }
        } else {
            // Obtem todas as classes
            try {
                $classes = Repositorio::getInstance()->listarClasses();
                status200($classes);
            } catch (Exception $e) { status404($e->getMessage()); }
        }
    } else { status405(); }
