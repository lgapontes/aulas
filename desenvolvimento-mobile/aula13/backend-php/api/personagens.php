<?php
    require_once __DIR__."/../libs/util.php";
    require_once __DIR__."/../libs/modelo.php";
    require_once __DIR__."/../libs/repositorio.php";

    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        try {
            $personagens = Repositorio::getInstance()->listarPersonagens();
            status200($personagens);
        } catch (Exception $e) { status404($e->getMessage()); }
    } else if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $body = file_get_contents("php://input");
        $dados = json_decode($body,true);
        if (
            !isset( $dados["codigoClasse"] ) ||
            !isset( $dados["nome"] )
        ) { status400("Dados inválidos!"); }

        try {
            $classe = Repositorio::getInstance()->obterClasse($dados["codigoClasse"]);
            $personagem = new Personagem(null, $classe, $dados["nome"] );
            $personagemInserido = Repositorio::getInstance()->inserirPersonagem($personagem);
            status200($personagemInserido);
        } catch (Exception $e) { status400($e->getMessage()); }
    } else { status405(); }
