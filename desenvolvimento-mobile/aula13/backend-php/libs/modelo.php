<?php

    class Classe implements JsonSerializable {
        private $codigo;
        private $nome;
        private $descricao;
        private $imagem;
        private $forca;
        private $destreza;
        private $inteligencia;

        public function __construct($codigo,$nome,$descricao,$imagem,$forca,$destreza,$inteligencia) {
            $this->codigo = $codigo;
            $this->nome = $nome;
            $this->descricao = $descricao;
            $this->imagem = $imagem;
            $this->forca = $forca;
            $this->destreza = $destreza;
            $this->inteligencia = $inteligencia;
        }

        public function getCodigo() { return $this->codigo; }

        public function jsonSerialize() {
            $vars = get_object_vars($this);
            return $vars;
        }
    }

    class Personagem implements JsonSerializable {
        private $codigo;
        private $classe;
        private $nome;

        public function __construct($codigo,$classe,$nome) {
            $this->codigo = $codigo;
            $this->classe = $classe;
            $this->nome = $nome;
        }

        public function getClasse() {
            return $this->classe;
        }

        public function getNome() {
            return $this->nome;
        }

        public function jsonSerialize() {
            $vars = get_object_vars($this);
            return $vars;
        }
    }
