<?php
error_reporting(E_ERROR | E_PARSE);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

/* Singleton */
class Repositorio {
    private static $instance;
    private $conexao;
    private function __construct() {}

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new Repositorio();
            self::$instance->conexao = new mysqli('linu.com.br','linuc318_rpg','123eja','linuc318_rpgistas_db');
            mysqli_set_charset(self::$instance->conexao, "utf8");
        }
        return self::$instance;
    }

    public function listarClasses() {
        $lista = array();
        $resultado = $this->conexao->query('select * from classes order by codigo');

        if ($resultado->num_rows > 0) {
            while ($linha = $resultado->fetch_assoc()) {
                $classe = new Classe(
                    $linha['codigo'],
                    $linha['nome'],
                    $linha['descricao'],
                    $linha['imagem'],
                    $linha['forca'],
                    $linha['destreza'],
                    $linha['inteligencia']
                );
                array_push($lista,$classe);
            }
        }

        return $lista;
    }

    public function obterClasse($codigo) {
        $query = $this->conexao->prepare("select * from classes where codigo = ?");
        $query->bind_param("i",$codigo);
        $query->execute();
        $query->store_result();
        $query->bind_result(
            $bind_codigo, $bind_nome, $bind_descricao, $bind_imagem,
            $bind_forca, $bind_destreza, $bind_inteligencia
        );

        if ($query->num_rows == 1) {
            $query->fetch();
            $classe = new Classe(
                $bind_codigo, $bind_nome, $bind_descricao, $bind_imagem,
                $bind_forca, $bind_destreza, $bind_inteligencia
            );
            return $classe;
        } else {
            throw new Exception("Classe de código " . $codigo . " não encontrada!");
        }
    }

    public function listarPersonagens() {
        $sql = "select
                    p.codigo as p_codigo, p.nome as p_nome, c.codigo as c_codigo, c.nome as c_nome,
                    c.descricao as c_descricao, c.imagem as c_imagem, c.forca as c_forca,
                    c.destreza as c_destreza, c.inteligencia as c_inteligencia
                from personagens p
                inner join classes c on p.codigoClasse = c.codigo
                order by p.codigo desc";
        $lista = array();
        $resultado = $this->conexao->query($sql);

        if ($resultado->num_rows > 0) {
            while ($linha = $resultado->fetch_assoc()) {
                $classe = new Classe(
                    $linha['c_codigo'], $linha['c_nome'], $linha['c_descricao'], $linha['c_imagem'],
                    $linha['c_forca'], $linha['c_destreza'], $linha['c_inteligencia']
                );
                $personagem = new Personagem($linha['p_codigo'], $classe, $linha['p_nome']);
                array_push($lista,$personagem);
            }
        }

        return $lista;
    }

    public function obterPersonagem($codigo) {
        $sql = "select
                    p.codigo as p_codigo, p.nome as p_nome, c.codigo as c_codigo, c.nome as c_nome,
                    c.descricao as c_descricao, c.imagem as c_imagem, c.forca as c_forca, c.destreza as c_destreza,
                    c.inteligencia as c_inteligencia
                from personagens p inner join classes c on p.codigoClasse = c.codigo where p.codigo = ?";

        $query = $this->conexao->prepare($sql);
        $query->bind_param("i",$codigo);
        $query->execute();
        $query->store_result();
        $query->bind_result(
            $bind_p_codigo, $bind_p_nome, $bind_c_codigo, $bind_c_nome, $bind_c_descricao,
            $bind_c_imagem, $bind_c_forca, $bind_c_destreza, $bind_c_inteligencia
        );

        if ($query->num_rows == 1) {
            $query->fetch();
            $classe = new Classe(
                $bind_c_codigo, $bind_c_nome, $bind_c_descricao, $bind_c_imagem,
                $bind_c_forca, $bind_c_destreza, $bind_c_inteligencia
            );
            $personagem = new Personagem($bind_p_codigo, $classe, $bind_p_nome);
            return $personagem;
        } else {
            throw new Exception("Personagem de código " . $codigo . " não encontrado!");
        }
    }


    function inserirPersonagem($personagem) {
        try {
            $query = $this->conexao->prepare("insert into personagens (codigoClasse, nome) values (?,?)");
            $query->bind_param(
                "is",
                $personagem->getClasse()->getCodigo(),
                $personagem->getNome()
            );
            $query->execute();

            $codigoInserido = $this->conexao->insert_id;
            return $this->obterPersonagem($codigoInserido);

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
