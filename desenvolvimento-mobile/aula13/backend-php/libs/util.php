<?php
    // 200 OK
    function status200($json) {
        header('Content-Type: application/json');
        echo json_encode($json, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        die();
    }
    // 400 Bad Request
    function status400($msg) {
        header("HTTP/1.1 400");
        echo '{"mensagem": "' . $msg . '"}';
        die();
    }
    // 404 Not Found
    function status404($msg) {
        header("HTTP/1.1 404");
        echo '{"mensagem": "' . $msg . '"}';
        die();
    }
    // 405 Method Not Allowed
    function status405() {
        header("HTTP/1.1 405");
        echo '{"mensagem": "Método não permitido!"}';
        die();
    }
