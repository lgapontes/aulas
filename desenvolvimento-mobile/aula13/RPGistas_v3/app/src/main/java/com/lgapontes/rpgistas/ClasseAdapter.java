package com.lgapontes.rpgistas;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lgapontes.rpgistas.modelo.Classe;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ClasseAdapter extends BaseAdapter {

    public static String PATH_FONTE = "fonts/lingming_manuscript.ttf";

    private Activity activity;
    private List<Classe> classes;

    public ClasseAdapter(Activity activity, List<Classe> classes) {
        this.activity = activity;
        this.classes = classes;
    }

    @Override
    public int getCount() {
        return this.classes.size();
    }

    @Override
    public Object getItem(int i) {
        return this.classes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.classes.get(i).getCodigo();
    }

    @Override
    public View getView(int position, View view, ViewGroup parentView) {
        RelativeLayout layout = (RelativeLayout) this.activity.getLayoutInflater()
                .inflate(R.layout.layout_classes, parentView, false);
        Classe classe = (Classe) getItem(position);

        TextView textNome = (TextView) layout.findViewById(R.id.nome_classe);
        textNome.setText(classe.getNome());
        definirFonte(textNome);
        TextView textDescricao = (TextView) layout.findViewById(R.id.descricao_classe);
        textDescricao.setText(classe.getDescricao());
        definirFonte(textDescricao);

        TextView textForca = (TextView) layout.findViewById(R.id.forca_classe);
        textForca.setText("FOR: " + Integer.toString(classe.getForca()));
        definirFonte(textForca);
        TextView textDestreza = (TextView) layout.findViewById(R.id.destreza_classe);
        textDestreza.setText("DES: " + Integer.toString(classe.getDestreza()));
        definirFonte(textDestreza);
        TextView textInteligencia = (TextView) layout.findViewById(R.id.inteligencia_classe);
        textInteligencia.setText("INT: " + Integer.toString(classe.getInteligencia()));
        definirFonte(textInteligencia);

        ImageView imageView = (ImageView) layout.findViewById(R.id.imagem_classe);
        Picasso.get().load(classe.getImagem()).into(imageView);

        return layout;
    }

    private void definirFonte(TextView view) {
        Typeface typeface = Typeface.createFromAsset(this.activity.getAssets(),PATH_FONTE);
        view.setTypeface(typeface);
    }
}
