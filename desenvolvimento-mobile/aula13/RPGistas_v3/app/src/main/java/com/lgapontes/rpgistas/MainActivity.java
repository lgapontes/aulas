package com.lgapontes.rpgistas;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.lgapontes.rpgistas.modelo.Classe;
import com.lgapontes.rpgistas.modelo.Personagem;
import com.lgapontes.rpgistas.repositorio.HttpUtils;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private EditText editText;
    private SwipeRefreshLayout swipe;

    private Classe classeSelecionada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipe.setRefreshing(true);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                obterClasses();
            }
        });

        editText = (EditText) findViewById(R.id.nome_personagem);
        Typeface typeface = Typeface.createFromAsset(getAssets(),ClasseAdapter.PATH_FONTE);
        editText.setTypeface(typeface);

        listView = MainActivity.this.findViewById(R.id.lista_classes);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                classeSelecionada = (Classe) listView.getItemAtPosition(position);
            }
        });

        obterClasses();
    }

    private void obterClasses() {
        String url = "http://lgapontes.com/aulas/rpgistas/api/classes";
        HttpUtils.get(url, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray array) {
                try {
                    if (statusCode == 200) {
                        List<Classe> lista = Classe.converterJsonParaLista(array);
                        ClasseAdapter adapter = new ClasseAdapter(MainActivity.this, lista);
                        listView.setAdapter(adapter);
                        swipe.setRefreshing(false);
                    } else {
                        throw new Exception("Ocorreu um erro ao obter os dados!");
                    }
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void salvarPersonagem(View view) {
        try {
            swipe.setRefreshing(true);
            String nome = editText.getText().toString();
            Personagem personagem = new Personagem(nome,classeSelecionada);
            if (!personagem.validar()) {
                throw new Exception("O nome e a classe do personagem são obrigatórios!");
            }

            JSONObject json = personagem.converterObjetoParaJson();
            String url = "http://lgapontes.com/aulas/rpgistas/api/personagens";
            HttpUtils.post(this, url, json, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                    Toast.makeText(MainActivity.this,
                            "Personagem inserido com sucesso!",Toast.LENGTH_LONG).show();
                    swipe.setRefreshing(false);
                }
            });
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
