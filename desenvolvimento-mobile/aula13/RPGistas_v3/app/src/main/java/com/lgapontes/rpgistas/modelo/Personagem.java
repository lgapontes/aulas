package com.lgapontes.rpgistas.modelo;

import org.json.JSONObject;

public class Personagem {
    private String nome;
    private Classe classe;

    public Personagem(String nome, Classe classe) {
        this.nome = nome;
        this.classe = classe;
    }

    public boolean validar() {
        if ( (this.nome == null) || (this.nome.length() < 3) ) return false;
        if ( (this.classe == null) || (this.classe.getCodigo() == 0) ) return false;
        return true;
    }

    public JSONObject converterObjetoParaJson() throws Exception  {
        try {
            JSONObject json = new JSONObject();
            json.put("codigoClasse",this.classe.getCodigo());
            json.put("nome",this.nome);
            return json;
        } catch (Exception e) {
            throw new Exception("Erro ao converter o objeto!");
        }
    }
}