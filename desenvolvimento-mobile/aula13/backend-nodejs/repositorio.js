var mysql = require('mysql');

var banco = mysql.createConnection({
  host: "linu.com.br",
  user: "linuc318_rpg",
  password: "123eja",
  database: "linuc318_rpgistas_db"
});

banco.connect(function(err) {
  if (err) throw err;
});

function listarClasses(callback) {
    let sql = "select * from classes";
    banco.query(sql, function (err, result) {
      if (err) throw err;
      callback(result);
    });
}

function obterClasse(codigo,callback) {
    let sql = "select * from classes where codigo = ?";
    banco.query(sql, [codigo], function (err, result) {
      if (err) throw err;
      if (result.length == 1) {
          callback(result[0]);
      } else {
          callback(null);
      }
    });
}

function obterPersonagens(callback,codigo) {
    let sql = "select p.codigo as p_codigo, p.nome as p_nome, c.codigo as c_codigo, " +
        "c.nome as c_nome, c.descricao as c_descricao, c.imagem as c_imagem, " +
        "c.forca as c_forca, c.destreza as c_destreza, c.inteligencia as c_inteligencia " +
        "from personagens p inner join classes c on p.codigoClasse = c.codigo ";
    if (codigo !== undefined) {
        sql += "where p.codigo = ?";
        banco.query(sql, [codigo], function (err, result) {
          if (err) throw err;
          callback(montarPersonagem(result[0]));
        });
    } else {
        banco.query(sql, function (err, result) {
          if (err) throw err;
          let lista = [];
          result.forEach(function(entry,index){
              lista.push(montarPersonagem(entry));
              if (index === (result.length - 1)) {
                  callback(lista);
              }
          });
        });
    }
}

function montarPersonagem(result) {
    if (result === undefined) return null;
    return { codigo: result.p_codigo, nome: result.p_nome,
      classe: { codigo: result.c_codigo, nome: result.c_nome,
          descricao: result.c_descricao, imagem: result.c_imagem,
          forca: result.c_forca, destreza: result.c_destreza,
          inteligencia: result.c_inteligencia }};
}

function listarPersonagens(callback) {
    obterPersonagens(callback);
}

function inserirPersonagem(dto,callback) {
    let sql = "insert into personagens (nome,codigoClasse) values (?,?)";
    banco.query(sql, [dto.nome,dto.codigoClasse], function (err, result) {
      if (err) throw err;

      let codigoInserido = result.insertId;
      obterPersonagens(function(personagem){
          callback(personagem);
      },codigoInserido);
    });
}

module.exports = {
    listarClasses: listarClasses,
    obterClasse: obterClasse,
    listarPersonagens: listarPersonagens,
    inserirPersonagem: inserirPersonagem
};
