var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var repositorio = require('./repositorio');

/* Permite obter dados de um POST */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var router = express.Router();

router.get('/classes', function(req, res) {
    repositorio.listarClasses(function(dados) {
        res.json(dados);
    });
});

router.get('/classes/:codigo', function(req, res) {
    let codigo = req.params.codigo;
    repositorio.obterClasse(codigo,function(classe) {
        if (classe === null) {
            res.status(404).send('Classe não encontrada!');
        } else {
            res.json(classe);
        }
    });
});

router.get('/personagens', function(req, res) {
    repositorio.listarPersonagens(function(dados) {
        res.json(dados);
    });
});

router.post('/personagens', function(req, res) {
    let dto = req.body;
    if (dto.nome === undefined || dto.nome.length < 3) {
        res.status(400).send('Nome inválido!');
    } else {
        repositorio.obterClasse(dto.codigoClasse,function(classe) {
            if (classe === null) {
                res.status(400).send('Classe não encontrada!');
            } else {
                repositorio.inserirPersonagem(dto,function(inserido){
                    res.json(inserido);
                });
            }
        });
    }
});

app.use('/api', router);
app.listen(8080);
console.log('Servidor inicializado');
