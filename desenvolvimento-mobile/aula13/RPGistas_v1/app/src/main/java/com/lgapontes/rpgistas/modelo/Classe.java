package com.lgapontes.rpgistas.modelo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Classe {
    private long codigo;
    private String nome;
    private String descricao;
    private String imagem;
    private int forca;
    private int destreza;
    private int inteligencia;

    public long getCodigo() { return codigo; }
    public String getNome() { return nome; }
    public String getDescricao() { return descricao; }
    public String getImagem() { return imagem; }
    public int getForca() { return forca; }
    public int getDestreza() { return destreza; }
    public int getInteligencia() { return inteligencia; }

    @Override
    public String toString() {
        return nome + " >> " + descricao;
    }

    private static Classe converterJsonParaObjeto(JSONObject json) throws JSONException {
        Classe classe = new Classe();
        classe.codigo = Long.parseLong(json.getString("codigo"));
        classe.nome = json.getString("nome");
        classe.descricao = json.getString("descricao");
        classe.imagem = json.getString("imagem");
        classe.forca = Integer.parseInt(json.getString("forca"));
        classe.destreza = Integer.parseInt(json.getString("destreza"));
        classe.inteligencia = Integer.parseInt(json.getString("inteligencia"));
        return classe;
    }

    public static List<Classe> converterJsonParaLista(JSONArray array) throws Exception {
        try {
            List<Classe> lista = new ArrayList<Classe>();

            for (int i=0; i<array.length(); i++) {
                JSONObject json = array.getJSONObject(i);
                Classe classe = converterJsonParaObjeto(json);
                lista.add(classe);
            }

            return lista;
        } catch (Exception e) {
            throw new Exception("Erro ao converter o JSON!");
        }
    }
}
