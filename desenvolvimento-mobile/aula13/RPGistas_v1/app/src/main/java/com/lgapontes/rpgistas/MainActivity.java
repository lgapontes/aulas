package com.lgapontes.rpgistas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.lgapontes.rpgistas.modelo.Classe;
import com.lgapontes.rpgistas.repositorio.HttpUtils;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        obterClasses();
    }

    private void obterClasses() {
        String url = "http://lgapontes.com/aulas/rpgistas/api/classes";
        HttpUtils.get(url, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray array) {
                try {
                    if (statusCode == 200) {
                        List<Classe> lista = Classe.converterJsonParaLista(array);
                        ArrayAdapter<Classe> adapter = new ArrayAdapter<Classe>(
                                MainActivity.this,android.R.layout.simple_list_item_1,lista);
                        ListView listView = MainActivity.this.findViewById(R.id.lista_classes);
                        listView.setAdapter(adapter);
                    } else {
                        throw new Exception("Ocorreu um erro ao obter os dados!");
                    }
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
