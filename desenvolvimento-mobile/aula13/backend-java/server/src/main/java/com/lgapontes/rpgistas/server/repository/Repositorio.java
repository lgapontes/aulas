package com.lgapontes.rpgistas.server.repository;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lgapontes.rpgistas.server.model.Classe;
import com.lgapontes.rpgistas.server.model.Personagem;

@Repository
@Transactional
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class Repositorio {
	
	@Autowired
	private SessionFactory sessionFactory;	
	
	public List<Classe> listarClasses() {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
	    TypedQuery<Classe> query = session.createQuery("select c from Classe c");			
		return query.getResultList();
	}
	
	public Classe obterClasse(long codigo) {
		Session session = sessionFactory.getCurrentSession();
		return session.find(Classe.class, codigo);
	}
	
	public List<Personagem> listarPersonagens() {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
	    TypedQuery<Personagem> query = session.createQuery("select p from Personagem p");			
		return query.getResultList();
	}
	
	public Personagem inserirPersonagem(Personagem personagem) {
		Session session = sessionFactory.getCurrentSession();
		Long codigoInserido = (Long) session.save(personagem);
		personagem.setCodigo(codigoInserido.longValue());
		return personagem;
	}
	
}