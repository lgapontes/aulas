package com.lgapontes.rpgistas.server.dto;

public class PersonagemDTO {

	private int codigoClasse;
	private String nome;
	
	public int getCodigoClasse() {
		return codigoClasse;
	}
	public void setCodigoClasse(int codigoClasse) {
		this.codigoClasse = codigoClasse;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
