package com.lgapontes.rpgistas.server.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.lgapontes.rpgistas.server.model.Classe;
import com.lgapontes.rpgistas.server.repository.Repositorio;

@RestController
public class ClasseController {

	@Autowired
	private Repositorio repositorio;
	
	@GetMapping("/classes")
	public List<Classe> getClasses() {		
		return this.repositorio.listarClasses();
	}
	
	@GetMapping("/classes/{id}")
	public ResponseEntity<Classe> getClasse(@PathVariable("id") long id) {		
		Classe classe = this.repositorio.obterClasse(id);
		if (classe == null) {
			return new ResponseEntity<Classe>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Classe>(classe, HttpStatus.OK);
		}		
	}	
}
