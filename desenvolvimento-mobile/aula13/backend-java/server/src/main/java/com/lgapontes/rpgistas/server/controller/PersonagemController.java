package com.lgapontes.rpgistas.server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lgapontes.rpgistas.server.dto.PersonagemDTO;
import com.lgapontes.rpgistas.server.model.Classe;
import com.lgapontes.rpgistas.server.model.Personagem;
import com.lgapontes.rpgistas.server.repository.Repositorio;

@RestController
public class PersonagemController {

	@Autowired
	private Repositorio repositorio;
	
	@GetMapping("/personagens")
	public List<Personagem> getPersonagens() {		
		return this.repositorio.listarPersonagens();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping(value = "/personagens")
	public ResponseEntity inserirPersonagem(@RequestBody PersonagemDTO dto) {
		
		Classe classe = this.repositorio.obterClasse(dto.getCodigoClasse());
		if (classe == null) {
			return new ResponseEntity("Código da classe não encontrado: " + 
					dto.getCodigoClasse(),HttpStatus.NOT_FOUND);			
		}
		
		if ( (dto.getNome() == null) || (dto.getNome().length() < 3)) {
			return new ResponseEntity("Nome inválido: " + 
					dto.getNome(),HttpStatus.BAD_REQUEST);			
		}
		
		Personagem personagem = new Personagem();
		personagem.setClasse(classe);
		personagem.setNome(dto.getNome());
		Personagem personagemInserido = this.repositorio.inserirPersonagem(personagem);

		return new ResponseEntity(personagemInserido, HttpStatus.OK);
	}
	
}
