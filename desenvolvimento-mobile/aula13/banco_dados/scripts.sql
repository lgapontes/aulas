create table classes (
    codigo bigint auto_increment,
    nome varchar(40) not null unique,
    descricao varchar(100) not null,
    imagem varchar(2048) not null,
    forca int not null,
    destreza int not null,
    inteligencia int not null,
    primary key (codigo)
);

create table personagens (
    codigo bigint auto_increment,
    codigoClasse bigint not null,
    nome varchar(40) not null,
    primary key (codigo),
    foreign key (codigoClasse) references classes(codigo)
);

insert into classes (nome,descricao,imagem,forca,destreza,inteligencia) values
    ('Guerreiro','Habilidade com armas brancas',
        'http://lgapontes.com/aulas/rpgistas/img/ic_guerreiro.png',5,3,2),
    ('Arqueiro','Habilidade com arcos',
        'http://lgapontes.com/aulas/rpgistas/img/ic_arqueiro.png',3,4,3),
    ('Feiticeiro','Conhecimento mágico',
        'http://lgapontes.com/aulas/rpgistas/img/ic_feiticeiro.png',2,2,6),
    ('Ninja','Artes marciais com espadas',
        'http://lgapontes.com/aulas/rpgistas/img/ic_ninja.png',3,5,2);
