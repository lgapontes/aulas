package com.lgapontes.dicaboa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.lgapontes.dicaboa.modelo.Marca;
import com.lgapontes.dicaboa.repositorio.IRepositorio;
import com.lgapontes.dicaboa.repositorio.MarcaRepositorio;
import com.lgapontes.dicaboa.repositorio.ProcessadorRepositorio;
import com.lgapontes.dicaboa.repositorio.SistemaRepositorio;

import java.util.List;

public class CadastroActivity extends AppCompatActivity {

    private Spinner spinnerMarcas;
    private Spinner spinnerSistemas;
    private Spinner spinnerProcessadores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        spinnerMarcas = (Spinner) findViewById(R.id.form_spinner_marcas);
        popularSpinner(spinnerMarcas, new MarcaRepositorio());

        spinnerSistemas = (Spinner) findViewById(R.id.form_spinner_sistemas);
        popularSpinner(spinnerSistemas, new SistemaRepositorio());

        spinnerProcessadores = (Spinner) findViewById(R.id.form_spinner_processadores);
        popularSpinner(spinnerProcessadores, new ProcessadorRepositorio());
    }

    private void popularSpinner(Spinner spinner, IRepositorio repositorio) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                repositorio.listar()
        );
        spinnerArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

}
