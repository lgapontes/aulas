package com.lgapontes.dicaboa.repositorio;

import com.lgapontes.dicaboa.modelo.Processador;
import java.util.ArrayList;
import java.util.List;

public class ProcessadorRepositorio implements IRepositorio<Processador> {

    @Override
    public List<Processador> listar() {
        String[] itens = new String[] {
                "Intel i3 2GHz",
                "Intel i5 2,5GHz",
                "Intel i5 3GHz",
                "Intel i7 3GHz",
                "Intel i9 3,5GHz"
        };

        List<Processador> cpus = new ArrayList<Processador>();
        for (int i=0; i<itens.length; i++) {
            cpus.add(new Processador( i+1, itens[i]));
        }

        return cpus;
    }
}
