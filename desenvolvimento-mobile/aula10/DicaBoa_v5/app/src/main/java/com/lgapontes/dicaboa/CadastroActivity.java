package com.lgapontes.dicaboa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.lgapontes.dicaboa.modelo.Marca;
import com.lgapontes.dicaboa.repositorio.IRepositorio;
import com.lgapontes.dicaboa.repositorio.MarcaRepositorio;
import com.lgapontes.dicaboa.repositorio.ProcessadorRepositorio;
import com.lgapontes.dicaboa.repositorio.SistemaRepositorio;

import java.util.List;

public class CadastroActivity extends AppCompatActivity {

    private Spinner spinnerMarcas;
    private Spinner spinnerSistemas;
    private Spinner spinnerProcessadores;
    private Switch switchUsado;
    private RadioGroup radioGroupTipo;

    private TextView textViewMemoria;
    private SeekBar seekBarMemoria;

    private EditText editTextHD;
    private EditText editTextSSD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        spinnerMarcas = (Spinner) findViewById(R.id.form_spinner_marcas);
        popularSpinner(spinnerMarcas, new MarcaRepositorio());

        spinnerSistemas = (Spinner) findViewById(R.id.form_spinner_sistemas);
        popularSpinner(spinnerSistemas, new SistemaRepositorio());

        spinnerProcessadores = (Spinner) findViewById(R.id.form_spinner_processadores);
        popularSpinner(spinnerProcessadores, new ProcessadorRepositorio());

        switchUsado = (Switch) findViewById(R.id.form_switch_usado);
        radioGroupTipo = (RadioGroup) findViewById(R.id.form_radiogroup_tipo);

        textViewMemoria = (TextView) findViewById(R.id.form_textview_memoria);
        seekBarMemoria = (SeekBar) findViewById(R.id.form_seekbar_memoria);
        seekBarMemoria.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean fromUser) {
                textViewMemoria.setText(calcularValorSeekBar(value));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        editTextHD = (EditText) findViewById(R.id.form_edittext_hd);
        editTextSSD = (EditText) findViewById(R.id.form_edittext_ssd);
    }

    private String calcularValorSeekBar(int valor) {
        if (valor == 0) {
            return "Sem memória RAM";
        } else {
            return Integer.toString(valor) + " GB";
        }
    }

    private void popularSpinner(Spinner spinner, IRepositorio repositorio) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                    this,
                    R.layout.spinner_item,
                    repositorio.listar()
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

}
