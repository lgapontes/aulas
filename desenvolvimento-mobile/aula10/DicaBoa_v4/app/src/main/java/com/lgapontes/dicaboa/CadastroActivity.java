package com.lgapontes.dicaboa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;

import com.lgapontes.dicaboa.modelo.Marca;
import com.lgapontes.dicaboa.repositorio.IRepositorio;
import com.lgapontes.dicaboa.repositorio.MarcaRepositorio;
import com.lgapontes.dicaboa.repositorio.ProcessadorRepositorio;
import com.lgapontes.dicaboa.repositorio.SistemaRepositorio;

import java.util.List;

public class CadastroActivity extends AppCompatActivity {

    private Spinner spinnerMarcas;
    private Spinner spinnerSistemas;
    private Spinner spinnerProcessadores;
    private Switch switchUsado;
    private RadioGroup radioGroupTipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        spinnerMarcas = (Spinner) findViewById(R.id.form_spinner_marcas);
        popularSpinner(spinnerMarcas, new MarcaRepositorio());

        spinnerSistemas = (Spinner) findViewById(R.id.form_spinner_sistemas);
        popularSpinner(spinnerSistemas, new SistemaRepositorio());

        spinnerProcessadores = (Spinner) findViewById(R.id.form_spinner_processadores);
        popularSpinner(spinnerProcessadores, new ProcessadorRepositorio());

        switchUsado = (Switch) findViewById(R.id.form_switch_usado);
        radioGroupTipo = (RadioGroup) findViewById(R.id.form_radiogroup_tipo);
    }

    private void popularSpinner(Spinner spinner, IRepositorio repositorio) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                    this,
                    R.layout.spinner_item,
                    repositorio.listar()
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

}
