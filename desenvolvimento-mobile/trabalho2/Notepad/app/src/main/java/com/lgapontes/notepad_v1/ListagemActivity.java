package com.lgapontes.notepad_v1;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.lgapontes.notepad_v1.modelo.Arquivo;

import java.util.List;

public class ListagemActivity extends AppCompatActivity {

    private ListView listView;
    private FloatingActionButton fabNovo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem);

        listView = (ListView) findViewById(R.id.lista_arquivos);
        fabNovo = (FloatingActionButton) findViewById(R.id.fab_novo);

        fabNovo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentNovoArquivo = new Intent(ListagemActivity.this, ArquivoActivity.class);
                startActivity(intentNovoArquivo);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> lista, View view, int position, long id) {
                Arquivo arquivo = (Arquivo) lista.getItemAtPosition(position);
                Intent intent = new Intent(ListagemActivity.this, ArquivoActivity.class);
                intent.putExtra("arquivo", arquivo);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<Arquivo> arquivos = Arquivo.listar(this);
        ArrayAdapter<Arquivo> adapter = new ArrayAdapter<Arquivo>(
                this,
                android.R.layout.simple_list_item_1,
                arquivos);
        listView.setAdapter(adapter);
    }
}
