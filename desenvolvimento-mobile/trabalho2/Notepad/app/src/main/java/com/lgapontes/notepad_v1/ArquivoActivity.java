package com.lgapontes.notepad_v1;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.lgapontes.notepad_v1.modelo.Arquivo;

public class ArquivoActivity extends AppCompatActivity {

    private EditText nomeArquivo;
    private EditText textoArquivo;
    private CoordinatorLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arquivo);

        nomeArquivo = (EditText) findViewById(R.id.nome_arquivo);
        textoArquivo = (EditText) findViewById(R.id.texto_arquivo);
        layout = (CoordinatorLayout) findViewById(R.id.layout_novo_arquivo);

        Intent intent = getIntent();
        if (intent.hasExtra("arquivo")) {
            Arquivo arquivo = (Arquivo) intent.getSerializableExtra("arquivo");
            nomeArquivo.setText(arquivo.getNome());
            textoArquivo.setText(arquivo.getTexto());
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_arquivo, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void salvarArquivo() {
        if (nomeArquivo.getText().toString().length() == 0) {
            Toast.makeText(this, R.string.msg_nome_arquivo_invalido, Toast.LENGTH_LONG).show();
            return;
        }

        Arquivo arquivo = new Arquivo(
                nomeArquivo.getText().toString(),
                textoArquivo.getText().toString()
        );

        arquivo.salvar(this);
        finish();
        Toast.makeText(this, R.string.msg_arquivo_salvo, Toast.LENGTH_LONG).show();
    }

    private void apagarArquivo() {
        Snackbar snackbar = Snackbar.make(layout, R.string.msg_apagar_arquivo, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.action_msg_apagar_arquivo, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Arquivo arquivo = new Arquivo(
                        nomeArquivo.getText().toString(),
                        textoArquivo.getText().toString()
                );
                arquivo.apagar(ArquivoActivity.this);
                finish();
                Toast.makeText(ArquivoActivity.this, R.string.msg_arquivo_apagado, Toast.LENGTH_LONG);
            }
        });
        snackbar.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_salvar:
                salvarArquivo();
                break;
            case R.id.menu_apagar:
                apagarArquivo();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
