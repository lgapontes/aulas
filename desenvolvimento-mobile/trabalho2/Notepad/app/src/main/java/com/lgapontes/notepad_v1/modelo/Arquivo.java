package com.lgapontes.notepad_v1.modelo;

import android.content.Context;
import android.util.Log;

import com.lgapontes.notepad_v1.util.ArquivoHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Arquivo implements Serializable {

    private String nome;
    private String texto;

    public Arquivo(String nome, String texto) {
        this.nome = nome;
        this.texto = texto;
    }

    public String getNome() {
        return nome;
    }

    public String getTexto() {
        return texto;
    }

    @Override
    public String toString() {
        return this.nome;
    }

    public void salvar(Context context) {
        try {
            ArquivoHelper.salvarArquivo(context, getNome(), getTexto());
        }
        catch (Exception e) {
            Log.e("Exception", e.toString());
        }
    }

    public void apagar(Context context) {
        if (getNome() != null) {
            ArquivoHelper.apagarArquivo(context, getNome());
        }
    }

    public static List<Arquivo> listar(Context context) {
        List<Arquivo> arquivos = new ArrayList<Arquivo>();

        try {
            String[] nomes = ArquivoHelper.listarArquivos(context);
            for (int i=0; i<nomes.length; i++) {
                String nome = nomes[i];
                String conteudo = ArquivoHelper.lerArquivo(context, nomes[i]);
                arquivos.add(new Arquivo(nome, conteudo));
            }
        }
        catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return arquivos;
    }
}
