package com.lgapontes.pdf4me_v1;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class Repositorio {

    private SharedPreferences preferences;

    public Repositorio(Context context) {
        this.preferences = context.getSharedPreferences("pdf4me", Context.MODE_PRIVATE);
    }

    public Configuracao abrir() {
        String json = this.preferences.getString("configuracao", null);
        if (json != null) {
            Gson gson = new Gson();
            Configuracao configuracao = gson.fromJson(json, Configuracao.class);
            return configuracao;
        }
        return null;
    }

    public void salvar(Configuracao configuracao) {
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(configuracao);
        editor.putString("configuracao", json);
        editor.commit();
    }

}
