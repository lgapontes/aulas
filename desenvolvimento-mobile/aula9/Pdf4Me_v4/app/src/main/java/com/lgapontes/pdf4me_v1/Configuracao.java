package com.lgapontes.pdf4me_v1;

public class Configuracao {

    private String uri;
    private int pagina;

    public Configuracao(String uri, int pagina) {
        this.uri = uri;
        this.pagina = pagina;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }
}
