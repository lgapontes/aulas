package com.lgapontes.pdf4me_v1;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

public class MainActivity extends AppCompatActivity {

    private FloatingActionButton fabPaginaAnterior;
    private FloatingActionButton fabPaginaPosterior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fabPaginaAnterior = (FloatingActionButton) findViewById(R.id.fab_pagina_anterior);
        fabPaginaPosterior = (FloatingActionButton) findViewById(R.id.fab_pagina_posterior);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
}