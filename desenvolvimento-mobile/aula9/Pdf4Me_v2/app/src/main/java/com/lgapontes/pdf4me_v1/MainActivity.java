package com.lgapontes.pdf4me_v1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private static final int CODIGO_ABRIR_ARQUIVO = 1;

    private FloatingActionButton fabPaginaAnterior;
    private FloatingActionButton fabPaginaPosterior;

    private ImageView pdfView;
    private ArquivoPDF arquivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fabPaginaAnterior = (FloatingActionButton)
                findViewById(R.id.fab_pagina_anterior);
        fabPaginaAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arquivo != null) {
                    arquivo.paginaAnterior();
                }
            }
        });

        fabPaginaPosterior = (FloatingActionButton)
                findViewById(R.id.fab_pagina_posterior);
        fabPaginaPosterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arquivo != null) {
                    arquivo.paginaPosterior();
                }
            }
        });

        pdfView = (ImageView)findViewById(R.id.pdfview);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_abrir:
                abrirFAS();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void abrirFAS() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/pdf");
        startActivityForResult(intent, CODIGO_ABRIR_ARQUIVO);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CODIGO_ABRIR_ARQUIVO) {
                if (resultData != null) {
                    Uri uriArquivo = resultData.getData();
                    this.arquivo = ArquivoPDF.abrir(this, pdfView, uriArquivo);
                }
            }
        }
    }

}
