package com.lgapontes.numerosecreto_v1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.List;

public class ResultadoActivity extends AppCompatActivity {

    private ListView partidas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        this.partidas = (ListView) findViewById(R.id.partidas);
        Button novaPartida = (Button) findViewById(R.id.nova_partida);
        novaPartida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<Partida> partidas = Repositorio.getInstance().listar();
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, partidas);
        this.partidas.setAdapter(adapter);
    }
}
