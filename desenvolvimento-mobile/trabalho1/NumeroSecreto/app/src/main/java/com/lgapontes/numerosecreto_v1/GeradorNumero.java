package com.lgapontes.numerosecreto_v1;

import java.util.Random;

public class GeradorNumero {

    private static int TOTAL_CARACTERES = 4;

    private static int gerarUmNumero() {
        Random random = new Random();
        int numero = random.nextInt(10);
        return numero;
    }

    public static String gerar() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<TOTAL_CARACTERES; i++) {
            sb.append( gerarUmNumero() );
        }
        return sb.toString();
    }

}
