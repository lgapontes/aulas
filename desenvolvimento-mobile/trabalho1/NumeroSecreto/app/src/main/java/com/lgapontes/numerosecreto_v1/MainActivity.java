package com.lgapontes.numerosecreto_v1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private static boolean DEBUG = false;

    private TextView[] numeros;
    private Partida partida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.numeros = new TextView[]{
                (TextView) findViewById(R.id.numero_1),
                (TextView) findViewById(R.id.numero_2),
                (TextView) findViewById(R.id.numero_3),
                (TextView) findViewById(R.id.numero_4)
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.partida = Repositorio.getInstance().novaPartida();
        atualizarStatus();
        atualizarDebug();
    }

    private void atualizarStatus() {
        String[] status = MainActivity.this.partida.obterStatus();
        for (int i=0; i<status.length; i++) {
            TextView textView = MainActivity.this.numeros[i];
            textView.setText(status[i]);
        }
    }

    private void atualizarDebug() {
        if (DEBUG) {
            TextView debug = findViewById(R.id.debug);
            debug.setText(MainActivity.this.partida.toString());
        }
    }

    public void click(View view) {
        Partida p = MainActivity.this.partida;
        String digito = ( (Button) view).getText().toString();
        String numeroDigitado = p.getNumeroDigitado() + digito;
        p.setNumeroDigitado(numeroDigitado);

        atualizarStatus();
        atualizarDebug();

        if (partida.terminou()) {

            Repositorio.getInstance().incluir(partida);

            Intent intent = new Intent(MainActivity.this, ResultadoActivity.class);
            startActivity(intent);
        }
    }
}
