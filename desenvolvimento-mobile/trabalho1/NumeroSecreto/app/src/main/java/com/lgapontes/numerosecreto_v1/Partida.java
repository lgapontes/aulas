package com.lgapontes.numerosecreto_v1;

import java.util.Arrays;

public class Partida {

    private int codigoPartida;
    private String numeroAleatorio;
    private String numeroDigitado;

    public Partida(int codigoPartida, String numeroAleatorio) {
        this.codigoPartida = codigoPartida;
        this.numeroAleatorio = numeroAleatorio;
        this.numeroDigitado = "";
    }

    public String getNumeroDigitado() {
        return this.numeroDigitado;
    }

    public void setNumeroDigitado(String numeroDigitado) {
        this.numeroDigitado = numeroDigitado;
    }

    public String[] obterStatus() {
        String[] status = new String[4];
        Arrays.fill(status,"_");

        for (int i=0; i < this.numeroDigitado.length(); i++) {
            if (this.numeroDigitado.charAt(i) == this.numeroAleatorio.charAt(i)) {
                status[i] = "Ok";
            } else {
                status[i] = "X";
            }
        }

        return status;
    }

    private int totalPontos() {
        int pontos = 0;
        for (int i=0; i<this.numeroDigitado.length(); i++) {
            if (this.numeroAleatorio.charAt(i) == this.numeroDigitado.charAt(i)) {
                pontos += 2;
            }
        }
        return pontos;
    }

    public boolean terminou() {
        return this.numeroDigitado.length() == 4;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return "#" + this.codigoPartida +
                " > " + this.numeroAleatorio +
                " | " + this.numeroDigitado +
                " = " + this.totalPontos() + " pontos";
    }
}
