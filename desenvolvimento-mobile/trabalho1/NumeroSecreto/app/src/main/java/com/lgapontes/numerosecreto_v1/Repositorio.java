package com.lgapontes.numerosecreto_v1;

import java.util.ArrayList;
import java.util.List;

public class Repositorio {
    private static final Repositorio singleton = new Repositorio();

    public static Repositorio getInstance() {
        return singleton;
    }

    private List<Partida> partidas;

    private Repositorio() {
        this.partidas = new ArrayList<Partida>();
    }

    public List<Partida> listar() {
        return this.partidas;
    }

    public void incluir(Partida partida) {
        this.partidas.add(partida);
    }

    public Partida novaPartida() {
        return new Partida(
                this.partidas.size() + 1,
                GeradorNumero.gerar()
        );
    }
}
