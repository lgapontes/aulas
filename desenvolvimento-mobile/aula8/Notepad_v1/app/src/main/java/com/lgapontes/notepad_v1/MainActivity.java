package com.lgapontes.notepad_v1;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private CoordinatorLayout layout;
    private Button botaoLimpar;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        editText = (EditText) findViewById(R.id.edit_text);

        botaoLimpar = (Button) findViewById(R.id.botao_limpar);
        botaoLimpar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snackbar = Snackbar.make(layout, R.string.mensagem_confirmacao, Snackbar.LENGTH_LONG);
                snackbar.setAction(R.string.snackbar_action, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editText.setText("");
                    }
                });
                snackbar.show();
            }
        });
    }
}
