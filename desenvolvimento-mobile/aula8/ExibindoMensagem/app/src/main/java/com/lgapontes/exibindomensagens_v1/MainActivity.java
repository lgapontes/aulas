package com.lgapontes.exibindomensagens_v1;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void exibirToast(View view) {
        Toast.makeText(this, "Eu sou um toast!", Toast.LENGTH_SHORT).show();
    }

    public void exibirToastCustomizado(View view) {
        Toast toast = new Toast(this);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(
                R.layout.toast_layout,
                (LinearLayout) findViewById(R.id.toast_container)
        );

        TextView textView = (TextView) layout.findViewById(R.id.toast_message);
        textView.setText("Eu sou um toast customizado!");

        toast.setView(layout);
        toast.show();
    }

    public void exibirSnackbar_(View view) {
        CoordinatorLayout main = (CoordinatorLayout) findViewById(R.id.main_layout);
        final Snackbar snackbar = Snackbar.make(main, "Eu sou um Snackbar", Snackbar.LENGTH_LONG);
        snackbar.setAction("DESFAZER", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
                Toast.makeText(MainActivity.this, "Parabéns, ação desfeita!", Toast.LENGTH_SHORT).show();
            }
        });

        snackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                if (event == Snackbar.Callback.DISMISS_EVENT_SWIPE) {
                    Toast.makeText(MainActivity.this, "Ok, você cancelou!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onShown(Snackbar snackbar) {
            }
        });

        snackbar.show();
    }

    public void exibirSnackbar(View view) {
        CoordinatorLayout main = (CoordinatorLayout) findViewById(R.id.main_layout);
        Snackbar snackbar = Snackbar.make(main, "Eu sou um Snackbar", Snackbar.LENGTH_LONG);
        snackbar.setAction("DESFAZER", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Parabéns, ação desfeita!", Toast.LENGTH_SHORT).show();
            }
        });
        snackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                if (event == Snackbar.Callback.DISMISS_EVENT_SWIPE) {
                    Toast.makeText(MainActivity.this, "Ok, você cancelou!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onShown(Snackbar snackbar) {}
        });
        snackbar.show();
    }
}