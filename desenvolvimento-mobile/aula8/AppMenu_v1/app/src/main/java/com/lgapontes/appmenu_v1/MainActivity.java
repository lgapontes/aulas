package com.lgapontes.appmenu_v1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bolt: exibirToast("Flash"); break;
            case R.id.menu_download: exibirToast("Download"); break;
            case R.id.menu_plugin: exibirToast("Plugin"); break;
            case R.id.menu_like: exibirToast("Like"); break;
            case R.id.menu_stars: exibirToast("Importante"); break;
            case R.id.menu_tag: exibirToast("Marcador"); break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void exibirToast(String menu) {
        Toast.makeText(this, "O menu " + menu + " foi clicado!", Toast.LENGTH_SHORT).show();
    }
}
