package com.lgapontes.dicaboa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.lgapontes.dicaboa.modelo.Computador;
import com.lgapontes.dicaboa.repositorio.ComputadorRepositorio;
import com.lgapontes.dicaboa.repositorio.Repositorio;

import java.util.List;

public class ListagemActivity extends AppCompatActivity {

    private ListView listagemComputadores;
    private ComputadorRepositorio repositorio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem);

        this.listagemComputadores = (ListView) findViewById(R.id.listagem_computadores);
        this.repositorio = new ComputadorRepositorio(new Repositorio(this));
    }

    @Override
    protected void onResume() {
        super.onResume();

        List<Computador> lista = this.repositorio.listar();
        ArrayAdapter<Computador> adapter = new ArrayAdapter<Computador>(
                this,
                android.R.layout.simple_list_item_1,
                lista
        );
        listagemComputadores.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.form_listagem_menu_salvar) {
            Intent intent = new Intent(this,CadastroActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_listagem,menu);
        return super.onCreateOptionsMenu(menu);
    }
}
