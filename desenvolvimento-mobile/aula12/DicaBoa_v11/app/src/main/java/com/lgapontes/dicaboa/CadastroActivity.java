package com.lgapontes.dicaboa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.lgapontes.dicaboa.modelo.Computador;
import com.lgapontes.dicaboa.modelo.Marca;
import com.lgapontes.dicaboa.modelo.Processador;
import com.lgapontes.dicaboa.modelo.Sistema;
import com.lgapontes.dicaboa.repositorio.ComputadorRepositorio;
import com.lgapontes.dicaboa.repositorio.IRepositorio;
import com.lgapontes.dicaboa.repositorio.MarcaRepositorio;
import com.lgapontes.dicaboa.repositorio.ProcessadorRepositorio;
import com.lgapontes.dicaboa.repositorio.Repositorio;
import com.lgapontes.dicaboa.repositorio.SistemaRepositorio;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class CadastroActivity extends AppCompatActivity {

    private Spinner spinnerMarcas;
    private Spinner spinnerSistemas;
    private Spinner spinnerProcessadores;
    private Switch switchUsado;
    private RadioGroup radioGroupTipo;

    private TextView textViewMemoria;
    private SeekBar seekBarMemoria;

    private EditText editTextHD;
    private EditText editTextSSD;

    private EditText editTextPreco;

    private RatingBar ratingBarNota;

    private Repositorio repositorio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        this.repositorio = new Repositorio(this);
        spinnerMarcas = (Spinner) findViewById(R.id.form_spinner_marcas);
        popularSpinner(spinnerMarcas, new MarcaRepositorio(this.repositorio));

        spinnerSistemas = (Spinner) findViewById(R.id.form_spinner_sistemas);
        popularSpinner(spinnerSistemas, new SistemaRepositorio(this.repositorio));

        spinnerProcessadores = (Spinner) findViewById(R.id.form_spinner_processadores);
        popularSpinner(spinnerProcessadores, new ProcessadorRepositorio(this.repositorio));

        switchUsado = (Switch) findViewById(R.id.form_switch_usado);
        radioGroupTipo = (RadioGroup) findViewById(R.id.form_radiogroup_tipo);

        textViewMemoria = (TextView) findViewById(R.id.form_textview_memoria);
        seekBarMemoria = (SeekBar) findViewById(R.id.form_seekbar_memoria);
        seekBarMemoria.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean fromUser) {
                textViewMemoria.setText(calcularValorSeekBar(value));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        editTextHD = (EditText) findViewById(R.id.form_edittext_hd);
        editTextSSD = (EditText) findViewById(R.id.form_edittext_ssd);

        editTextPreco = (EditText) findViewById(R.id.form_edittext_preco);
        FormatadorMoeda.configurar(editTextPreco);

        ratingBarNota = (RatingBar) findViewById(R.id.form_ratingbar_nota);
    }

    private String calcularValorSeekBar(int valor) {
        if (valor == 0) {
            return "Sem memória RAM";
        } else {
            return Integer.toString(valor) + " GB";
        }
    }

    private void popularSpinner(Spinner spinner, IRepositorio repositorio) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                    this,
                    R.layout.spinner_item,
                    repositorio.listar()
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cadastro, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.form_cadastro_menu_salvar:
                criarComputador();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void criarComputador() {

        Computador computador = new Computador();

        Marca marca = (Marca) spinnerMarcas.getSelectedItem();
        computador.setMarca(marca);

        Sistema sistema = (Sistema) spinnerSistemas.getSelectedItem();
        computador.setSistema(sistema);

        Processador processador = (Processador) spinnerProcessadores.getSelectedItem();
        computador.setCpu(processador);

        computador.setUsado(switchUsado.isChecked());

        int idSelecionado = radioGroupTipo.getCheckedRadioButtonId();
        if (idSelecionado == -1) {
            Toast.makeText(this,"Selecione o tipo!", Toast.LENGTH_SHORT).show();
            return;
        }

        RadioButton radioButtonSelecionado = (RadioButton) findViewById(idSelecionado);
        computador.setTipo(radioButtonSelecionado.getText().toString());

        computador.setMemoriaRam(seekBarMemoria.getProgress());

        String hd = editTextHD.getText().toString();
        if (!hd.equals("")) { computador.setHd(Integer.parseInt(hd)); }

        String ssd = editTextSSD.getText().toString();
        if (!ssd.equals("")) { computador.setSsd(Integer.parseInt(ssd)); }

        String preco = editTextPreco.getText().toString();
        if (preco.equals("")) { preco = "R$0,00"; }
        computador.setPreco(preco);

        computador.setNota(ratingBarNota.getRating());

        ComputadorRepositorio db = new ComputadorRepositorio(this.repositorio);
        db.salvar(computador);

        Toast.makeText(this,"Dados salvos com sucesso!",
                Toast.LENGTH_SHORT).show();
        finish();
    }

}
