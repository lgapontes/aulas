package com.lgapontes.dicaboa;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lgapontes.dicaboa.modelo.Computador;

import java.util.List;

public class ComputadoresAdapter extends BaseAdapter {

    private List<Computador> computadores;
    private Activity activity;

    public ComputadoresAdapter(List<Computador> computadores, Activity activity) {
        this.computadores = computadores;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return this.computadores.size();
    }

    @Override
    public Object getItem(int i) {
        return this.computadores.get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.valueOf(i);
    }

    @Override
    public View getView(int position, View view, ViewGroup parentView) {
        RelativeLayout layout = (RelativeLayout) activity.getLayoutInflater()
                .inflate(R.layout.exibir_computador, parentView, false);
        Computador computador = (Computador) getItem(position);

        if (computador.getTipo().equals("Notebook")) {
            ImageView imageView = layout.findViewById(R.id.icone_tipo_computador);
            imageView.setImageResource(R.drawable.ic_notebook);
        }

        TextView tvMarca = layout.findViewById(R.id.textview_marca_computador);
        tvMarca.setText(computador.getMarca().getNome());

        String detalhes = "CPU " + computador.getCpu().getNome() + " / " +
                "RAM " + Integer.toString(computador.getMemoriaRam())  + "GB";
        TextView tvDetalhes = layout.findViewById(R.id.textview_detalhes_computador);
        tvDetalhes.setText(detalhes);

        TextView tvPreco = layout.findViewById(R.id.textview_preco_computador);
        tvPreco.setText(computador.getPreco());

        return layout;
    }

}
