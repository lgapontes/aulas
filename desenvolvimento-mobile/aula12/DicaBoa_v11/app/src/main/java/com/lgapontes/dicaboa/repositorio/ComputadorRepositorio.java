package com.lgapontes.dicaboa.repositorio;

import com.lgapontes.dicaboa.modelo.Computador;

import java.util.List;

public class ComputadorRepositorio {

    private Repositorio repositorio;

    public ComputadorRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    public void salvar(Computador computador) {
        this.repositorio.salvarComputador(computador);
    }

    public List<Computador> listar() {
        return this.repositorio.listarComputadores();
    }

}