package com.lgapontes.dicaboa.repositorio;

import com.lgapontes.dicaboa.modelo.IDetalhe;
import com.lgapontes.dicaboa.modelo.Sistema;

import java.util.ArrayList;
import java.util.List;

public class SistemaRepositorio implements IRepositorio<IDetalhe> {

    private Repositorio repositorio;

    public SistemaRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public List<IDetalhe> listar() {
        return this.repositorio.listarDetalhes(Sistema.class,"sistemas");
    }
}
